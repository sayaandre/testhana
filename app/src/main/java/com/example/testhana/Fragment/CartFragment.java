package com.example.testhana.Fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testhana.Activity.SalesOrderProductActivity;
import com.example.testhana.Activity.ScannerActivity;
import com.example.testhana.Adapter.CartOrderAdapter;
import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.CartHelper;
import com.example.testhana.Helper.Constant;
import com.example.testhana.Helper.RealmHelper;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Helper.Tools;
import com.example.testhana.Helper.Util;
import com.example.testhana.MainActivity;
import com.example.testhana.Models.Model.CartOrderModel;
import com.example.testhana.Models.Realm.Cart;
import com.example.testhana.Models.Realm.CartOrder;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.Models.Realm.ItemPrice;
import com.example.testhana.Models.Realm.SalesOrder;
import com.example.testhana.Models.Realm.SalesOrderDetail;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.ApiServiceHana;
import com.example.testhana.Rest.Response.SubmitSalesOrderResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartFragment extends Fragment implements SearchView.OnQueryTextListener{

    @BindView(R.id.imgCancel) ImageButton imgCancel;
    @BindView(R.id.txtCustomer) TextView txtCustomer;
    @BindView(R.id.txtScan) ImageView ivScan;
    @BindView(R.id.txtSearch) ImageView ivSearch;
    @BindView(R.id.btnCustomAmount) Button btnCustomAmount;
    @BindView(R.id.btnClear) Button btnClear;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.btnPay) Button btnPay;
    @BindView(R.id.txtSubtotal) TextView txtSubtotal;
    private CartOrderAdapter adapter;
    List<CartOrderModel> cartModels;
    CartHelper helper;
    Unbinder unbinder;
    Realm realm;
    SessionManagement session;
    ApiServiceHana apiHana;
    RealmHelper realmHelper;
    CartHelper cartHelper;
    ProgressDialog progressDialog;
    SearchView searchView;
    MenuItem item;
    ApiService api;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_cart, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        cartModels = new ArrayList<>();
        realm = Realm.getDefaultInstance();
        realmHelper = new RealmHelper();
        cartHelper = new CartHelper(getActivity());
        session = CoreApplication.getInstance().getSession();
        session.setKeyCustomer("");
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        api = ApiClient.getClient().create(ApiService.class);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        helper = new CartHelper(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, 0, true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        setAdapter();
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder kotakBuilder = new AlertDialog.Builder(getActivity(),
                        R.style.AppCompatAlertDialogStyle);
                kotakBuilder.setIcon(android.R.drawable.ic_dialog_alert);
                kotakBuilder.setTitle("Notice");
                kotakBuilder.setMessage("Are You Sure?");
                kotakBuilder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                realm.beginTransaction();
                                RealmResults<Cart> carts = realm.where(Cart.class).findAll();
                                carts.deleteAllFromRealm();
                                realm.commitTransaction();
                                setAdapter();
                            }
                        });
                kotakBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = kotakBuilder.create();
                dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_white));
                dialog.show();
            }
        });
        ivScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 20);
                }
                else
                {
                    Intent i = new Intent(getActivity(), ScannerActivity.class);
                    startActivityForResult(i, 95);
                }
            }
        });
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.setKeyCustomer("0");
                imgCancel.setVisibility(View.GONE);
                txtCustomer.setText("Customer");
            }
        });
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                session.setKeyCustomer("C30000");
                if(!session.getKeyCustomer().equalsIgnoreCase("") && !session.getKeyCustomer().equals("0"))
                {
                    AlertDialog.Builder kotakBuilder = new AlertDialog.Builder(getActivity(),
                            R.style.AppCompatAlertDialogStyle);
                    kotakBuilder.setIcon(android.R.drawable.ic_dialog_alert);
                    kotakBuilder.setTitle("Notice");
                    kotakBuilder.setMessage("Are You Sure to Create Order?");
                    kotakBuilder.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    submitSalesOrder();
                                }
                            });
                    kotakBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = kotakBuilder.create();
                    dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_white));
                    dialog.show();
                }
                else
                {
                    Toast.makeText(getActivity(), "Please Choose Customer", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return rootView;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 20: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent(getActivity(), ScannerActivity.class);
                    startActivityForResult(i, 95);
                } else {

                }
            }
        }
    }

    public void setAdapter()
    {
        cartModels.clear();
        cartModels.addAll(helper.getCartOrder());
        if(adapter == null) {
            adapter = new CartOrderAdapter(getActivity(), cartModels, new CartOrderAdapter.OnItemClickListener() {
                @Override
                public void onClick(CartOrderModel item) {
                    Intent i = new Intent(getActivity(), SalesOrderProductActivity.class);
                    i.putExtra("item_id", item.getItemId());
                    i.putExtra("mode", 1);
                    startActivityForResult(i, 100);
                }

                @Override
                public void onDeleteClick(CartOrderModel item) {

                }

                @Override
                public void onChangeAction(CartOrderModel item) {

                }
            });
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
        setDisplay();
    }

    private void setDisplay()
    {
        txtSubtotal.setText(Tools.convertMoney(getActivity(), helper.getTotalInCart()));
        btnPay.setText("Total " + Tools.convertMoney(getActivity(), helper.getTotalInCart()));
        if(helper.getTotalInCart() > 0)
        {
            btnPay.setEnabled(true);
        }
    }

    @Override
    public void onPause() {
        Tools.dismissKeyboard(getActivity());
        super.onPause();
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        getItemMasterByIdHana(s.toUpperCase());
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    public void getItemMasterByIdHana(final String iid)
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Search Item..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.getItemId(iid, Constant.SELECT_ITEM, cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        final Realm realm = Realm.getDefaultInstance();
                        res = new String(response.body().bytes());
                        JSONObject item = new JSONObject(res);
                        final ItemMaster itemMaster = new ItemMaster();
                        itemMaster.setItemNo(item.getString("ItemCode"));
                        itemMaster.setItemDesc(item.getString("ItemName"));
                        itemMaster.setForeignName(item.getString("ForeignName"));
                        itemMaster.setItemGroupId(item.getInt("ItemsGroupCode"));
                        itemMaster.setItemTypeId(item.getString("ItemType").equalsIgnoreCase("ititems") ? 1 :
                                item.getString("ItemType").equalsIgnoreCase("itlabor") ? 2 : 3);
                        itemMaster.setBarcode(item.getString("BarCode"));
                        JSONArray itemprice = item.getJSONArray("ItemPrices");
                        realm.beginTransaction();
                        RealmResults<ItemPrice> itemPriceRealmResults = realm.where(ItemPrice.class)
                                .equalTo("itemId", item.getString("ItemCode")).findAll();
                        itemPriceRealmResults.deleteAllFromRealm();
                        realm.commitTransaction();
                        for(int j = 0 ; j < itemprice.length() ; j++)
                        {
                            ItemPrice lastitemprice = realm.where(ItemPrice.class).findAll()
                                    .sort("priceId", Sort.DESCENDING).first();
                            JSONObject price = itemprice.getJSONObject(j);
                            if(!price.getString("Price").equalsIgnoreCase("null"))
                            {
                                realm.beginTransaction();
                                ItemPrice itemPrice = new ItemPrice();
                                itemPrice.setPriceId(lastitemprice == null ? 1 : lastitemprice.getPriceId() + 1);
                                itemPrice.setPriceList(price.getInt("PriceList"));
                                itemPrice.setPrice(price.getInt("Price"));
                                itemPrice.setCurrency(price.getString("Currency"));
                                itemPrice.setItemId(item.getString("ItemCode"));
                                realm.copyToRealmOrUpdate(itemPrice);
                                realm.commitTransaction();
                            }
                        }
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMaster);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                Intent i = new Intent(getActivity(), SalesOrderProductActivity.class);
                                i.putExtra("item_id", itemMaster.getItemNo());
                                i.putExtra("mode", 0);
                                startActivityForResult(i, 100);
                            }
                        });
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if(response.code() == 404)
                {
                    Toast.makeText(getActivity(), "Not Found", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
                    realmHelper.doLoginHana();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void dismissProgressDialog() {
        if(!getActivity().isFinishing() && progressDialog != null && progressDialog.isShowing()) {
            if(progressDialog.isShowing())
                progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        item = menu.findItem(R.id.action_search);
        item.setVisible(true);
        final MenuItem cart = menu.findItem(R.id.action_cart);
        cart.setVisible(false);
        searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(this);
        item.setOnActionExpandListener( new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
//                MainActivity.isFromSearch = true;
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                //searchView.setQuery("", true);
//                MainActivity.isFromSearch = false;
                return true; // Return true to collapse action view

            }
        });
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == getActivity().RESULT_OK)
        {
            if(requestCode == 95)
            {
                getBusinessPartnerMasterByIdHana(data.getExtras().getString("content"));
            }
            else if(requestCode == 100)
            {
                MenuItem searchMenuItem = item;
                if (searchMenuItem != null) {
                    searchMenuItem.collapseActionView();
                }
                setAdapter();
            }
        }
    }

    public void getBusinessPartnerMasterByIdHana(final String bpid)
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Search Customer..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.getBusinessPartnerById(bpid, Constant.SELECT_CUSTOMER, cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        res = new String(response.body().bytes());
                        JSONObject item = new JSONObject(res);
                        session.setKeyCustomer(item.getString("CardCode"));
                        txtCustomer.setText(item.getString("CardName"));
                        imgCancel.setVisibility(View.VISIBLE);
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if(response.code() == 404)
                {
                    Toast.makeText(getActivity(), "Not Found", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
                    realmHelper.doLoginHana();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void submitSalesOrder()
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Submit Payment..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        RealmResults<CartOrder> carts = realm.where(CartOrder.class).findAll();
        String jsonJournalDetail = "";
        final long id = Tools.dapatkanId();
        try
        {
            JSONArray jounralArr = new JSONArray();
            for(int i = 0 ; i < carts.size() ; i++)
            {
                JSONObject journalObj = new JSONObject();
                journalObj.put("itemId", carts.get(i).getItemId());
                journalObj.put("itemName", carts.get(i).getItemDesc());
                journalObj.put("itemQty", carts.get(i).getItemQty());
                journalObj.put("notes", carts.get(i).getRemarks());
                journalObj.put("itemBatch", carts.get(i).getItemBatch());
                journalObj.put("status", "Check Stock");
                jounralArr.put(journalObj);
            }
            jsonJournalDetail = jounralArr.toString();
        }catch (JSONException ex) {
            ex.printStackTrace();
        }
        Call<SubmitSalesOrderResponse> call = api.submitSalesOrder(id + "", helper.getTotalInCart(),
                helper.getTotalItemInCart(),
                Util.getDate(), Util.getTime(), session.getKeyUserId(), session.getKeyCustomer(),
                "Check Stock", jsonJournalDetail);
        call.enqueue(new Callback<SubmitSalesOrderResponse>() {
            @Override
            public void onResponse(Call<SubmitSalesOrderResponse> call, Response<SubmitSalesOrderResponse> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    if(response.body().getResult().equalsIgnoreCase("ok"))
                    {
                        realm.beginTransaction();
                        SalesOrder journal = new SalesOrder();
                        journal.setSalesOrderId(id);
                        journal.setSalesOrderNum(id + "");
                        journal.setTotal(helper.getTotalInCart());
                        journal.setRowCount(helper.getTotalItemInCart());
                        journal.setTrxDate(Util.getDate());
                        journal.setTrxTime(Util.getTime());
                        journal.setUserId(session.getKeyUserId());
                        journal.setCustomerId(session.getKeyCustomer());
                        journal.setStatus("Check Stock");
                        realm.copyToRealmOrUpdate(journal);
                        RealmResults<CartOrder> carts = realm.where(CartOrder.class).findAll();
                        for(int i = 0 ; i < carts.size() ; i++)
                        {
                            SalesOrderDetail journalDetail = new SalesOrderDetail();
                            journalDetail.setSalesOrderDetailId(Tools.dapatkanId());
                            journalDetail.setSalesOrderId(id);
                            journalDetail.setItemId(carts.get(i).getItemId());
                            journalDetail.setItemName(carts.get(i).getItemDesc());
                            journalDetail.setItemQty(carts.get(i).getItemQty());
                            journalDetail.setNotes(carts.get(i).getRemarks());
                            journalDetail.setStatus("Check Stock");
                            journalDetail.setItemBatch(carts.get(i).getItemBatch());
                            realm.copyToRealmOrUpdate(journalDetail);
                        }
                        carts.deleteAllFromRealm();
                        realm.commitTransaction();
                        session.setKeyCustomer("0");
                        Intent i = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                    }
                }
            }

            @Override
            public void onFailure(Call<SubmitSalesOrderResponse> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }
}
