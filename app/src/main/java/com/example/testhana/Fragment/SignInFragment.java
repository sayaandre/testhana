package com.example.testhana.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.testhana.Activity.CheckInOutActivity;
import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.Constant;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Helper.Util;
import com.example.testhana.Models.Realm.LocationCheckIn;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.Response.SubmitAbsenceResponse;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    Unbinder unbinder;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    private GoogleMap mMap;
    MapView mapView;
    Realm realm;
    SessionManagement session;
    ApiService api;
    ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        realm = Realm.getDefaultInstance();
        session = CoreApplication.getInstance().getSession();
        api = ApiClient.getClient().create(ApiService.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_sign_in, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        mapView = (MapView) rootView.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
        return rootView;
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
//Showing Current Location Marker on Map
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(latLng);
        LocationManager locationManager = (LocationManager)
                getActivity().getSystemService(Context.LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(new Criteria(), true);
        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location locations = locationManager.getLastKnownLocation(provider);
        List<String> providerList = locationManager.getAllProviders();
        if (null != locations && null != providerList && providerList.size() > 0) {
            double longitude = locations.getLongitude();
            double latitude = locations.getLatitude();
            Geocoder geocoder = new Geocoder(getActivity(),
                    Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(latitude,
                        longitude, 1);
                if (null != listAddresses && listAddresses.size() > 0) {
                    String state = listAddresses.get(0).getAdminArea();
                    String country = listAddresses.get(0).getCountryName();
                    String subLocality = listAddresses.get(0).getSubLocality();
//                    markerOptions.title("" + latLng + "," + subLocality + "," + state
//                            + "," + country);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
//        mCurrLocationMarker = mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(18));
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                    this);
        }
        LocationCheckIn locationCheckIn = realm.where(LocationCheckIn.class)
                .equalTo("locationId", session.getKeyLocationId())
                .findFirst();
        if(locationCheckIn != null)
        {
            float[] results = new float[1];
            Location.distanceBetween(Double.parseDouble(session.getKeyLatitude()), Double.parseDouble(session.getKeyLongitude()),
                    mLastLocation.getLatitude(), mLastLocation.getLongitude(), results);
            float distanceInMeters = results[0];
            boolean isWithin20m = distanceInMeters < Constant.RADIUS;
            String message = "";
            if(isWithin20m)
            {
                Bundle extras = location.getExtras();
                boolean isMockLocation = extras != null && extras.getBoolean(FusedLocationProviderApi.KEY_MOCK_LOCATION, false);
                if(!isMockLocation)
                {
                    message = "Check in success in " + locationCheckIn.getLocationName();
                    if(locationCheckIn != null)
                    {
                        submitAbsence(locationCheckIn.getLocationId());
                    }
                }
                else
                {
                    message = "Please Disable Mock Location..";
                }
            }
            else
            {
                message = locationCheckIn.getLocationName() + " is not in radius 20m!";
            }
            AlertDialog.Builder kotakBuilder = new AlertDialog.Builder(getActivity(),
                    R.style.AppCompatAlertDialogStyle);
            kotakBuilder.setIcon(android.R.drawable.ic_dialog_alert);
            kotakBuilder.setTitle("Notice");
            kotakBuilder.setMessage(message);
            kotakBuilder.setPositiveButton("Close",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            AlertDialog dialog = kotakBuilder.create();
            dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_white));
            dialog.show();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
        RealmResults<LocationCheckIn> locationCheckIns = realm.where(LocationCheckIn.class).findAll();
        for(int i = 0 ; i < locationCheckIns.size() ; i++)
        {
            LatLng ptvisintech = new LatLng(locationCheckIns.get(i).getLatitude(), locationCheckIns.get(i).getLongitude());
            googleMap.addMarker(new MarkerOptions().position(ptvisintech)
                    .title(locationCheckIns.get(i).getLocationName()));
            googleMap.addCircle(new CircleOptions()
                    .center(new LatLng(ptvisintech.latitude, ptvisintech.longitude))
                    .radius(Constant.RADIUS)
                    .strokeColor(Color.RED));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem item = menu.findItem(R.id.action_checkin);
        item.setVisible(true);
        final MenuItem cart = menu.findItem(R.id.action_cart);
        cart.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_checkin) {
//            Intent i = new Intent(getActivity(), LocationCheckInListActivity.class);
//            startActivityForResult(i, 100);
            Intent i = new Intent(getActivity(), CheckInOutActivity.class);
            startActivityForResult(i, 100);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == 100)
            {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                        mLocationRequest, this);
            }
        }
    }

    public void submitAbsence(final int locid)
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Submit Absence..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        Call<SubmitAbsenceResponse> call = api.submitAbsence(Util.getDate() + " " + Util.getTime(), locid);
        call.enqueue(new Callback<SubmitAbsenceResponse>() {
            @Override
            public void onResponse(Call<SubmitAbsenceResponse> call, final Response<SubmitAbsenceResponse> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    if(response.body().getResult().equalsIgnoreCase("ok"))
                    {
                        LocationCheckIn locationCheckIn = realm.where(LocationCheckIn.class)
                                .equalTo("locationId", locid)
                                .findFirst();
                        if(locationCheckIn != null)
                        {
                            realm.beginTransaction();
                            locationCheckIn.setCheckIn(true);
                            realm.commitTransaction();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SubmitAbsenceResponse> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void dismissProgressDialog() {
        if(!getActivity().isFinishing() && progressDialog != null && progressDialog.isShowing()) {
            if(progressDialog.isShowing())
                progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        super.onPause();
        onDestroyView();
    }
}
