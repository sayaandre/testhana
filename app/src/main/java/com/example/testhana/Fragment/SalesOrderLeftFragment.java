package com.example.testhana.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testhana.Activity.ChooseCustomerActivity;
import com.example.testhana.Activity.SalesOrderProductActivity;
import com.example.testhana.Adapter.SalesOrderAdapter;
import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.CartHelper;
import com.example.testhana.Helper.Constant;
import com.example.testhana.Helper.ItemMasterHelper;
import com.example.testhana.Helper.PaginationScrollListener;
import com.example.testhana.Helper.RealmHelper;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Helper.Tools;
import com.example.testhana.MainActivity;
import com.example.testhana.Models.Model.ItemMasterModel;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.Models.Realm.ItemPrice;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiServiceHana;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesOrderLeftFragment extends Fragment implements SearchView.OnQueryTextListener{

    @BindView(R.id.txtDataFound)
    TextView txtDataFound;
    private SwipeRefreshLayout swipeLayout;
    SwipeRefreshLayout.OnRefreshListener refreshListener;
    RecyclerView rvItem;
    private SalesOrderAdapter adapter;
    Realm realm;
    List<ItemMasterModel> itemMasterModelList;
    ItemMasterHelper helper;
    CartHelper cartHelper;
    Unbinder unbinder;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private boolean isNotFound = true;
    private int TOTAL_PAGES = 10;
    private int currentPage = 1;
    SessionManagement session;
    ApiServiceHana apiHana;
    String tampCurr = "0", tampPrev = "0", tampNext = "0";
    RealmHelper realmHelper;
    ProgressDialog progressDialog;
    SearchView searchView;
    MenuItem item;
    boolean tabletSize = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = CoreApplication.getInstance().getSession();
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        realm = Realm.getDefaultInstance();
        realmHelper = new RealmHelper();
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_sales_order_left, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        swipeLayout = rootView.findViewById(R.id.swipeLayout);
        refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getItemMasterHana(session.getKeyCurrItem());
            }
        };
        Tools.onCreateSwipeToRefresh(swipeLayout);
        swipeLayout.setOnRefreshListener(refreshListener);
        realm = Realm.getDefaultInstance();
        itemMasterModelList = new ArrayList<>();
        rvItem = rootView.findViewById(R.id.rvItemMaster);
        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        helper = new ItemMasterHelper(realm);
        cartHelper = new CartHelper(getActivity());
        rvItem.setLayoutManager(mLayoutManager);
        rvItem.addItemDecoration(new GridSpacingItemDecoration(2, 0, true));
        rvItem.setItemAnimator(new DefaultItemAnimator());
//        getItemMasterHana("0");
        setAdapter(currentPage);
        rvItem.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                rvItem.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.addLoadingFooter();
                    }
                });
                /*if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;*/
                // mocking network delay for API call
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPage();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        return rootView;
    }

    private void setAdapter(int page)
    {
        itemMasterModelList.clear();
        itemMasterModelList.addAll(helper.getItemMasterSales(page));
        if(itemMasterModelList.size() > 0)
        {
            txtDataFound.setVisibility(View.GONE);
        }
        else
        {
            txtDataFound.setVisibility(View.VISIBLE);
        }
        if(adapter == null) {
            adapter = new SalesOrderAdapter(getActivity(), itemMasterModelList, new SalesOrderAdapter.OnItemClickListener() {
                @Override
                public void onClick(ItemMasterModel item) {
//                    Cart cart = realm.where(Cart.class).equalTo("itemId", item.getItemNo()).findFirst();
//                    if(cart != null)
//                    {
//                        int quantity = cart.getItemQty() + 1;
//                        cartHelper.addCart(item.getItemNo(), item.getForeignName(), quantity, cart.getRemarks(),
//                                item.getUnitPrice() * quantity, item.getUnitPrice());
//                        Activity act = getActivity();
//                        if (act instanceof MainActivity) {
//                            ((MainActivity) act).getCount();
//                        }
//                    }
//                    else
//                    {
//                        Intent i = new Intent(getActivity(), SalesOrderProductActivity.class);
//                        i.putExtra("item_id", item.getItemId());
//                        i.putExtra("mode", 0);
//                        startActivityForResult(i, 100);
//                    }
                    Intent i = new Intent(getActivity(), SalesOrderProductActivity.class);
                    i.putExtra("item_id", item.getItemNo());
                    i.putExtra("mode", 0);
                    startActivityForResult(i, 100);
                }
            });
            rvItem.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
        if(isLoading) {
            adapter.removeLoadingFooter();
            isLoading = false;
        }
    }

    private void loadNextPage() {
        getItemMasterHana(session.getKeyNextItem());
    }

    public void getItemMasterHana(final String paging)
    {
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call;
        if(paging.equals("0")) {
            call = apiHana.getFirstItems(Constant.SELECT_ITEM, cookie);
        }
        else {
            call = apiHana.getItems(paging,Constant.SELECT_ITEM,  cookie);
        }
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                swipeLayout.setRefreshing(false);
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        final Realm realm = Realm.getDefaultInstance();
                        res = new String(response.body().bytes());
                        JSONObject obj = new JSONObject(res);
                        JSONArray value = obj.getJSONArray("value");
                        if(!obj.isNull("odata.nextLink"))
                        {
                            String fullstr = obj.getString("odata.nextLink");
                            String substr = fullstr.substring(fullstr.indexOf("skip="));
                            String[] nextlink = substr.replace("skip=", "").split("&");
                            session.setKeyNextItem(nextlink[0]);
                        }
                        final List<ItemMaster> itemMasters = new ArrayList<>();
                        for(int i = 0 ; i < value.length() ; i++)
                        {
                            JSONObject item = value.getJSONObject(i);
                            ItemMaster itemMaster = new ItemMaster();
                            itemMaster.setItemId(i + 1);
                            itemMaster.setItemNo(item.getString("ItemCode"));
                            itemMaster.setItemDesc(item.getString("ItemName"));
                            itemMaster.setForeignName(item.getString("ForeignName"));
                            itemMaster.setItemGroupId(item.getInt("ItemsGroupCode"));
                            JSONArray itemwarehouse = item.getJSONArray("ItemWarehouseInfoCollection");
                            int stock = 0;
                            for(int j = 0 ; j < itemwarehouse.length() ; j++)
                            {
                                JSONObject warehouse = itemwarehouse.getJSONObject(j);
                                stock = stock + warehouse.getInt("InStock");
                            }
                            itemMaster.setStock(stock);
                            itemMaster.setItemTypeId(item.getString("ItemType").equalsIgnoreCase("ititems") ? 1 :
                                    item.getString("ItemType").equalsIgnoreCase("itlabor") ? 2 : 3);
                            itemMaster.setBarcode(item.getString("BarCode"));
                            itemMasters.add(itemMaster);
                            JSONArray itemprice = item.getJSONArray("ItemPrices");
                            realm.beginTransaction();
                            RealmResults<ItemPrice> itemPriceRealmResults = realm.where(ItemPrice.class)
                                    .equalTo("itemId", item.getString("ItemCode")).findAll();
                            itemPriceRealmResults.deleteAllFromRealm();
                            realm.commitTransaction();
                            for(int j = 0 ; j < itemprice.length() ; j++)
                            {
                                ItemPrice lastitemprice = realm.where(ItemPrice.class).findAll()
                                        .sort("priceId", Sort.DESCENDING).first();
                                JSONObject price = itemprice.getJSONObject(j);
                                if(!price.getString("Price").equalsIgnoreCase("null"))
                                {
                                    realm.beginTransaction();
                                    ItemPrice itemPrice = new ItemPrice();
                                    itemPrice.setPriceId(lastitemprice == null ? 1 : lastitemprice.getPriceId() + 1);
                                    itemPrice.setPriceList(price.getInt("PriceList"));
                                    itemPrice.setPrice(price.getInt("Price"));
                                    itemPrice.setCurrency(price.getString("Currency"));
                                    itemPrice.setItemId(item.getString("ItemCode"));
                                    realm.copyToRealmOrUpdate(itemPrice);
                                    realm.commitTransaction();
                                }
                            }
                        }
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                setAdapter(currentPage);
                            }
                        });
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    realmHelper.doLoginHana();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                swipeLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        getItemMasterByIdHana(s.toUpperCase());
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    public void getItemMasterByIdHana(final String iid)
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Search Item..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.getItemId(iid, Constant.SELECT_ITEM, cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        final Realm realm = Realm.getDefaultInstance();
                        res = new String(response.body().bytes());
                        JSONObject item = new JSONObject(res);
                        final ItemMaster itemMaster = new ItemMaster();
                        itemMaster.setItemNo(item.getString("ItemCode"));
                        itemMaster.setItemDesc(item.getString("ItemName"));
                        itemMaster.setForeignName(item.getString("ForeignName"));
                        itemMaster.setItemGroupId(item.getInt("ItemsGroupCode"));
                        itemMaster.setItemTypeId(item.getString("ItemType").equalsIgnoreCase("ititems") ? 1 :
                                item.getString("ItemType").equalsIgnoreCase("itlabor") ? 2 : 3);
                        itemMaster.setBarcode(item.getString("BarCode"));
                        JSONArray itemprice = item.getJSONArray("ItemPrices");
                        realm.beginTransaction();
                        RealmResults<ItemPrice> itemPriceRealmResults = realm.where(ItemPrice.class)
                                .equalTo("itemId", item.getString("ItemCode")).findAll();
                        itemPriceRealmResults.deleteAllFromRealm();
                        realm.commitTransaction();
                        for(int j = 0 ; j < itemprice.length() ; j++)
                        {
                            ItemPrice lastitemprice = realm.where(ItemPrice.class).findAll()
                                    .sort("priceId", Sort.DESCENDING).first();
                            JSONObject price = itemprice.getJSONObject(j);
                            if(!price.getString("Price").equalsIgnoreCase("null"))
                            {
                                realm.beginTransaction();
                                ItemPrice itemPrice = new ItemPrice();
                                itemPrice.setPriceId(lastitemprice == null ? 1 : lastitemprice.getPriceId() + 1);
                                itemPrice.setPriceList(price.getInt("PriceList"));
                                itemPrice.setPrice(price.getInt("Price"));
                                itemPrice.setCurrency(price.getString("Currency"));
                                itemPrice.setItemId(item.getString("ItemCode"));
                                realm.copyToRealmOrUpdate(itemPrice);
                                realm.commitTransaction();
                            }
                        }
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMaster);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                Intent i = new Intent(getActivity(), SalesOrderProductActivity.class);
                                i.putExtra("item_id", itemMaster.getItemNo());
                                i.putExtra("mode", 0);
                                startActivityForResult(i, 100);
                            }
                        });
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if(response.code() == 404)
                {
                    Toast.makeText(getActivity(), "Not Found", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
                    realmHelper.doLoginHana();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void dismissProgressDialog() {
        if(!getActivity().isFinishing() && progressDialog != null && progressDialog.isShowing()) {
            if(progressDialog.isShowing())
                progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem itemcustomer = menu.findItem(R.id.action_customer);
        itemcustomer.setVisible(false);
        item = menu.findItem(R.id.action_search);
        item.setVisible(true);
        searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(this);
        item.setOnActionExpandListener( new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
//                MainActivity.isFromSearch = true;
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                //searchView.setQuery("", true);
//                MainActivity.isFromSearch = false;
                return true; // Return true to collapse action view

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_customer) {
            Intent i = new Intent(getActivity(), ChooseCustomerActivity.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == 100)
            {
//                RealmResults<ItemPrice> itemPrices = realm.where(ItemPrice.class).equalTo("itemId", data.getStringExtra("itemno")).findAll();
//                cartHelper.addCartFromHome(data.getStringExtra("itemno"), data.getStringExtra("itemdesc"), 1, "",
//                        itemPrices.size() >= 1 ? itemPrices.first().getPrice() : 0 * 1, itemPrices.size() >= 1 ? itemPrices.first().getPrice() : 0, 0);
                Activity act = getActivity();
                if (act instanceof MainActivity) {
                    ((MainActivity) act).getCount();
                }
                if(tabletSize)
                {
                    CartFragment f = (CartFragment) getFragmentManager().findFragmentByTag("salesright");
                    f.setAdapter();
                }
                setAdapter(currentPage);
            }
        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onDestroyView() {
        if(realm != null && !realm.isClosed())
            realm.close();
        super.onDestroyView();
//        unbinder.unbind();
    }

    @Override
    public void onPause() {
        super.onPause();
        onDestroyView();
    }
}
