package com.example.testhana.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;


import com.example.testhana.Activity.HistoryDetailActivity;
import com.example.testhana.Adapter.HistoryAdapter;
import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.JournalHelper;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Helper.Tools;
import com.example.testhana.Helper.Util;
import com.example.testhana.Models.Model.JournalModel;
import com.example.testhana.Models.Realm.Journal;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.Response.GetJournalResponse;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryListFragment extends Fragment {

    private static final String TAG = "Home";
    private Realm realm;
    private RecyclerView recyclerView;
    private TextView tvNotFound;
    private JournalHelper helper;
    private ArrayList<JournalModel> header;
    private SwipeRefreshLayout swipeLayout;
    private HistoryAdapter adapter;
    int selected = 0;
    boolean tabletSize = false;
    SessionManagement session;
    long count = 0;
    SwipeRefreshLayout.OnRefreshListener refreshListener;
    String userCashierId = "", zDay = "", paymentType = "", storeId = "";
    ApiService api;

    public HistoryListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = CoreApplication.getInstance().getSession();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history_list, container, false);
        header = new ArrayList<>();
        helper = new JournalHelper(getActivity());
        realm = Realm.getDefaultInstance();
        api = ApiClient.getClient().create(ApiService.class);
        if(savedInstanceState != null) {
            selected = savedInstanceState.getInt("selected", -1);
        }
        final Bundle bundle = getArguments();
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        swipeLayout = rootView.findViewById(R.id.swipeLayout);
        if(bundle != null && bundle.containsKey("summary")) {
            userCashierId = bundle.getInt("userCashierId") + "";
            zDay = bundle.getString("zday");
            paymentType = bundle.getString("paymentType");
            storeId = bundle.getString("storeId");
        }
        Tools.onCreateSwipeToRefresh(swipeLayout);
        refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(true);
                getJournal();
            }
        };
        swipeLayout.setOnRefreshListener(refreshListener);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        recyclerView = rootView.findViewById(R.id.rvHistory);
        tvNotFound = rootView.findViewById(R.id.textViewNotFound);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        setRecyclerView();

        return rootView;
    }

    public void getJournal()
    {
        Call<GetJournalResponse> call = api.getJournal(Util.getDate(),session.getKeyUserId());
        call.enqueue(new Callback<GetJournalResponse>() {
            @Override
            public void onResponse(Call<GetJournalResponse> call, final Response<GetJournalResponse> response) {
                swipeLayout.setRefreshing(false);
                if(response.isSuccessful())
                {
                    if(response.body().getResult().equalsIgnoreCase("ok"))
                    {
                        final Realm mRealm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        RealmResults<Journal> journals = realm.where(Journal.class).findAll();
                        journals.deleteAllFromRealm();
                        realm.commitTransaction();
                        final List<Journal> itemMasters = response.body().getJournals();
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                setRecyclerView();
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<GetJournalResponse> call, Throwable t) {
                swipeLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void setRecyclerView(){
        header.clear();
        header.addAll(helper.getHistory());
        if(header.size() > 0)
        {
            tvNotFound.setVisibility(View.GONE);
        }

        if(adapter == null) {
            adapter = new HistoryAdapter(getActivity(), header, new HistoryAdapter.OnItemClickListener() {
                @Override
                public void onClick(final JournalModel item, int position) {
                    if (tabletSize) {
                        HistoryDetailFragment f = (HistoryDetailFragment) getFragmentManager().findFragmentByTag("historydetail");
                        f.getJournalDetail(item.getJournalId());
//                        f.setRecyclerView(item.getJournalId());
                    }
                    else
                    {
                        Intent i = new Intent(getActivity(), HistoryDetailActivity.class);
                        i.putExtra("journalId", item.getJournalId());
                        startActivity(i);
                    }
                }
            });
            recyclerView.setAdapter(adapter);
        } else {
            adapter.updateData(header);
            adapter.notifyDataSetChanged();
        }
        swipeLayout.setRefreshing(false);
    }
}
