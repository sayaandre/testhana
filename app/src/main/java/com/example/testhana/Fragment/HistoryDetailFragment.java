package com.example.testhana.Fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testhana.Activity.CustomerDetailActivity;
import com.example.testhana.Activity.HistoryDetailActivity;
import com.example.testhana.Adapter.HistoryDetailAdapter;
import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.Constant;
import com.example.testhana.Helper.JournalHelper;
import com.example.testhana.Helper.RealmHelper;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Helper.Tools;
import com.example.testhana.Models.Model.JournalDetailModel;
import com.example.testhana.Models.Realm.CustomerMaster;
import com.example.testhana.Models.Realm.Journal;
import com.example.testhana.Models.Realm.JournalDetail;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.ApiServiceHana;
import com.example.testhana.Rest.Response.GetJournalDetailResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryDetailFragment extends Fragment {

    private RecyclerView recyclerView;
    private JournalHelper helper;
    private TextView txtSubtotal, txtVATAmount, txtTotal, txtPayType, btnCustomer, txtPayCash, txtPayOVO, txtPayMobey,
            txtPayUnikas, txtPayOther, txtPayAmount, txtPayAmountCash, txtPayAmountOVO, txtPayAmountMobey, txtPayAmountUnikas,
            txtPayAmountOther, txtChange, labelChange, txtDiscountCashier, txtDiscountTenant, txtDiscountCustom, txtTaxName,
            txtTaxValue, txtRewardRedeem, txtRewardEarnRedeem, txtReceiptDateTimeRight;
    private Button btnIssueReceipt, btnIssueRefund, btnCustomerRight;
    private TextView txtReceiptNumber, txtReceiptDateTime, txtNoReceipt, txtRefCode, txtCardNo, txtStatus, lblRefCode;
    private LinearLayout llCustomer, llChange,llSubtotal, llDiscountCashier, llDiscountTenant, llDiscountCustom, llVAT, llRef, llCard, llRewardRedeem, llRewardEarnRedeem, llNotSplit, llSplit, llPayCash, llPayOVO, llPayMobey, llPayUnikas, llPayOther;
    private RelativeLayout llReceipt;
    private HistoryDetailAdapter adapter;
    @BindView(R.id.btnChangeStatus)
    Button btnChangeStatus;
    @BindView(R.id.cardRefund)
    CardView cardRefund;
    @BindView(R.id.txtAmountRefund) TextView txtAmountRefund;
    @BindView(R.id.txtReceiptRefund) TextView txtReceiptRefund;
    @BindView(R.id.txtRefundDateTime) TextView txtRefundDateTime;
    @BindView(R.id.txtReasonRefund) TextView txtReasonRefund;
    @BindView(R.id.txtTableNum) TextView txtTableNum;
    @BindView(R.id.btnScan) Button btnScan;
    @BindView(R.id.txtWaiter) TextView txtWaiter;
    @BindView(R.id.rvPayment)
    RecyclerView rvPayment;
    List<JournalDetailModel> results = null;
    private Journal header;
    long trxNo = 0;
    String orderId = "";
    int mode = 0;
    ApiService apiService;
    boolean isSummary = false, isCustomer = false;
    public boolean issplit = false;
    Bundle bundle;
    ApiServiceHana apiHana;
    RealmHelper realmHelper;
    Realm realm;
    SessionManagement session;
    ProgressDialog progressDialog;
    Unbinder unbinder;

    public HistoryDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = CoreApplication.getInstance().getSession();
        realm = Realm.getDefaultInstance();
        apiService = ApiClient.getClient().create(ApiService.class);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_history_detail, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        apiService = ApiClient.getClient().create(ApiService.class);
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        realmHelper = new RealmHelper();
        helper = new JournalHelper(getActivity());
        bundle = getActivity().getIntent().getExtras();
        results = new ArrayList<JournalDetailModel>();
//        trxNo = bundle.getLong("journalId");
        recyclerView = rootView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        lblRefCode = rootView.findViewById(R.id.lblRefCode);
        txtSubtotal = rootView.findViewById(R.id.txtSubtotal);
        txtVATAmount = rootView.findViewById(R.id.VATAmount);
        txtTotal = rootView.findViewById(R.id.txtTotal);
        txtPayAmount = rootView.findViewById(R.id.txtPayAmount);
        txtPayType = rootView.findViewById(R.id.txtPayWith);
        txtPayAmountCash = rootView.findViewById(R.id.txtPayAmountCash);
        txtPayCash = rootView.findViewById(R.id.txtPayCash);
        txtPayAmountOVO = rootView.findViewById(R.id.txtPayAmountOVO);
        txtPayOVO = rootView.findViewById(R.id.txtPayOVO);
        txtPayAmountMobey = rootView.findViewById(R.id.txtPayAmountMobey);
        txtPayMobey = rootView.findViewById(R.id.txtPayMobey);
        txtPayAmountUnikas = rootView.findViewById(R.id.txtPayAmountUnikas);
        txtPayUnikas = rootView.findViewById(R.id.txtPayUnikas);
        txtPayAmountOther = rootView.findViewById(R.id.txtPayAmountOther);
        txtPayOther = rootView.findViewById(R.id.txtPayOther);
        llNotSplit = rootView.findViewById(R.id.llNotSplit);
        llSplit = rootView.findViewById(R.id.llSplits);
        llPayCash = rootView.findViewById(R.id.llPayCash);
        llPayOVO = rootView.findViewById(R.id.llPayOVO);
        llPayMobey = rootView.findViewById(R.id.llPayMobey);
        llPayUnikas = rootView.findViewById(R.id.llPayUnikas);
        llPayOther = rootView.findViewById(R.id.llPayOther);
        llChange = rootView.findViewById(R.id.llChange);
        btnIssueReceipt = rootView.findViewById(R.id.btnIssueReceipt);
        txtReceiptNumber = rootView.findViewById(R.id.txtReceiptNumber);
        txtReceiptDateTime = rootView.findViewById(R.id.txtReceiptDateTime);
        txtReceiptDateTimeRight = rootView.findViewById(R.id.txtReceiptDateTimeRight);
        btnCustomerRight = rootView.findViewById(R.id.txtCustomerRight);
        llReceipt = rootView.findViewById(R.id.llReceipt);
        txtNoReceipt = rootView.findViewById(R.id.txtNoReceipt);
        txtChange = rootView.findViewById(R.id.txtChangeAmount);
        labelChange = rootView.findViewById(R.id.txtChange);
        txtDiscountCashier = rootView.findViewById(R.id.txtDiscountCashier);
        txtDiscountTenant = rootView.findViewById(R.id.txtDiscountTenant);
        txtDiscountCustom = rootView.findViewById(R.id.txtDiscountCustom);
        txtRefCode = rootView.findViewById(R.id.txtRefCode);
        txtCardNo = rootView.findViewById(R.id.txtCardNo);
        txtTaxName = rootView.findViewById(R.id.txtTaxName);
        txtTaxValue = rootView.findViewById(R.id.txtTaxValue);
        txtRewardRedeem = rootView.findViewById(R.id.txtRewardRedeem);
        txtRewardEarnRedeem = rootView.findViewById(R.id.txtRewardEarnRedeem);
        btnIssueRefund = rootView.findViewById(R.id.btnIssueRefund);

        btnCustomer = rootView.findViewById(R.id.txtCustomer);

        llSubtotal = rootView.findViewById(R.id.llSubTotal);
        llVAT = rootView.findViewById(R.id.llVAT);
        llDiscountCashier = rootView.findViewById(R.id.llDiscountCashier);
        llDiscountTenant = rootView.findViewById(R.id.llDiscountTenant);
        llDiscountCustom = rootView.findViewById(R.id.llDiscountCustom);
        llRewardRedeem = rootView.findViewById(R.id.llRewardRedeem);
        llRewardEarnRedeem = rootView.findViewById(R.id.llRewardEarnRedeem);
        llRef = rootView.findViewById(R.id.llRef);
        llCard = rootView.findViewById(R.id.llCard);
        llCustomer = rootView.findViewById(R.id.llCustomer);

        btnCustomerRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBusinessPartnerMasterByIdHana(btnCustomerRight.getText().toString());
            }
        });

        rvPayment.setLayoutManager(new LinearLayoutManager(getActivity()));
        header = realm.where(Journal.class).equalTo("journalId", trxNo).findFirst();
        getJournalDetail(trxNo);
        return rootView;
    }

    public void getBusinessPartnerMasterByIdHana(final String bpid)
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Search Customer..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.getBusinessPartnerById(bpid, Constant.SELECT_CUSTOMER, cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        res = new String(response.body().bytes());
                        JSONObject item = new JSONObject(res);
                        Intent i = new Intent(getActivity(), CustomerDetailActivity.class);
                        i.putExtra("code", item.getString("CardCode"));
                        i.putExtra("name", item.getString("CardName"));
                        i.putExtra("phone", item.getString("Phone1"));
                        i.putExtra("email", item.getString("EmailAddress"));
                        i.putExtra("address", item.getString("Address"));
                        startActivity(i);
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if(response.code() == 404)
                {
                    Toast.makeText(getActivity(), "Not Found", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
                    realmHelper.doLoginHana();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void setDisplayView()
    {
        txtNoReceipt.setVisibility(View.GONE);
        llReceipt.setVisibility(View.VISIBLE);
        txtReceiptNumber.setText("Receipt #" + header.getReceiptNumber() + " / " + header.getDocnum());
        txtReceiptDateTimeRight.setText(header.getTrxDate() + "/" + header.getTrxTime());
        if(header.getCustomerId() != null || !header.getCustomerId().equals(""))
        {
            llCustomer.setVisibility(View.VISIBLE);
            btnCustomerRight.setText(header.getCustomerId());
        }
        txtTotal.setText(Tools.convertMoney(getActivity(), header.getTotal()));
        JournalDetail journalPayment = realm.where(JournalDetail.class)
                .equalTo("journalId", trxNo)
                .equalTo("journalType", "P")
                .findFirst();
        if(journalPayment != null)
        {
            txtPayType.setText(journalPayment.getItemName());
            txtPayAmount.setText(Tools.convertMoney(getActivity(), journalPayment.getItemTotalPrice()));
        }
        JournalDetail journalChange = realm.where(JournalDetail.class)
                .equalTo("journalId", trxNo)
                .equalTo("journalType", "C")
                .findFirst();
        if(journalChange != null)
        {
            llChange.setVisibility(View.VISIBLE);
            txtChange.setText(Tools.convertMoney(getActivity(), journalChange.getItemTotalPrice()));
        }
    }

    public void getJournalDetail(final long trxno)
    {
        Call<GetJournalDetailResponse> call = apiService.getJournalDetail(trxNo);
        call.enqueue(new Callback<GetJournalDetailResponse>() {
            @Override
            public void onResponse(Call<GetJournalDetailResponse> call, final Response<GetJournalDetailResponse> response) {
                if(response.isSuccessful())
                {
                    if(response.body().getResult().equalsIgnoreCase("ok"))
                    {
                        final List<JournalDetail> itemMasters = response.body().getJournalDetails();
                        final Realm mRealm = Realm.getDefaultInstance();
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                header = realm.where(Journal.class).equalTo("journalId", trxno).findFirst();
                                trxNo = trxno;
                                setRecyclerView();
                                setDisplayView();
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<GetJournalDetailResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void setRecyclerView()
    {
        try{
            results.clear();
            results.addAll(helper.getHistoryDetail(trxNo));
        }catch (Exception e){
            e.printStackTrace();
        }
        if (adapter == null) {
            if(results.size() != 0)
            {
                adapter = new HistoryDetailAdapter(getActivity(), results);
                recyclerView.setAdapter(adapter);
            }
        } else {
            adapter.notifyDataSetChanged();
        }
        setDisplayView();
    }

    public void dismissProgressDialog() {
        if(!getActivity().isFinishing() && progressDialog != null && progressDialog.isShowing()) {
            if(progressDialog.isShowing())
                progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
