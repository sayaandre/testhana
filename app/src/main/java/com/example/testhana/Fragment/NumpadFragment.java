package com.example.testhana.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Helper.Util;
import com.example.testhana.R;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NumpadFragment extends Fragment implements View.OnClickListener{
    @BindView(R.id.numpad_price)
    TextView txtNumpadPrice;
    @BindView(R.id.numpad_0)
    RelativeLayout txtNumpad0;
    @BindView(R.id.numpad_1) RelativeLayout txtNumpad1;
    @BindView(R.id.numpad_2) RelativeLayout txtNumpad2;
    @BindView(R.id.numpad_3) RelativeLayout txtNumpad3;
    @BindView(R.id.numpad_4) RelativeLayout txtNumpad4;
    @BindView(R.id.numpad_5) RelativeLayout txtNumpad5;
    @BindView(R.id.numpad_6) RelativeLayout txtNumpad6;
    @BindView(R.id.numpad_7) RelativeLayout txtNumpad7;
    @BindView(R.id.numpad_8) RelativeLayout txtNumpad8;
    @BindView(R.id.numpad_9) RelativeLayout txtNumpad9;
    @BindView(R.id.numpad_c) RelativeLayout txtNumpadC;
    @BindView(R.id.numpad_plus) RelativeLayout txtNumpadPlus;
    @BindView(R.id.etPaymentCash)
    MaterialEditText etPaymentCash;
    @BindView(R.id.radioGroupQC) RadioGroup radioGroupQC;
    @BindView(R.id.btnSubmitPaymentCash) Button submitPayment;

    private Unbinder unbinder;

    SessionManagement session;
    double totalChangeDue = -1;
    double tmp, tmpQC;
    Double amount, subtotal;
    int finRes = 0;
    boolean tabletSize = false;
    boolean isFromOrder = false;
    String intentFilter = "paymentEnabled";

    public NumpadFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private final BroadcastReceiver myBroadcastReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, android.content.Intent intent) {
            submitPayment.setEnabled(true);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_numpad, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        tabletSize = getResources().getBoolean(R.bool.isTablet);

        session = CoreApplication.getInstance().getSession();
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        amount = getArguments().getDouble("total", 0);
        subtotal = getArguments().getDouble("subtotal", 0);
        isFromOrder = getArguments().containsKey("order");

        tmpQC = amount;
        quickCash(amount);
        etPaymentCash.setHint("0");
        etPaymentCash.addTextChangedListener(new NumberTextWatcher2(etPaymentCash));

        etPaymentCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finRes = 0;
                radioGroupQC.clearCheck();
            }
        });

        etPaymentCash.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b){
                    finRes = 0;
                    radioGroupQC.clearCheck();
                }
            }
        });

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(myBroadcastReceiver,
                new IntentFilter(intentFilter));

        txtNumpad0.setOnClickListener(this);
        txtNumpad1.setOnClickListener(this);
        txtNumpad2.setOnClickListener(this);
        txtNumpad3.setOnClickListener(this);
        txtNumpad4.setOnClickListener(this);
        txtNumpad5.setOnClickListener(this);
        txtNumpad6.setOnClickListener(this);
        txtNumpad7.setOnClickListener(this);
        txtNumpad8.setOnClickListener(this);
        txtNumpad9.setOnClickListener(this);
        txtNumpadC.setOnClickListener(this);
        txtNumpadPlus.setOnClickListener(this);
        txtNumpadPrice.addTextChangedListener(new NumberTextWatcher(txtNumpadPrice, getActivity()));

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if(!tabletSize) {
            TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 0f);
            txtNumpadPrice.setPadding(dpToPx(15),dpToPx(5),dpToPx(15),dpToPx(5));
            txtNumpadPrice.setLayoutParams(params);
        }

        if(totalChangeDue < 0) {
            Util.setTextViewEnabled(getActivity(), false, txtNumpadPlus);
            submitPayment.setEnabled(false);
        } else {
            Util.setTextViewEnabled(getActivity(), true, txtNumpadPlus);
            submitPayment.setEnabled(true);
        }

        submitPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inptCash = etPaymentCash.getText().toString();
                if (!inptCash.equals("") && inptCash != null) {
                    Locale current = getResources().getConfiguration().locale;
                    DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(current);
                    DecimalFormat df;
                    df = new DecimalFormat("#,###.##", symbols);
                    df.setDecimalSeparatorAlwaysShown(true);
                    int resin = Integer.parseInt(inptCash.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), ""));
                    if (resin >= tmpQC) {
                        int cashInput = resin;
                        proceedPayment(cashInput);
                    }else{
                        Toast.makeText(getActivity(), "Insufficient Amount", Toast.LENGTH_LONG).show();
                    }
                }else {
                    if (finRes > 0) {
                        proceedPayment(finRes);
                    }else{
                        Toast.makeText(getActivity(), "Please Fill Payment", Toast.LENGTH_LONG).show();
                    }
                }
            }


        });

        return rootView;
    }

    private void proceedPayment(int amount){
        tmpQC = amount;
        totalChangeDue = tmpQC - this.amount;
//        submitPayment.setEnabled(false);
        PaymentFragment f = (PaymentFragment) getFragmentManager().findFragmentByTag("payment");
        f.createOrder("", tmpQC, totalChangeDue, 0.0);
        txtNumpadPrice.setText("0");
    }

    private void quickCash(double amount){
        String input = String.valueOf((int)amount);
        int cash = Integer.parseInt(input);

        int addedAmount = 0, result = 0, result2 = 0, result3 = 0;

        //Lima Ribuan Terdekat

        double desimal1 = (double) cash / 5000;
        int roundUp1 = (int)Math.ceil(desimal1);
        result = roundUp1 * 5000;

        //Puluhan Ribu Terdekat

        double desimal2 = (double) cash / 10000;
        int roundUp2 = (int)Math.ceil(desimal2);
        result2 = roundUp2 * 10000;

        //Lima Puluhan Ribu Terdekat

        double desimal3 = (double) cash / 50000;
        int roundUp3 = (int)Math.ceil(desimal3);
        result3 = roundUp3 * 50000;

        int hasil = cash + addedAmount;

        if (result == result2){
            result = 0;
        }else if (result == result3){
            result = 0;
        }

        if (result2 == result3){
            result2 = 0;
        }

        //Make The View
        RadioGroup.LayoutParams lp = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
        lp.setMargins(2,0,2,0);

        if (result > 0) {
            RadioButton rbQuickCash = new RadioButton(getContext());
            rbQuickCash.setBackground(getResources().getDrawable(R.drawable.button_selection));
            rbQuickCash.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            Locale current = getResources().getConfiguration().locale;
            DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(current);
            DecimalFormat formatter = new DecimalFormat("#,###,###", symbols);
            rbQuickCash.setText(formatter.format(result));

            if (tabletSize){
                rbQuickCash.setPadding(10, 15, 10, 15);
                rbQuickCash.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.font_normal));
                //rbQuickCash.setTextSize(getResources().getDimensionPixelSize(R.dimen.font_very_small));
            }else {
                rbQuickCash.setPadding(10, 15, 10, 15);
                rbQuickCash.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.font_normal));
                //rbQuickCash.setPadding(50, 50, 50, 50);
            }
            rbQuickCash.setTextColor(getResources().getColor(R.color.colorPrimary));
            rbQuickCash.setButtonDrawable(getResources().getDrawable(R.color.zxing_transparent));
            rbQuickCash.setTextColor(getResources().getColorStateList(R.color.btn_payment_color));
            rbQuickCash.setMaxLines(1);
            rbQuickCash.setEllipsize(TextUtils.TruncateAt.END);

            final int ees = result;
            rbQuickCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    proceedPayment(finRes);
                    finRes = ees;
                    etPaymentCash.setText(finRes + "");
                }
            });
            radioGroupQC.addView(rbQuickCash, lp);
        }

        if (result2 > 0){
            RadioButton rbQuickCash = new RadioButton(getContext());
            rbQuickCash.setBackground(getResources().getDrawable(R.drawable.button_selection));
            rbQuickCash.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            Locale current = getResources().getConfiguration().locale;
            DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(current);
            DecimalFormat formatter = new DecimalFormat("#,###,###", symbols);
            rbQuickCash.setText(formatter.format(result2));
            if (tabletSize){
                rbQuickCash.setPadding(10, 15, 10, 15);
                rbQuickCash.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.font_normal));
            }else {
                rbQuickCash.setPadding(10, 15, 10, 15);
                rbQuickCash.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.font_normal));
                //rbQuickCash.setPadding(50, 50, 50, 50);
            }
            rbQuickCash.setTextColor(getResources().getColor(R.color.colorPrimary));
            rbQuickCash.setButtonDrawable(getResources().getDrawable(R.color.zxing_transparent));
            rbQuickCash.setTextColor(getResources().getColorStateList(R.color.btn_payment_color));
            rbQuickCash.setMaxLines(1);
            rbQuickCash.setEllipsize(TextUtils.TruncateAt.END);

            final int ees = result2;
            rbQuickCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    proceedPayment(finRes);
                    finRes = ees;
                    etPaymentCash.setText(finRes + "");
                }
            });
            radioGroupQC.addView(rbQuickCash, lp);
        }

        if (result3 > 0){
            RadioButton rbQuickCash = new RadioButton(getContext());
            rbQuickCash.setBackground(getResources().getDrawable(R.drawable.button_selection));
            rbQuickCash.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            Locale current = getResources().getConfiguration().locale;
            DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(current);
            DecimalFormat formatter = new DecimalFormat("#,###,###", symbols);

            rbQuickCash.setText(formatter.format(result3));
            if (tabletSize){
                rbQuickCash.setPadding(10, 15, 10, 15);
                rbQuickCash.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.font_normal));
            }else {
                rbQuickCash.setPadding(10, 15, 10, 15);
                rbQuickCash.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.font_normal));
                //rbQuickCash.setPadding(50, 50, 50, 50);
            }
            rbQuickCash.setTextColor(getResources().getColor(R.color.colorPrimary));
            rbQuickCash.setButtonDrawable(getResources().getDrawable(R.color.zxing_transparent));
            rbQuickCash.setTextColor(getResources().getColorStateList(R.color.btn_payment_color));
            rbQuickCash.setMaxLines(1);
            rbQuickCash.setEllipsize(TextUtils.TruncateAt.END);

            final int ees = result3;
            rbQuickCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    proceedPayment(finRes);
                    finRes = ees;
                    etPaymentCash.setText(finRes + "");
                }
            });
            radioGroupQC.addView(rbQuickCash, lp);
        }
//        tvHasil.setText("Cash 1. "+String.valueOf(result)+" ; "+String.valueOf(result2)+" ; "+String.valueOf(result3)+" ; ");
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onClick(View v) {
        if(v instanceof RelativeLayout) {
            View tv = ((RelativeLayout) v).getChildAt(0);
            if (tv instanceof TextView) {
                TextView button = (TextView) tv;

                if (button.getText().toString().equalsIgnoreCase("Clear")) {
                    txtNumpadPrice.setText("0");
                } else if (button.getText().toString().equals("OK")) {
                    txtNumpadPrice.setText("0");
                } else {
                    if (txtNumpadPrice.getText().toString().equals("0"))
                        txtNumpadPrice.setText(button.getText().toString());
                    else
                        txtNumpadPrice.append(button.getText().toString());
                }

            }
        }
    }

    public class NumberTextWatcher implements TextWatcher {

        private DecimalFormat df;
        private DecimalFormat dfnd;
        private boolean hasFractionalPart;

        private TextView et;

        public NumberTextWatcher(TextView et, Context context)
        {
            Locale current = getResources().getConfiguration().locale;
            DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(current);
            df = new DecimalFormat("#,###.##", symbols);
            df.setDecimalSeparatorAlwaysShown(true);
            dfnd = new DecimalFormat("#,###", symbols);
            this.et = et;
            hasFractionalPart = false;
        }

        @SuppressWarnings("unused")
        private static final String TAG = "NumberTextWatcher";

        @Override
        public void afterTextChanged(Editable s)
        {
            et.removeTextChangedListener(this);

            try {
                int inilen, endlen;
                inilen = et.getText().length();

                String v = s.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "");
                Number n = df.parse(v);
                int cp = et.getSelectionStart();
                if (hasFractionalPart) {
                    et.setText(df.format(n));
                } else {
                    et.setText(dfnd.format(n));
                }
                endlen = et.getText().length();
                int sel = (cp + (endlen - inilen));
                if (sel > 0 && sel <= et.getText().length()) {
                    // et.setSelection(sel);
                } else {
                    // place cursor at the end?
                    //et.setSelection(et.getText().length() - 1);
                }
            } catch (NumberFormatException nfe) {
                // do nothing?
            } catch (ParseException e) {
                // do nothing?
            }

            et.addTextChangedListener(this);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {
            hasFractionalPart = s.toString().contains(String.valueOf(df.getDecimalFormatSymbols().getDecimalSeparator()));

            if(s.toString().equalsIgnoreCase(""))
            {
                tmp = 0.0 ;
            }else
            {
                String v = s.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "");
                tmp = Double.parseDouble(v);
            }

            int amnt = amount.intValue();
            totalChangeDue = tmp- amnt;
            if(totalChangeDue < 0) {
                Util.setTextViewEnabled(getActivity().getBaseContext(), false, txtNumpadPlus);
            } else {
                Util.setTextViewEnabled(getActivity().getBaseContext(), true, txtNumpadPlus);

            }
        }
    }

    public class NumberTextWatcher2 implements TextWatcher {

        private DecimalFormat df;
        private DecimalFormat dfnd;
        private boolean hasFractionalPart;

        private EditText et;

        public NumberTextWatcher2(EditText et)
        {
            Locale current = getResources().getConfiguration().locale;
            DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(current);
            df = new DecimalFormat("#,###.##", symbols);
            df.setDecimalSeparatorAlwaysShown(true);
            dfnd = new DecimalFormat("#,###", symbols);
            this.et = et;
            hasFractionalPart = false;
        }

        @SuppressWarnings("unused")
        private static final String TAG = "NumberTextWatcher";

        @Override
        public void afterTextChanged(Editable s)
        {
            et.removeTextChangedListener(this);

            try {
                int inilen, endlen;
                inilen = et.getText().length();

                String v = s.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "");
                Number n = df.parse(v);
                int cp = et.getSelectionStart();
                if (hasFractionalPart) {
                    et.setText(df.format(n));
                } else {
                    et.setText(dfnd.format(n));
                }
                endlen = et.getText().length();
                int sel = (cp + (endlen - inilen));
                if (sel > 0 && sel <= et.getText().length()) {
                    et.setSelection(sel);
                } else {
                    // place cursor at the end?
                    et.setSelection(et.getText().length() - 1);
                }
            } catch (NumberFormatException nfe) {
                // do nothing?
            } catch (ParseException e) {
                // do nothing?
            }

            et.addTextChangedListener(this);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {
            hasFractionalPart = s.toString().contains(String.valueOf(df.getDecimalFormatSymbols().getDecimalSeparator()));

            if(s.toString().equalsIgnoreCase(""))
            {
                tmp = 0.0 ;
            }else
            {
                String v = s.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "");
                tmp = Double.parseDouble(v);
            }

            int amnt = amount.intValue();
            totalChangeDue = (int)tmp - amnt;
            if(totalChangeDue < 0) {
                submitPayment.setEnabled(false);
            } else {
                submitPayment.setEnabled(true);
            }
        }
    }

    @Override
    public void onDestroyView() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(myBroadcastReceiver);

        super.onDestroyView();
        unbinder.unbind();
    }
}
