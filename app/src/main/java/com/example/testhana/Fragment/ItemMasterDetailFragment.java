package com.example.testhana.Fragment;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.ItemMasterHelper;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Helper.Tools;
import com.example.testhana.Helper.Util;
import com.example.testhana.Models.Realm.ItemGroupMaster;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.Models.Realm.ItemPrice;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.ApiServiceHana;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.Sort;

public class ItemMasterDetailFragment extends Fragment {

    private String[] itemTypes, itemGroups;ItemMasterHelper helper;
    @BindView(R.id.txtItemNo) EditText txtItemNo;
    @BindView(R.id.txtItemDesc) EditText txtItemDesc;
    @BindView(R.id.txtForeignName) EditText txtItemName;
    @BindView(R.id.txtItemPrice) EditText txtItemPrice;
    @BindView(R.id.txtItemStock) EditText txtItemStock;
    @BindView(R.id.spinnerItemType) Spinner spinnerItemType;
    @BindView(R.id.spinnerItemGroup) Spinner spinnerItemGroup;
    @BindView(R.id.imgItem) ImageButton itemImage;
    protected static final int SELECT_FILE = 2;
    private static final int CROP_FROM_CAMERA = 3;
    private static final int SELECT_CATEGORY = 4;
    String img_str = "";
    private Uri mImageCaptureUri;
    ItemMaster itemMaster;
    Bundle bundle;
    ApiService api;
    ApiServiceHana apiHana;
    Realm realm;
    SessionManagement session;
    ProgressDialog progressDialog;
    Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = CoreApplication.getInstance().getSession();
        realm = Realm.getDefaultInstance();
        api = ApiClient.getClient().create(ApiService.class);
        helper = new ItemMasterHelper(realm);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_item_master_detail, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        helper = new ItemMasterHelper(realm);
        api = ApiClient.getClient().create(ApiService.class);
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        itemTypes = helper.getItemType();
        itemGroups = helper.getItemGroup();
        txtItemPrice.addTextChangedListener(new Util.NumberTextWatcher(txtItemPrice, getActivity()));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, itemTypes);
        spinnerItemType.setAdapter(adapter);
        spinnerItemType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerItemGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        bundle = getActivity().getIntent().getExtras();
        return rootView;
    }

    public void setDisplay(String itemno)
    {
        itemMaster = realm.where(ItemMaster.class).equalTo("itemNo", itemno).findFirst();
        if(itemMaster != null)
        {
            getActivity().setTitle(itemMaster.getItemDesc());
            if(itemMaster.getItemImage() == null || (itemMaster.getItemImage() != null && itemMaster.getItemImage().equals("")))
            {

            }
            else
            {
                String base64String = itemMaster.getItemImage();
                String base64Image = base64String;
                byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                itemImage.setImageBitmap(decodedByte);
            }
            img_str = itemMaster.getItemImage();
            txtItemNo.setText(itemMaster.getItemNo());
            txtItemName.setText(itemMaster.getForeignName().equals("null") ? "" : itemMaster.getForeignName());
            txtItemDesc.setText(itemMaster.getItemDesc());
            txtItemStock.setText(itemMaster.getStock() + "");
            ItemPrice itemPrice = realm.where(ItemPrice.class)
                    .equalTo("itemId", itemMaster.getItemNo())
                    .sort("priceId", Sort.ASCENDING).findFirst();
            if(itemPrice != null)
            {
                txtItemPrice.setText(Tools.convertMoney(getActivity(), itemPrice.getPrice()));
            }
            spinnerItemType.setSelection(itemMaster.getItemTypeId() - 1);
            ItemGroupMaster itemGroupMaster = realm.where(ItemGroupMaster.class)
                    .equalTo("itemGroupId", itemMaster.getItemGroupId()).findFirst();
            if(itemGroupMaster != null)
            {
                final ArrayAdapter<String> adaptergroup = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_spinner_item, itemGroups);
                spinnerItemGroup.setAdapter(adaptergroup);
                int spinnerPosition = adaptergroup.getPosition(itemGroupMaster.getItemGroupName());
                spinnerItemGroup.setSelection(spinnerPosition);
            }
            spinnerItemGroup.setEnabled(false);
            spinnerItemType.setEnabled(false);
            txtItemNo.setEnabled(false);
        }
    }
}
