package com.example.testhana.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class OtherPaymentFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.groupOtherPayment)
    RadioGroup otherRadGroup;
    @BindView(R.id.groupOtherPayment2)
    RadioGroup otherRadGroup2;

    @BindView(R.id.etOtherNotes)
    EditText etOtherNotes;
    @BindView(R.id.btnSubmitOtherPayment)
    Button btnSubmit;
    @BindView(R.id.tvCharRemaining)
    TextView tvCharRemaining;
    private Unbinder unbinder;

    RadioButton radioButton;

    SessionManagement session;
    Double amount, subtotal;
    public String payType = "";
    public boolean isFromOrder = false;
    String intentFilter = "paymentEnabled";

    public OtherPaymentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private final BroadcastReceiver myBroadcastReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, android.content.Intent intent) {
            btnSubmit.setEnabled(true);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_other_payment, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        session = CoreApplication.getInstance().getSession();

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        amount = getArguments().getDouble("total", 0);
        subtotal = getArguments().getDouble("subtotal", 0);
        isFromOrder = getArguments().containsKey("order");

        etOtherNotes.addTextChangedListener(mTextEditorWatcher);

        otherRadGroup.clearCheck();
        otherRadGroup2.clearCheck();
        otherRadGroup.setOnCheckedChangeListener(listener1);
        otherRadGroup2.setOnCheckedChangeListener(listener2);

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(myBroadcastReceiver,
                new IntentFilter(intentFilter));

        btnSubmit.setOnClickListener(this);

        return rootView;
    }

    private RadioGroup.OnCheckedChangeListener listener1 = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                otherRadGroup2.setOnCheckedChangeListener(null);
                otherRadGroup2.clearCheck();
                otherRadGroup2.setOnCheckedChangeListener(listener2);

                int selectedid = otherRadGroup.getCheckedRadioButtonId();
                radioButton = getActivity().findViewById(selectedid);

                String termPay = radioButton.getText().toString();
                if (termPay.equalsIgnoreCase("EDC")){
                    payType = "E";
                }else if (termPay.equalsIgnoreCase("Gift Card")){
                    payType = "G";
                }else if (termPay.equalsIgnoreCase("Voucher")){
                    payType = "V";
                }else if (termPay.equalsIgnoreCase("Other")){
                    payType = "L";
                }
                etOtherNotes.setText("");
                etOtherNotes.clearFocus();

                PaymentFragment.payType = payType;
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener listener2 = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                otherRadGroup.setOnCheckedChangeListener(null);
                otherRadGroup.clearCheck();
                otherRadGroup.setOnCheckedChangeListener(listener1);

                int selectedid = otherRadGroup2.getCheckedRadioButtonId();
                radioButton = getActivity().findViewById(selectedid);

                String termPay = radioButton.getText().toString();
                if (termPay.equalsIgnoreCase("EDC")){
                    payType = "E";
                }else if (termPay.equalsIgnoreCase("Gift Card")){
                    payType = "G";
                }else if (termPay.equalsIgnoreCase("Voucher")){
                    payType = "V";
                }else if (termPay.equalsIgnoreCase("Other")){
                    payType = "L";
                }
                etOtherNotes.setText("");
                etOtherNotes.clearFocus();
                PaymentFragment.payType = payType;
            }
        }
    };

    private final TextWatcher mTextEditorWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //This sets a textview to the current length
            tvCharRemaining.setText(String.valueOf(s.length())+"/50");
        }

        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    public void onClick(View v) {
        if(v instanceof Button) {
            Button button = (Button) v;
            if (button.getText().toString().equalsIgnoreCase("Pay")) {
                if (!payType.equalsIgnoreCase("")) {
                    if (etOtherNotes != null && !etOtherNotes.getText().toString().trim().equals("")) {
//                        btnSubmit.setEnabled(false);
                        PaymentFragment f = (PaymentFragment) getFragmentManager().findFragmentByTag("payment");
                        f.createOrder(etOtherNotes.getText().toString(), amount, 0.0, 0.0);
                    }else{
                        Toast.makeText(getContext(), "Fill Reference No", Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getContext(), "Choose Payment Type", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onDestroyView() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(myBroadcastReceiver);

        super.onDestroyView();
        unbinder.unbind();
    }
}
