package com.example.testhana.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.R;

import java.util.ArrayList;
import java.util.List;

public class InvoiceWarehouseFragment extends Fragment implements TabLayout.OnTabSelectedListener{

    public static TabLayout tabLayout;
    public static RelativeLayout badgeHome1, badgeHome2, badgeHome3;
    public static TextView badgeText1, badgeText2, badgeText3;
    private ViewPager viewPager;
    SessionManagement session;
    ProgressDialog progressDialog;
    public static int storeid = 0;
    boolean isTablet = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = CoreApplication.getInstance().getSession();
        isTablet = getResources().getBoolean(R.bool.isTablet);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_warehouse_invoice, container, false);
        viewPager = (ViewPager) rootView.findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(4);
        Bundle b = this.getArguments();
        if(b != null)
        {
            storeid = Integer.parseInt(b.getString("storeId"));
        }
        setupViewPager(viewPager);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
//        TabLayout.Tab tab = tabLayout.newTab()
//                .setText("tab name")
//                .setCustomView(R.layout.badges_tablayout_ordering_pembelian);
//        tabLayout.addTab(tab);
//        TabLayout.Tab tab1 = tabLayout.getTabAt(0);
//        View tabView = tab1.getCustomView();
//        TextView badgeText = (TextView) tabView.findViewById(R.id.count);
//        badgeText.setText("1");

        TabLayout.Tab tab = tabLayout.getTabAt(0);
        tab.setCustomView(R.layout.badges_tablayout_order_customer);
        View tabView = tab.getCustomView();
        RelativeLayout badgeHome = (RelativeLayout) tabView.findViewById(R.id.counterValuePanel);
        TextView badgeText = (TextView) tabView.findViewById(R.id.count);
        TextView badgeTitle = (TextView) tabView.findViewById(R.id.txtTitle);
        badgeTitle.setText("Open");
        badgeText.setText("12");
        badgeHome.setVisibility(View.VISIBLE);

        TabLayout.Tab tab1 = tabLayout.getTabAt(1);
        tab1.setCustomView(R.layout.badges_tablayout_order_customer);
        View tabView1 = tab1.getCustomView();
        badgeHome1 = (RelativeLayout) tabView1.findViewById(R.id.counterValuePanel);
        badgeText1 = (TextView) tabView1.findViewById(R.id.count);
        TextView badgeTitle1 = (TextView) tabView1.findViewById(R.id.txtTitle);
        badgeTitle1.setText("Overdue");
        badgeText1.setText("2");
        badgeHome1.setVisibility(View.VISIBLE);

        return rootView;
    }

    public void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new OrderListFragment(), "Received"); // no
        adapter.addFragment(new OrderCustomerSOOpenFragment(), "Pending");
        //adapter.addFragment(new PurchaseOrderStatusFragment(), getString(R.string.statuspemesanan));
        //adapter.addFragment(new DeliveryOrderListFragment(), getString(R.string.delo));
        //adapter.addFragment(new ReceiveGoodListFragment(), getString(R.string.reg));
        //adapter.addFragment(new TransactionFragment(), getString(R.string.transaction));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void setCurrentItem(int item) {
        viewPager.setCurrentItem(item);
    }

    @Override
    public void onDestroyView() {
        //unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem cart = menu.findItem(R.id.action_cart);
        if(cart != null)
            cart.setVisible(false);
        MenuItem choosecustomer = menu.findItem(R.id.action_customer);
        if(choosecustomer != null)
        {
            choosecustomer.setVisible(true);
        }
    }
}

