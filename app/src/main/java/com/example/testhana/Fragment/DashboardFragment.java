package com.example.testhana.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.testhana.Adapter.DashboardAdapter;
import com.example.testhana.Helper.CartHelper;
import com.example.testhana.Helper.ItemMasterHelper;
import com.example.testhana.Models.Model.ItemMasterModel;
import com.example.testhana.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;

public class DashboardFragment extends Fragment {

    @BindView(R.id.chartBar) BarChart chartBar;
    @BindView(R.id.chartPie) PieChart chartPie;
    @BindView(R.id.chartRadar) RadarChart chartRadar;
    @BindView(R.id.chartLine) LineChart chartLine;
    private DashboardAdapter adapter;
    Realm realm;
    List<ItemMasterModel> itemMasterModelList;
    ItemMasterHelper helper;
    CartHelper cartHelper;
    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        setBarChart();
        setPieChart();
        setRadarChart();
        setData();
        return rootView;
    }

    public void setBarChart()
    {
        BarData data = new BarData(getXAxisValues(), getDataSet());
        chartBar.setData(data);
        chartBar.setDescription("Items By Qty");
        chartBar.animateXY(2000, 2000);
        chartBar.invalidate();
    }

    public void setPieChart()
    {
        ArrayList NoOfEmp = new ArrayList();
        NoOfEmp.add(new Entry(10, 0));
        NoOfEmp.add(new Entry(4, 1));
        NoOfEmp.add(new Entry(12, 2));
        NoOfEmp.add(new Entry(1, 3));
        PieDataSet dataSet = new PieDataSet(NoOfEmp, "Items By Revenue");
        ArrayList year = new ArrayList();
        year.add("Pending");
        year.add("Partial");
        year.add("FulFill");
        year.add("Cancel");
        PieData data = new PieData(year, dataSet);
        chartPie.setData(data);
        chartPie.setDescription("Status Order");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        chartPie.animateXY(5000, 5000);
    }

    public void setRadarChart()
    {
        ArrayList NoOfEmp = new ArrayList();
        NoOfEmp.add(new Entry(123f, 0));
        NoOfEmp.add(new Entry(456f, 1));
        NoOfEmp.add(new Entry(1164f, 2));
        NoOfEmp.add(new Entry(865f, 3));
        RadarDataSet dataSet = new RadarDataSet(NoOfEmp, "Customer By Revenue");
        dataSet.setColor(Color.rgb(103, 110, 129));
        dataSet.setFillColor(Color.rgb(103, 110, 129));
        dataSet.setDrawFilled(true);
        dataSet.setFillAlpha(180);
        dataSet.setLineWidth(2f);
        dataSet.setDrawHighlightCircleEnabled(true);
        dataSet.setDrawHighlightIndicators(false);
        ArrayList year = new ArrayList();
        year.add("Charles");
        year.add("Xavior");
        year.add("Peter");
        year.add("Parker");
        RadarData data = new RadarData(year, dataSet);
        chartRadar.setData(data);
        chartRadar.setDescription("Customer By Revenue");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        chartRadar.animateXY(5000, 5000);
    }

    private ArrayList<String> setXAxisValues(){
        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("10");
        xVals.add("20");
        xVals.add("30");
        xVals.add("30.5");
        xVals.add("40");
        return xVals;
    }

    private ArrayList<Entry> setYAxisValues(){
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        yVals.add(new Entry(60, 0));
        yVals.add(new Entry(48, 1));
        yVals.add(new Entry(70.5f, 2));
        yVals.add(new Entry(100, 3));
        yVals.add(new Entry(180.9f, 4));
        return yVals;
    }

    private void setData() {
        ArrayList<String> xVals = setXAxisValues();
        ArrayList<Entry> yVals = setYAxisValues();
        LineDataSet set1;
        set1 = new LineDataSet(yVals, "Status Order per Month(Million)");
        set1.setFillAlpha(110);
        set1.setColor(Color.BLACK);
        set1.setCircleColor(Color.BLACK);
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setDrawFilled(true);
        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1);
        LineData data = new LineData(xVals, dataSets);
        chartLine.setData(data);
        chartLine.setDescription("Status Order");
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("JAN");
        xAxis.add("FEB");
        xAxis.add("MAR");
        return xAxis;
    }

    private ArrayList<IBarDataSet> getDataSet() {
        ArrayList<IBarDataSet> dataSets = null;
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1 = new BarEntry(110.000f, 0); // Jan
        valueSet1.add(v1e1);
        BarEntry v1e2 = new BarEntry(40.000f, 1); // Feb
        valueSet1.add(v1e2);
        BarEntry v1e3 = new BarEntry(60.000f, 2); // Mar
        valueSet1.add(v1e3);
        ArrayList<BarEntry> valueSet2 = new ArrayList<>();
        BarEntry v2e1 = new BarEntry(150.000f, 0); // Jan
        valueSet2.add(v2e1);
        BarEntry v2e2 = new BarEntry(90.000f, 1); // Feb
        valueSet2.add(v2e2);
        BarEntry v2e3 = new BarEntry(120.000f, 2); // Mar
        valueSet2.add(v2e3);
        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Mariko 240");
        barDataSet1.setColor(Color.rgb(0, 155, 0));
        BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Jurassic 240");
        //barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);
        barDataSet2.setColor(Color.rgb(193, 37, 82));
        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        return dataSets;
    }
}
