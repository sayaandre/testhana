package com.example.testhana.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.testhana.R;

public class CustomerMasterFragment extends Fragment {

    View rootView;
    boolean tabletSize = false;
    Bundle saveInstance;

    public CustomerMasterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_customer_master, container, false);
        FragmentManager fragmentManager = getChildFragmentManager();
        saveInstance = savedInstanceState;
        if (saveInstance == null) {
            if (rootView.findViewById(R.id.fragment_container_left) != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                CustomerMasterListFragment fragment = new CustomerMasterListFragment();
                if(getArguments() != null)
                    fragment.setArguments(getArguments());
                fragmentTransaction.replace(R.id.fragment_container_left, fragment, "customerlist");

                fragmentTransaction.commit();
            }

            if (rootView.findViewById(R.id.fragment_container_right) != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                CustomerMasterDetailFragment fragment = new CustomerMasterDetailFragment();
                if(getArguments() != null)
                    fragment.setArguments(getArguments());
                fragmentTransaction.replace(R.id.fragment_container_right, fragment, "customerdetail");
                fragmentTransaction.commit();
            }
        }
        return rootView;
    }

}
