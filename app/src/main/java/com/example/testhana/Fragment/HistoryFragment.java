package com.example.testhana.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.testhana.R;

public class HistoryFragment extends Fragment {

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_history, container, false);
        FragmentManager fragmentManager = getChildFragmentManager();

        if (savedInstanceState == null) {
            if (rootView.findViewById(R.id.fragment_container_left) != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                HistoryListFragment fragment = new HistoryListFragment();
                if(getArguments() != null)
                    fragment.setArguments(getArguments());
                fragmentTransaction.replace(R.id.fragment_container_left, fragment, "historylist");
                fragmentTransaction.commit();
            }

            if (rootView.findViewById(R.id.fragment_container_right) != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                HistoryDetailFragment fragment = new HistoryDetailFragment();
                if(getArguments() != null)
                    fragment.setArguments(getArguments());
                fragmentTransaction.replace(R.id.fragment_container_right, fragment, "historydetail");
                fragmentTransaction.commit();
            }
        }
        return rootView;
    }
}
