package com.example.testhana.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.testhana.R;

public class ItemMasterFragment extends Fragment {

    public ItemMasterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_item_master, container, false);
        FragmentManager fragmentManager = getChildFragmentManager();

        if (savedInstanceState == null) {
            if (rootView.findViewById(R.id.fragment_container_left) != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ItemMasterListFragment fragment = new ItemMasterListFragment();
                if(getArguments() != null)
                    fragment.setArguments(getArguments());
                fragmentTransaction.replace(R.id.fragment_container_left, fragment, "itemlist");

                fragmentTransaction.commit();
            }

            if (rootView.findViewById(R.id.fragment_container_right) != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ItemMasterDetailFragment fragment = new ItemMasterDetailFragment();
                if(getArguments() != null)
                    fragment.setArguments(getArguments());
                fragmentTransaction.replace(R.id.fragment_container_right, fragment, "itemdetail");
                fragmentTransaction.commit();
            }
        }
        return rootView;
    }
}