package com.example.testhana.Fragment;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.CustomerMasterHelper;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Models.Realm.CustomerCurrencyMaster;
import com.example.testhana.Models.Realm.CustomerGroupMaster;
import com.example.testhana.Models.Realm.CustomerMaster;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.ApiServiceHana;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;

public class CustomerMasterDetailFragment extends Fragment {

    private String[] customerRole, customerGroup, customerCurrency;
    CustomerMasterHelper helper;
    @BindView(R.id.txtCustomerCode)
    EditText txtCustomerCode;
    @BindView(R.id.txtCustomerName) EditText txtCustomerName;
    @BindView(R.id.txtCustomerPhone) EditText txtCustomerPhone;
    @BindView(R.id.txtCustomerEmail) EditText txtCustomerEmail;
    @BindView(R.id.txtCustomerAddress) EditText txtCustomerAddress;
    @BindView(R.id.spinnerCustomerRole)
    Spinner spinnerCustomerRole;
    @BindView(R.id.spinnerCustomerGroup) Spinner spinnerCustomerGroup;
    @BindView(R.id.spinnerCustomerCurrency) Spinner spinnerCustomerCurrency;
    @BindView(R.id.imgCustomer)
    ImageButton imgCustomer;
    protected static final int SELECT_FILE = 2;
    private static final int CROP_FROM_CAMERA = 3;
    private static final int SELECT_CATEGORY = 4;
    String img_str = "";
    private Uri mImageCaptureUri;
    CustomerMaster customerMaster;
    Bundle bundle;
    ApiService api;
    ApiServiceHana apiHana;
    Realm realm;
    SessionManagement session;
    ProgressDialog progressDialog;
    Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = CoreApplication.getInstance().getSession();
        realm = Realm.getDefaultInstance();
        api = ApiClient.getClient().create(ApiService.class);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_customer_master_detail, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        helper = new CustomerMasterHelper(realm);
        api = ApiClient.getClient().create(ApiService.class);
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        customerRole = helper.getCustomerRole();
        customerGroup = helper.getCustomerGroup();
        customerCurrency = helper.getCustomerCurrency();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, customerRole);
        spinnerCustomerRole.setAdapter(adapter);
        spinnerCustomerRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        final ArrayAdapter<String> adaptergroup = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, customerGroup);
        spinnerCustomerGroup.setAdapter(adaptergroup);
        spinnerCustomerGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        final ArrayAdapter<String> adaptercurr = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, customerCurrency);
        spinnerCustomerCurrency.setAdapter(adaptercurr);
        spinnerCustomerCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        bundle = getActivity().getIntent().getExtras();
        if(bundle != null && bundle.containsKey("customer_id"))
        {

        }
        else
        {
            getActivity().setTitle("Add Customer");
        }
        return rootView;
    }

    public void setDisplay(String custid)
    {
        customerMaster = realm.where(CustomerMaster.class).equalTo("customerCode", custid).findFirst();
        if(customerMaster != null)
        {
            getActivity().setTitle(customerMaster.getCustomerName());
            if(customerMaster.getCustomerImage() == null ||
                    (customerMaster.getCustomerImage() != null && customerMaster.getCustomerImage().equals("")))
            {

            }
            else
            {
                String base64String = customerMaster.getCustomerImage();
                String base64Image = base64String;
                byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                imgCustomer.setImageBitmap(decodedByte);
            }
            img_str = customerMaster.getCustomerImage();
            txtCustomerCode.setText(customerMaster.getCustomerCode());
            txtCustomerName.setText(customerMaster.getCustomerName());
            txtCustomerPhone.setText(customerMaster.getCustomerPhone());
            txtCustomerEmail.setText(customerMaster.getCustomerEmail());
            txtCustomerAddress.setText(customerMaster.getCustomerAddress());
            spinnerCustomerRole.setSelection(customerMaster.getCustomerRoleId() - 1);
            CustomerGroupMaster customerGroupMaster = realm.where(CustomerGroupMaster.class)
                    .equalTo("customerGroupId", customerMaster.getCustomerGroupId()).findFirst();
            if(customerGroupMaster != null)
            {
                final ArrayAdapter<String> adaptergroup = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_spinner_item, customerGroup);
                spinnerCustomerGroup.setAdapter(adaptergroup);
                int spinnerPosition = adaptergroup.getPosition(customerGroupMaster.getCustomerGroupName());
                spinnerCustomerGroup.setSelection(spinnerPosition);
            }
            CustomerCurrencyMaster customerCurrencyMaster = realm.where(CustomerCurrencyMaster.class)
                    .equalTo("Code", customerMaster.getCustomerCurrencyId()).findFirst();
            if(customerCurrencyMaster != null)
            {
                final ArrayAdapter<String> adaptercurr = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_spinner_item, customerCurrency);
                spinnerCustomerCurrency.setAdapter(adaptercurr);
                int spinnerPosition = adaptercurr.getPosition(customerCurrencyMaster.getName());
                spinnerCustomerCurrency.setSelection(spinnerPosition);
            }
//                spinnerCustomerCurrency.setSelection(customerMaster.getCustomerCurrencyId() - 1);
            spinnerCustomerRole.setEnabled(false);
            txtCustomerCode.setEnabled(false);
        }
    }
}
