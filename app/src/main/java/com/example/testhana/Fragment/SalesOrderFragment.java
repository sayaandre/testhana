package com.example.testhana.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.example.testhana.Activity.SalesOrderProductActivity;
import com.example.testhana.Adapter.SalesOrderAdapter;
import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.CartHelper;
import com.example.testhana.Helper.Constant;
import com.example.testhana.Helper.ItemMasterHelper;
import com.example.testhana.Helper.PaginationScrollListener;
import com.example.testhana.Helper.RealmHelper;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.MainActivity;
import com.example.testhana.Models.Model.ItemMasterModel;
import com.example.testhana.Models.Realm.Cart;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.Models.Realm.ItemPrice;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiServiceHana;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesOrderFragment extends Fragment implements SearchView.OnQueryTextListener{

    boolean tabletSize = false;
    MenuItem item;
    CartHelper cartHelper;
    Realm realm;
    SessionManagement session;

    public SalesOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        cartHelper = new CartHelper(getActivity());
        realm = Realm.getDefaultInstance();
        session = CoreApplication.getInstance().getSession();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_sales_order, container, false);
        FragmentManager fragmentManager = getChildFragmentManager();
        if (savedInstanceState == null) {
            if (rootView.findViewById(R.id.fragment_container_left) != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                SalesOrderLeftFragment fragment = new SalesOrderLeftFragment();
                if(getArguments() != null)
                    fragment.setArguments(getArguments());
                fragmentTransaction.replace(R.id.fragment_container_left, fragment, "salesleft");
                fragmentTransaction.commit();
            }

            if (rootView.findViewById(R.id.fragment_container_right) != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                CartFragment fragment = new CartFragment();
                if(getArguments() != null)
                    fragment.setArguments(getArguments());
                fragmentTransaction.replace(R.id.fragment_container_right, fragment, "salesright");
                fragmentTransaction.commit();
            }
        }
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        item = menu.findItem(R.id.action_search);
        item.setVisible(true);
        final SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(this);
        item.setOnActionExpandListener( new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
//                MainActivity.isFromSearch = true;
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                //searchView.setQuery("", true);
//                MainActivity.isFromSearch = false;
                return true; // Return true to collapse action view

            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        if(!s.equals("") && !s.isEmpty())
        {
            ItemMaster itemMaster = realm.where(ItemMaster.class)
                    .equalTo("kodeBarang", s).findFirst();
            if(itemMaster != null)
            {
                Intent i = new Intent(getActivity(), SalesOrderProductActivity.class);
                i.putExtra("item_id", itemMaster.getItemNo());
                i.putExtra("mode", 0);
                startActivityForResult(i, 100);
            }
            else
            {
                Toast.makeText(getActivity(), "Item Not Found", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(getActivity(), "Fill Item Id", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }
}