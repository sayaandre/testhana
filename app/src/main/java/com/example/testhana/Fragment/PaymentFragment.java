package com.example.testhana.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testhana.Activity.ReceiptActivity;
import com.example.testhana.Activity.SalesOrderProductActivity;
import com.example.testhana.Helper.CartHelper;
import com.example.testhana.Helper.RealmHelper;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Helper.Tools;
import com.example.testhana.Helper.Util;
import com.example.testhana.Models.Realm.Cart;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.Models.Realm.ItemPrice;
import com.example.testhana.Models.Realm.Journal;
import com.example.testhana.Models.Realm.JournalDetail;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.ApiServiceHana;
import com.example.testhana.Rest.ModelClass.DocumentLinesClass;
import com.example.testhana.Rest.ModelClass.OrderClass;
import com.example.testhana.Rest.Response.SubmitJournalResponse;
import com.google.gson.Gson;
import com.hopenlib.cflextools.FlexRadioGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentFragment extends Fragment {

    @BindView(R.id.groupPayment)
    FlexRadioGroup groupPayment;
    @BindView(R.id.txtAmount) TextView txtAmount;
    Unbinder unbinder;
    double total = 0.0;
    int selected = 0;
    int lastSelect = -1;
    public static String payType = "";
    private Realm realm;
    CartHelper helper;
    SessionManagement session;
    ApiService api;
    ProgressDialog progressDialog;
    ApiServiceHana apiHana;
    RealmHelper realmHelper;
    List<DocumentLinesClass> documentLinesClasses;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_payment, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        realm = Realm.getDefaultInstance();
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        realmHelper = new RealmHelper();
        api = ApiClient.getClient().create(ApiService.class);
        session = new SessionManagement(getActivity());
        helper = new CartHelper(getActivity());
        total = getArguments().getDouble("total", 0);
        txtAmount.setText(Tools.convertMoney(getActivity(), total));
        RadioButton rbCash = groupPayment.findViewById(R.id.radioCash);
        groupPayment.setOnCheckedChangeListener(new FlexRadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(FlexRadioGroup group, int checkedId) {
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                if(checkedId == R.id.radioCash)
                {
                    selected = 0;
                    if(lastSelect != selected) {
                        payType = "Cash";
                        NumpadFragment numpadPay = new NumpadFragment();
                        numpadPay.setArguments(getArguments());
                        if (getResources().getBoolean(R.bool.isTablet)) {
                            ft.replace(R.id.fragment_container_left, numpadPay, "cash");
                        }else{
                            ft.replace(R.id.fragment_container_left, numpadPay, "cash");
                        }
                        ft.commit();
                    }
                }
                else if(checkedId == R.id.radioOther)
                {
                    selected = 1;
                    if (lastSelect != selected) {
                        OtherPaymentFragment otherPayment = new OtherPaymentFragment();
                        otherPayment.setArguments(getArguments());
                        if (getResources().getBoolean(R.bool.isTablet)) {
                            ft.replace(R.id.fragment_container_left, otherPayment, "other");
                        }else{
                            ft.replace(R.id.fragment_container_left, otherPayment, "other");
                        }
                        ft.commit();
                    }
                }
                lastSelect = selected;
            }
        });
        ((RadioButton) groupPayment.getChildAt(selected)).setChecked(true);
        return rootView;
    }

    public void saveTransaction(final int docentry, final int docnum, final String refno, final double amount, final double kembali, double aa)
    {
        RealmResults<Cart> carts = realm.where(Cart.class).findAll();
        String jsonJournalDetail = "";
        final long id = Tools.dapatkanId();
        try
        {
            JSONArray jounralArr = new JSONArray();
            documentLinesClasses = new ArrayList<>();
            for(int i = 0 ; i < carts.size() ; i++)
            {
                DocumentLinesClass documentLinesClass = new DocumentLinesClass(carts.get(i).getItemId(),
                        String.valueOf(carts.get(i).getItemQty()),
                        "", String.valueOf(carts.get(i).getItemPrice()));
                documentLinesClasses.add(documentLinesClass);
                JSONObject journalObj = new JSONObject();
                journalObj.put("itemId", carts.get(i).getItemId());
                journalObj.put("itemName", carts.get(i).getItemDesc());
                journalObj.put("itemPrice", carts.get(i).getItemPrice());
                journalObj.put("itemQty", carts.get(i).getItemQty());
                journalObj.put("itemTotalPrice", carts.get(i).getTotal());
                journalObj.put("notes", carts.get(i).getRemarks());
                journalObj.put("journalType", "S");
                jounralArr.put(journalObj);
            }
            JSONObject journalObj = new JSONObject();
            journalObj.put("itemId", 0);
            journalObj.put("itemName", payType);
            journalObj.put("itemPrice", 0.0);
            journalObj.put("itemQty", 0);
            journalObj.put("itemTotalPrice", amount);
            journalObj.put("notes", refno);
            journalObj.put("journalType", "P");
            jounralArr.put(journalObj);
            if(payType.equalsIgnoreCase("cash"))
            {
                JSONObject journalChange = new JSONObject();
                journalChange.put("itemId", 0);
                journalChange.put("itemName", "Change");
                journalChange.put("itemPrice", 0.0);
                journalChange.put("itemQty", 0);
                journalChange.put("itemTotalPrice", kembali);
                journalChange.put("notes", "");
                journalChange.put("journalType", "C");
                jounralArr.put(journalChange);
            }
            jsonJournalDetail = jounralArr.toString();
        }catch (JSONException ex) {
            ex.printStackTrace();
        }
        Call<SubmitJournalResponse> call = api.submitJournal(id + "", total, helper.getTotalItemInCartInvoice(),
                payType, Util.getDate(), Util.getTime(), session.getKeyUserId(), session.getKeyCustomer(),
                refno, docentry, docnum, jsonJournalDetail);
        call.enqueue(new Callback<SubmitJournalResponse>() {
            @Override
            public void onResponse(Call<SubmitJournalResponse> call, Response<SubmitJournalResponse> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    if(response.body().getResult().equalsIgnoreCase("ok"))
                    {
                        realm.beginTransaction();
                        Journal journal = new Journal();
                        journal.setJournalId(id);
                        journal.setReceiptNumber(id + "");
                        journal.setTotal(total);
                        journal.setRowCount(helper.getTotalItemInCartInvoice());
                        journal.setTrxType(payType);
                        journal.setTrxDate(Util.getDate());
                        journal.setTrxTime(Util.getTime());
                        journal.setCashierId(session.getKeyUserPhone());
                        journal.setCustomerId(session.getKeyCustomer());
                        journal.setRefNo(refno);
                        journal.setDocentry(docentry);
                        journal.setDocnum(docnum);
                        realm.copyToRealmOrUpdate(journal);
                        RealmResults<Cart> carts = realm.where(Cart.class).findAll();
                        for(int i = 0 ; i < carts.size() ; i++)
                        {
                            JournalDetail journalDetail = new JournalDetail();
                            journalDetail.setJournalDetailId(Tools.dapatkanId());
                            journalDetail.setJournalId(id);
                            journalDetail.setItemId(carts.get(i).getItemId());
                            journalDetail.setItemName(carts.get(i).getItemDesc());
                            journalDetail.setItemPrice(carts.get(i).getItemPrice());
                            journalDetail.setItemQty(carts.get(i).getItemQty());
                            journalDetail.setItemTotalPrice(carts.get(i).getTotal());
                            journalDetail.setNotes(carts.get(i).getRemarks());
                            journalDetail.setJournalType("S");
                            realm.copyToRealmOrUpdate(journalDetail);
                        }
                        JournalDetail journalDetail = new JournalDetail();
                        journalDetail.setJournalDetailId(Tools.dapatkanId());
                        journalDetail.setJournalId(id);
                        journalDetail.setItemId("0");
                        journalDetail.setItemName(payType);
                        journalDetail.setItemPrice(0.0);
                        journalDetail.setItemQty(0);
                        journalDetail.setItemTotalPrice(amount);
                        journalDetail.setNotes(refno);
                        journalDetail.setJournalType("P");
                        if(payType.equalsIgnoreCase("cash"))
                        {
                            JournalDetail journalChange = new JournalDetail();
                            journalChange.setJournalDetailId(Tools.dapatkanId());
                            journalChange.setJournalId(id);
                            journalChange.setItemId("0");
                            journalChange.setItemName("Change");
                            journalChange.setItemPrice(0.0);
                            journalChange.setItemQty(0);
                            journalChange.setItemTotalPrice(kembali);
                            journalChange.setNotes("");
                            journalChange.setJournalType("C");
                            realm.copyToRealmOrUpdate(journalChange);
                        }
                        realm.copyToRealmOrUpdate(journalDetail);
                        carts.deleteAllFromRealm();
                        realm.commitTransaction();
                        session.setKeyCustomer("0");
                        Intent i = new Intent(getActivity(), ReceiptActivity.class);
                        i.putExtra("refNo", refno);
                        i.putExtra("total", total);
                        i.putExtra("amount", amount);
                        i.putExtra("change", kembali);
                        i.putExtra("payType", payType);
                        startActivity(i);
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<SubmitJournalResponse> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void createOrder(final String refno, final double amount, final double kembali, final double aa)
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Submit Payment..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        RealmResults<Cart> carts = realm.where(Cart.class).findAll();
        documentLinesClasses = new ArrayList<>();
        for(int i = 0 ; i < carts.size() ; i++)
        {
            DocumentLinesClass documentLinesClass = new DocumentLinesClass(carts.get(i).getItemId(),
                    String.valueOf(carts.get(i).getItemQty()),
                    "", String.valueOf(carts.get(i).getItemPrice()));
            documentLinesClasses.add(documentLinesClass);
        }
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date c = calendar.getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);
        OrderClass orderClass = new OrderClass(session.getKeyCustomer() == null ? "" : session.getKeyCustomer(),
                formattedDate, documentLinesClasses);
        Gson gson = new Gson();
        String json = gson.toJson(orderClass);
        Call<ResponseBody> call = apiHana.createOrder(cookie, orderClass);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        res = new String(response.body().bytes());
                        JSONObject obj = new JSONObject(res);
                        saveTransaction(obj.getInt("DocEntry"), obj.getInt("DocNum"), refno, amount, kembali, aa);
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    realmHelper.doLoginHana();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void dismissProgressDialog() {
        if(!getActivity().isFinishing() && progressDialog != null && progressDialog.isShowing()) {
            if(progressDialog.isShowing())
                progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
