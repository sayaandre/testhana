package com.example.testhana.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;


import com.example.testhana.Activity.CustomerMasterDetailActivity;
import com.example.testhana.Activity.HistoryDetailActivity;
import com.example.testhana.Activity.InitDataActivity;
import com.example.testhana.Activity.ItemMasterDetailActivity;
import com.example.testhana.Activity.LoginActivity;
import com.example.testhana.Adapter.CustomerMasterAdapter;
import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.Constant;
import com.example.testhana.Helper.CustomerMasterHelper;
import com.example.testhana.Helper.RealmHelper;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Helper.Tools;
import com.example.testhana.MainActivity;
import com.example.testhana.Models.Model.CustomerMasterModel;
import com.example.testhana.Models.Realm.CustomerMaster;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.ApiServiceHana;
import com.example.testhana.Rest.ModelClass.LoginHanaClass;
import com.example.testhana.Rest.Response.GetCustomerResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerMasterListFragment extends Fragment implements SearchView.OnQueryTextListener{

    @BindView(R.id.rvCustomerMaster)
    RecyclerView rvItem;
    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.txtDataFound) TextView txtDataFound;
    private CustomerMasterAdapter adapter;
    Realm realm;
    List<CustomerMasterModel> customerMasterModelList;
    CustomerMasterHelper helper;
    Unbinder unbinder;
    private SwipeRefreshLayout swipeLayout;
    SwipeRefreshLayout.OnRefreshListener refreshListener;
    ApiService api;
    ApiServiceHana apiHana;
    SessionManagement session;
    MenuItem itemr;
    MenuItem iteml;
    String tampCurr = "0", tampPrev = "0", tampNext = "0";
    RealmHelper realmHelper;
    int isFirst = 1;
    ProgressDialog progressDialog;
    MenuItem item;
    boolean tabletSize = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_customer_master_list, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        swipeLayout = rootView.findViewById(R.id.swipeLayout);
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        realm = Realm.getDefaultInstance();
        api = ApiClient.getClient().create(ApiService.class);
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        realmHelper = new RealmHelper();
        session = CoreApplication.getInstance().getSession();
        customerMasterModelList = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        helper = new CustomerMasterHelper(realm);
        Tools.onCreateSwipeToRefresh(swipeLayout);
        refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCustomerMasterHana(session.getKeyCurrCust());
            }
        };
        swipeLayout.setOnRefreshListener(refreshListener);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        rvItem.setLayoutManager(mLayoutManager);
        rvItem.addItemDecoration(new GridSpacingItemDecoration(1, 0, true));
        rvItem.setItemAnimator(new DefaultItemAnimator());
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), CustomerMasterDetailActivity.class);
                startActivityForResult(i, 100);
            }
        });
        return rootView;
    }

    public void getCustomerMasterHana(final String paging)
    {
        swipeLayout.setRefreshing(true);
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call;
        if(paging.equals("0")) {
            call = apiHana.getFirstBusinessPartner(Constant.SELECT_CUSTOMER, cookie);
        }
        else {
            call = apiHana.getBusinessPartner(Constant.SELECT_CUSTOMER, paging, cookie);
        }
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                swipeLayout.setRefreshing(false);
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        final Realm realm = Realm.getDefaultInstance();
                        res = new String(response.body().bytes());
                        JSONObject obj = new JSONObject(res);
                        JSONArray value = obj.getJSONArray("value");
                        if(!obj.isNull("odata.nextLink"))
                        {
                            String fullstr = obj.getString("odata.nextLink");
                            String substr = fullstr.substring(fullstr.indexOf("skip="));
                            String[] nextlink = substr.replace("skip=", "").split("&");
                            session.setKeyNextCust(nextlink[0]);
                            tampNext = nextlink[0];
                            itemr.setVisible(true);
                        }
                        else
                        {
                            itemr.setVisible(false);
                            session.setKeyNextCust(String.valueOf(Integer.parseInt(session.getKeyCurrCust()) + value.length()));
                        }
                        if(paging.equals("0")){
                            iteml.setVisible(false);
                        }
                        else {
                            iteml.setVisible(true);
                        }
                        final List<CustomerMaster> customerMasters = new ArrayList<>();
                        for(int i = 0 ; i < value.length() ; i++)
                        {
                            JSONObject customer = value.getJSONObject(i);
                            CustomerMaster customerMaster = new CustomerMaster();
                            customerMaster.setCustomerId(i + 1);
                            customerMaster.setCustomerCode(customer.getString("CardCode"));
                            customerMaster.setCustomerName(customer.getString("CardName"));
                            customerMaster.setCustomerRoleId(customer.getString("CardType").equalsIgnoreCase("ccustomer") ? 1 :
                                    customer.getString("CardType").equalsIgnoreCase("clid") ? 2 : 3);
                            customerMaster.setCustomerGroupId(customer.getInt("GroupCode"));
                            customerMaster.setCustomerCurrencyId(customer.getString("Currency"));
                            customerMaster.setCustomerPhone(customer.getString("Phone1"));
                            customerMaster.setCustomerAddress(customer.getString("Address"));
                            customerMaster.setCustomerEmail(customer.getString("EmailAddress"));
                            customerMasters.add(customerMaster);
                        }
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(customerMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                setAdapter();
                            }
                        });
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    realmHelper.doLoginHana();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                swipeLayout.setRefreshing(false);
                session.setKeyPrevCust(tampPrev);
                session.setKeyCurrCust(tampCurr);
                session.setKeyNextCust(tampNext);
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void deleteCustomerMasterHana(final String bpid)
    {
        swipeLayout.setRefreshing(true);
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.deleteBusinessPartner(bpid, cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                swipeLayout.setRefreshing(false);
                if(response.isSuccessful())
                {
                    Realm mrealm = Realm.getDefaultInstance();
                    CustomerMaster customerMaster = realm.where(CustomerMaster.class)
                            .equalTo("customerCode", bpid).findFirst();
                    if(customerMaster != null)
                    {
                        mrealm.beginTransaction();
                        customerMaster.deleteFromRealm();
                        mrealm.commitTransaction();
                        getCustomerMasterHana(session.getKeyCurrCust());
                    }
                }
                else if(response.code() == 404)
                {
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    getCustomerMasterHana(session.getKeyCurrCust());
                }
                else if(response.code() == 400)
                {
                    String res = null;
                    try {
                        res = new String(response.errorBody().bytes());
                        JSONObject item = new JSONObject(res);
                        JSONObject error = item.getJSONObject("error");
                        JSONObject message = error.getJSONObject("message");
                        AlertDialog.Builder kotakBuilder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
                        kotakBuilder.setIcon(android.R.drawable.ic_dialog_alert);
                        kotakBuilder.setTitle("Notice");
                        kotakBuilder.setMessage(message.getString("value"));
                        kotakBuilder.setPositiveButton("Close",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog dialog = kotakBuilder.create();
                        dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_white));
                        dialog.show();
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    realmHelper.doLoginHana();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                swipeLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private void setAdapter()
    {
        customerMasterModelList.clear();
        customerMasterModelList.addAll(helper.getCustomerMaster(Integer.parseInt(session.getKeyCurrCust()),
                Integer.parseInt(session.getKeyNextCust())));
        if(customerMasterModelList.size() > 0)
            txtDataFound.setVisibility(View.GONE);
        if(adapter == null) {
            adapter = new CustomerMasterAdapter(getActivity(), customerMasterModelList, new CustomerMasterAdapter.OnItemClickListener() {
                @Override
                public void onClick(CustomerMasterModel item, int position) {
                    isFirst = 0;
                    if (tabletSize) {
                        CustomerMasterDetailFragment f = (CustomerMasterDetailFragment) getFragmentManager().findFragmentByTag("customerdetail");
                        f.setDisplay(item.getCustomerCode());
                    }
                    else
                    {
                        Intent i = new Intent(getActivity(), CustomerMasterDetailActivity.class);
                        i.putExtra("customer_id", item.getCustomerCode());
                        startActivityForResult(i, 100);
                    }
                }

                @Override
                public void onDelete(final CustomerMasterModel item, int position) {
                    AlertDialog.Builder kotakBuilder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
                    kotakBuilder.setIcon(android.R.drawable.ic_dialog_alert);
                    kotakBuilder.setTitle("Notice");
                    kotakBuilder.setMessage("Delete This Customer?");
                    kotakBuilder.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    deleteCustomerMasterHana(item.getCustomerCode());
                                }
                            });
                    kotakBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = kotakBuilder.create();
                    dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_white));
                    dialog.show();
                }
            });
            rvItem.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == 100)
            {
                MenuItem searchMenuItem = item;
                if (searchMenuItem != null) {
                    searchMenuItem.collapseActionView();
                }
                setAdapter();
            }
        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onDestroyView() {
        if(realm != null && !realm.isClosed())
            realm.close();
        super.onDestroyView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_right) {
            tampPrev = session.getKeyPrevCust();
            tampCurr = session.getKeyCurrCust();
            session.setKeyPrevCust(session.getKeyCurrCust());
            session.setKeyCurrCust(session.getKeyNextCust());
            getCustomerMasterHana(session.getKeyCurrCust());
        }
        if (item.getItemId() == R.id.action_left) {
            tampNext = session.getKeyNextCust();
            tampCurr = session.getKeyCurrCust();
            session.setKeyNextCust(session.getKeyCurrCust());
            session.setKeyCurrCust(session.getKeyPrevCust());
            getCustomerMasterHana(session.getKeyCurrCust());
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        item = menu.findItem(R.id.action_search);
        item.setVisible(true);
        iteml = menu.findItem(R.id.action_left);
        iteml.setVisible(false);
        itemr = menu.findItem(R.id.action_right);
        itemr.setVisible(true);
        final MenuItem cart = menu.findItem(R.id.action_cart);
        cart.setVisible(false);
        if(isFirst == 1)
        {
            session.setKeyPrevCust("0");
            session.setKeyCurrCust("0");
            getCustomerMasterHana("0");
//            setAdapter();
        }
        final SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(this);

        item.setOnActionExpandListener( new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
//                MainActivity.isFromSearch = true;
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                //searchView.setQuery("", true);
//                MainActivity.isFromSearch = false;
                return true; // Return true to collapse action view

            }
        });
    }

    public void getBusinessPartnerMasterByIdHana(final String bpid)
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Search Customer..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.getBusinessPartnerById(bpid, Constant.SELECT_CUSTOMER, cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        res = new String(response.body().bytes());
                        JSONObject customer = new JSONObject(res);
                        realm.beginTransaction();
                        final CustomerMaster customerMaster = new CustomerMaster();
                        customerMaster.setCustomerCode(customer.getString("CardCode"));
                        customerMaster.setCustomerName(customer.getString("CardName"));
                        customerMaster.setCustomerRoleId(customer.getString("CardType").equalsIgnoreCase("ccustomer") ? 1 :
                                customer.getString("CardType").equalsIgnoreCase("clid") ? 2 : 3);
                        customerMaster.setCustomerGroupId(customer.getInt("GroupCode"));
                        customerMaster.setCustomerCurrencyId(customer.getString("Currency"));
                        customerMaster.setCustomerPhone(customer.getString("Phone1"));
                        customerMaster.setCustomerAddress(customer.getString("Address"));
                        customerMaster.setCustomerEmail(customer.getString("EmailAddress"));
                        realm.copyToRealmOrUpdate(customerMaster);
                        realm.commitTransaction();
                        Intent i = new Intent(getActivity(), CustomerMasterDetailActivity.class);
                        i.putExtra("customer_id", customerMaster.getCustomerCode());
                        startActivityForResult(i, 100);
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if(response.code() == 404)
                {
                    Toast.makeText(getActivity(), "Not Found", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
                    realmHelper.doLoginHana();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void dismissProgressDialog() {
        if(!getActivity().isFinishing() && progressDialog != null && progressDialog.isShowing()) {
            if(progressDialog.isShowing())
                progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        getBusinessPartnerMasterByIdHana(s);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    @Override
    public void onPause() {
        super.onPause();
        onDestroyView();
    }
}
