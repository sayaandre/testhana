package com.example.testhana.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testhana.Activity.ItemMasterDetailActivity;
import com.example.testhana.Adapter.ItemMasterAdapter;
import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.Constant;
import com.example.testhana.Helper.ItemMasterHelper;
import com.example.testhana.Helper.RealmHelper;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Helper.Tools;
import com.example.testhana.Models.Model.ItemMasterModel;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.Models.Realm.ItemPrice;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.ApiServiceHana;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemMasterListFragment extends Fragment implements SearchView.OnQueryTextListener{

    @BindView(R.id.rvItemMaster)
    RecyclerView rvItem;
    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.txtDataFound) TextView txtDataFound;
    private ItemMasterAdapter adapter;
    Realm realm;
    List<ItemMasterModel> itemMasterModelList;
    ItemMasterHelper helper;
    Unbinder unbinder;
    private SwipeRefreshLayout swipeLayout;
    SwipeRefreshLayout.OnRefreshListener refreshListener;
    ApiService api;
    ApiServiceHana apiHana;
    SessionManagement session;
    MenuItem itemr;
    MenuItem iteml;
    String tampCurr = "0", tampPrev = "0", tampNext = "0";
    RealmHelper realmHelper;
    int isFirst = 1;
    ProgressDialog progressDialog;
    MenuItem item;
    boolean tabletSize = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_item_master_list, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        swipeLayout = rootView.findViewById(R.id.swipeLayout);
        realm = Realm.getDefaultInstance();
        realmHelper = new RealmHelper();
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        api = ApiClient.getClient().create(ApiService.class);
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        session = CoreApplication.getInstance().getSession();
        itemMasterModelList = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        helper = new ItemMasterHelper(realm);
        Tools.onCreateSwipeToRefresh(swipeLayout);
        refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getItemMasterHana(session.getKeyCurrItem());
            }
        };
        swipeLayout.setOnRefreshListener(refreshListener);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        rvItem.setLayoutManager(mLayoutManager);
        rvItem.addItemDecoration(new GridSpacingItemDecoration(1, 0, true));
        rvItem.setItemAnimator(new DefaultItemAnimator());
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ItemMasterDetailActivity.class);
                startActivityForResult(i, 100);
            }
        });
        return rootView;
    }

    public void getItemMasterHana(final String paging)
    {
        swipeLayout.setRefreshing(true);
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call;
        if(paging.equals("0")) {
            call = apiHana.getFirstItems(Constant.SELECT_ITEM, cookie);
        }
        else {
            call = apiHana.getItems(paging,Constant.SELECT_ITEM,  cookie);
        }
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                swipeLayout.setRefreshing(false);
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        final Realm realm = Realm.getDefaultInstance();
                        res = new String(response.body().bytes());
                        JSONObject obj = new JSONObject(res);
                        JSONArray value = obj.getJSONArray("value");
                        if(!obj.isNull("odata.nextLink"))
                        {
                            String fullstr = obj.getString("odata.nextLink");
                            String substr = fullstr.substring(fullstr.indexOf("skip="));
                            String[] nextlink = substr.replace("skip=", "").split("&");
                            session.setKeyNextItem(nextlink[0]);
                            tampNext = nextlink[0];
                            itemr.setVisible(true);
                        }
                        else
                        {
                            itemr.setVisible(false);
                            session.setKeyNextItem(String.valueOf(Integer.parseInt(session.getKeyCurrItem()) + value.length()));
                            tampNext = String.valueOf(Integer.parseInt(session.getKeyCurrItem()) + value.length());
                        }
                        if(paging.equals("0")){
                            iteml.setVisible(false);
                        }
                        else {
                            iteml.setVisible(true);
                        }
                        final List<ItemMaster> itemMasters = new ArrayList<>();
                        for(int i = 0 ; i < value.length() ; i++)
                        {
                            JSONObject item = value.getJSONObject(i);
                            ItemMaster itemMaster = new ItemMaster();
                            itemMaster.setItemId(i + 1);
                            itemMaster.setItemNo(item.getString("ItemCode"));
                            itemMaster.setItemDesc(item.getString("ItemName"));
                            itemMaster.setForeignName(item.getString("ForeignName"));
                            itemMaster.setItemGroupId(item.getInt("ItemsGroupCode"));
                            JSONArray itemwarehouse = item.getJSONArray("ItemWarehouseInfoCollection");
                            int stock = 0;
                            for(int j = 0 ; j < itemwarehouse.length() ; j++)
                            {
                                JSONObject warehouse = itemwarehouse.getJSONObject(j);
                                stock = stock + warehouse.getInt("InStock");
                            }
                            itemMaster.setStock(stock);
                            itemMaster.setItemTypeId(item.getString("ItemType").equalsIgnoreCase("ititems") ? 1 :
                                    item.getString("ItemType").equalsIgnoreCase("itlabor") ? 2 : 3);
                            itemMaster.setBarcode(item.getString("BarCode"));
                            itemMasters.add(itemMaster);
                            JSONArray itemprice = item.getJSONArray("ItemPrices");
                            realm.beginTransaction();
                            RealmResults<ItemPrice> itemPriceRealmResults = realm.where(ItemPrice.class)
                                    .equalTo("itemId", item.getString("ItemCode")).findAll();
                            itemPriceRealmResults.deleteAllFromRealm();
                            realm.commitTransaction();
                            for(int j = 0 ; j < itemprice.length() ; j++)
                            {
                                ItemPrice lastitemprice = realm.where(ItemPrice.class).findAll()
                                        .sort("priceId", Sort.DESCENDING).first();
                                JSONObject price = itemprice.getJSONObject(j);
                                if(!price.getString("Price").equalsIgnoreCase("null"))
                                {
                                    realm.beginTransaction();
                                    ItemPrice itemPrice = new ItemPrice();
                                    itemPrice.setPriceId(lastitemprice == null ? 1 : lastitemprice.getPriceId() + 1);
                                    itemPrice.setPriceList(price.getInt("PriceList"));
                                    itemPrice.setPrice(price.getInt("Price"));
                                    itemPrice.setCurrency(price.getString("Currency"));
                                    itemPrice.setItemId(item.getString("ItemCode"));
                                    realm.copyToRealmOrUpdate(itemPrice);
                                    realm.commitTransaction();
                                }
                            }
                        }
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                setAdapter();
                            }
                        });
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    realmHelper.doLoginHana();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                swipeLayout.setRefreshing(false);
                session.setKeyPrevItem(tampPrev);
                session.setKeyCurrItem(tampCurr);
                session.setKeyNextItem(tampNext);
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private void setAdapter()
    {
        itemMasterModelList.clear();
        itemMasterModelList.addAll(helper.getItemMaster(Integer.parseInt(session.getKeyCurrItem()),
                Integer.parseInt(session.getKeyNextItem())));
        if(itemMasterModelList.size() > 0)
        {
            txtDataFound.setVisibility(View.GONE);
        }
        if(adapter == null) {
            adapter = new ItemMasterAdapter(getActivity(), itemMasterModelList, new ItemMasterAdapter.OnItemClickListener() {
                @Override
                public void onClick(ItemMasterModel item) {
                    isFirst = 0;
                    if (tabletSize) {
                        ItemMasterDetailFragment f = (ItemMasterDetailFragment) getFragmentManager().findFragmentByTag("itemdetail");
                        f.setDisplay(item.getItemNo());
                    }
                    else
                    {
                        Intent i = new Intent(getActivity(), ItemMasterDetailActivity.class);
                        i.putExtra("item_id", item.getItemNo());
                        startActivityForResult(i, 100);
                    }
                }

                @Override
                public void onDelete(final ItemMasterModel item, int position) {
                    AlertDialog.Builder kotakBuilder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
                    kotakBuilder.setIcon(android.R.drawable.ic_dialog_alert);
                    kotakBuilder.setTitle("Notice");
                    kotakBuilder.setMessage("Delete This Item?");
                    kotakBuilder.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    deleteItemMasterHana(item.getItemNo());
                                }
                            });
                    kotakBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = kotakBuilder.create();
                    dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_white));
                    dialog.show();
                }
            });
            rvItem.setAdapter(adapter);
        } else {
//            adapter.updateData(header);
            adapter.notifyDataSetChanged();
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == 100)
            {
                MenuItem searchMenuItem = item;
                if (searchMenuItem != null) {
                    searchMenuItem.collapseActionView();
                }
                setAdapter();
            }
        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onDestroyView() {
        if(realm != null && !realm.isClosed())
            realm.close();
        super.onDestroyView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_right) {
            tampPrev = session.getKeyPrevItem();
            tampCurr = session.getKeyCurrItem();
            session.setKeyPrevItem(session.getKeyCurrItem());
            session.setKeyCurrItem(session.getKeyNextItem());
            getItemMasterHana(session.getKeyCurrItem());
        }
        if (item.getItemId() == R.id.action_left) {
            tampNext = session.getKeyNextItem();
            tampCurr = session.getKeyCurrItem();
            session.setKeyNextItem(session.getKeyCurrItem());
            session.setKeyCurrItem(session.getKeyPrevItem());
            getItemMasterHana(session.getKeyCurrItem());
        }
        return super.onOptionsItemSelected(item);
    }

    public void deleteItemMasterHana(final String iid)
    {
        swipeLayout.setRefreshing(true);
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.deleteItem(iid, cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                swipeLayout.setRefreshing(false);
                if(response.isSuccessful())
                {
                    Realm mrealm = Realm.getDefaultInstance();
                    ItemMaster itemMaster = realm.where(ItemMaster.class)
                            .equalTo("itemNo", iid).findFirst();
                    if(itemMaster != null)
                    {
                        mrealm.beginTransaction();
                        itemMaster.deleteFromRealm();
                        mrealm.commitTransaction();
                        getItemMasterHana(session.getKeyCurrItem());
                    }
                }
                else if(response.code() == 404)
                {
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    getItemMasterHana(session.getKeyCurrItem());
                }
                else if(response.code() == 400)
                {
                    String res = null;
                    try {
                        res = new String(response.errorBody().bytes());
                        JSONObject item = new JSONObject(res);
                        JSONObject error = item.getJSONObject("error");
                        JSONObject message = error.getJSONObject("message");
                        AlertDialog.Builder kotakBuilder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
                        kotakBuilder.setIcon(android.R.drawable.ic_dialog_alert);
                        kotakBuilder.setTitle("Notice");
                        kotakBuilder.setMessage(message.getString("value"));
                        kotakBuilder.setPositiveButton("Close",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog dialog = kotakBuilder.create();
                        dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_white));
                        dialog.show();
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    realmHelper.doLoginHana();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                swipeLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        item = menu.findItem(R.id.action_search);
        item.setVisible(true);
        iteml = menu.findItem(R.id.action_left);
        iteml.setVisible(false);
        itemr = menu.findItem(R.id.action_right);
        itemr.setVisible(false);
        final MenuItem cart = menu.findItem(R.id.action_cart);
        cart.setVisible(false);
        if(isFirst == 1)
        {
            session.setKeyPrevItem("0");
            session.setKeyCurrItem("0");
            setAdapter();
//            getItemMasterHana("0");
        }
        final SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(this);

        item.setOnActionExpandListener( new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
//                MainActivity.isFromSearch = true;
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                //searchView.setQuery("", true);
//                MainActivity.isFromSearch = false;
                return true; // Return true to collapse action view

            }
        });
    }

    public void getItemMasterByIdHana(final String iid)
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Search Item..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.getItemId(iid, Constant.SELECT_ITEM, cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        final Realm realm = Realm.getDefaultInstance();
                        res = new String(response.body().bytes());
                        JSONObject item = new JSONObject(res);
                        final ItemMaster itemMaster = new ItemMaster();
                        itemMaster.setItemNo(item.getString("ItemCode"));
                        itemMaster.setItemDesc(item.getString("ItemName"));
                        itemMaster.setForeignName(item.getString("ForeignName"));
                        itemMaster.setItemGroupId(item.getInt("ItemsGroupCode"));
                        itemMaster.setItemTypeId(item.getString("ItemType").equalsIgnoreCase("ititems") ? 1 :
                                item.getString("ItemType").equalsIgnoreCase("itlabor") ? 2 : 3);
                        itemMaster.setBarcode(item.getString("BarCode"));
                        JSONArray itemprice = item.getJSONArray("ItemPrices");
                        realm.beginTransaction();
                        RealmResults<ItemPrice> itemPriceRealmResults = realm.where(ItemPrice.class)
                                .equalTo("itemId", item.getString("ItemCode")).findAll();
                        itemPriceRealmResults.deleteAllFromRealm();
                        realm.commitTransaction();
                        for(int j = 0 ; j < itemprice.length() ; j++)
                        {
                            ItemPrice lastitemprice = realm.where(ItemPrice.class).findAll()
                                    .sort("priceId", Sort.DESCENDING).first();
                            JSONObject price = itemprice.getJSONObject(j);
                            if(!price.getString("Price").equalsIgnoreCase("null"))
                            {
                                realm.beginTransaction();
                                ItemPrice itemPrice = new ItemPrice();
                                itemPrice.setPriceId(lastitemprice == null ? 1 : lastitemprice.getPriceId() + 1);
                                itemPrice.setPriceList(price.getInt("PriceList"));
                                itemPrice.setPrice(price.getInt("Price"));
                                itemPrice.setCurrency(price.getString("Currency"));
                                itemPrice.setItemId(item.getString("ItemCode"));
                                realm.copyToRealmOrUpdate(itemPrice);
                                realm.commitTransaction();
                            }
                        }
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMaster);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                Intent i = new Intent(getActivity(), ItemMasterDetailActivity.class);
                                i.putExtra("item_id", itemMaster.getItemNo());
                                startActivityForResult(i, 100);
                            }
                        });
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if(response.code() == 404)
                {
                    Toast.makeText(getActivity(), "Not Found", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
                    realmHelper.doLoginHana();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void dismissProgressDialog() {
        if(!getActivity().isFinishing() && progressDialog != null && progressDialog.isShowing()) {
            if(progressDialog.isShowing())
                progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        getItemMasterByIdHana(s);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    @Override
    public void onPause() {
        super.onPause();
        onDestroyView();
    }

}
