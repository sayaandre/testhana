package com.example.testhana.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.testhana.Activity.SalesOrderProductActivity;
import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.CartHelper;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.R;

import io.realm.Realm;

public class SalesInvoiceFragment extends Fragment implements SearchView.OnQueryTextListener{

    boolean tabletSize = false;
    MenuItem item;
    CartHelper cartHelper;
    Realm realm;
    SessionManagement session;

    public SalesInvoiceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        cartHelper = new CartHelper(getActivity());
        realm = Realm.getDefaultInstance();
        session = CoreApplication.getInstance().getSession();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_sales_invoice, container, false);
        FragmentManager fragmentManager = getChildFragmentManager();
        if (savedInstanceState == null) {
            if (rootView.findViewById(R.id.fragment_container_left) != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                SalesInvoiceLeftFragment fragment = new SalesInvoiceLeftFragment();
                if(getArguments() != null)
                    fragment.setArguments(getArguments());
                fragmentTransaction.replace(R.id.fragment_container_left, fragment, "salesleft");
                fragmentTransaction.commit();
            }

            if (rootView.findViewById(R.id.fragment_container_right) != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                CartInvoiceFragment fragment = new CartInvoiceFragment();
                if(getArguments() != null)
                    fragment.setArguments(getArguments());
                fragmentTransaction.replace(R.id.fragment_container_right, fragment, "salesright");
                fragmentTransaction.commit();
            }
        }
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        item = menu.findItem(R.id.action_search);
        item.setVisible(true);
        final SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(this);
        item.setOnActionExpandListener( new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
//                MainActivity.isFromSearch = true;
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                //searchView.setQuery("", true);
//                MainActivity.isFromSearch = false;
                return true; // Return true to collapse action view

            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        if(!s.equals("") && !s.isEmpty())
        {
            ItemMaster itemMaster = realm.where(ItemMaster.class)
                    .equalTo("kodeBarang", s).findFirst();
            if(itemMaster != null)
            {
                Intent i = new Intent(getActivity(), SalesOrderProductActivity.class);
                i.putExtra("item_id", itemMaster.getItemNo());
                i.putExtra("mode", 0);
                startActivityForResult(i, 100);
            }
            else
            {
                Toast.makeText(getActivity(), "Item Not Found", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(getActivity(), "Fill Item Id", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }
}
