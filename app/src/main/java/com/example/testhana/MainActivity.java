package com.example.testhana;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.testhana.Activity.BaseActivity;
import com.example.testhana.Activity.CartActivity;
import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Fragment.CustomerMasterFragment;
import com.example.testhana.Fragment.CustomerMasterListFragment;
import com.example.testhana.Fragment.DashboardFragment;
import com.example.testhana.Fragment.HistoryFragment;
import com.example.testhana.Fragment.InvoiceFragment;
import com.example.testhana.Fragment.InvoiceWarehouseFragment;
import com.example.testhana.Fragment.ItemMasterFragment;
import com.example.testhana.Fragment.ItemMasterListFragment;
import com.example.testhana.Fragment.OrderCustomerFragment;
import com.example.testhana.Fragment.OrderWarehouseCustomerFragment;
import com.example.testhana.Fragment.SalesInvoiceFragment;
import com.example.testhana.Fragment.SalesOrderFragment;
import com.example.testhana.Fragment.SignInFragment;
import com.example.testhana.Helper.CartHelper;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.Response.UpdateTokenFBResponse;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;
    private MenuItem activeMenuItem;
    int lastselected = R.id.nav_dashboard;
    int currselected = R.id.nav_dashboard;
    String selectedTitle = "Sales";
    @BindView(R.id.nav_view) NavigationView navigationView;
    int mNotificationCount;
    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;
    boolean flagGPS = false;
    SessionManagement session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawerLayout = findViewById(R.id.drawer_layout);
//        Toolbar toolbar = findViewById(R.id.toolbar_main);
//        setSupportActionBar(toolbar);
        setTitle("");
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.baseline_menu_white_18);
        session = CoreApplication.getInstance().getSession();
        sendRegistrationToServer();
        checkPermissions();
        navigationView = findViewById(R.id.nav_view);
        hideItem(session.getKeyUserRole());
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        Fragment fragment = null;
                        switch (menuItem.getItemId()) {
                            case R.id.nav_dashboard: {
                                fragment = new DashboardFragment();
                                setTitle("Dashboard");
                                currselected = R.id.nav_dashboard;
                                break;
                            }
                            case R.id.nav_item_master: {
                                fragment = new ItemMasterFragment();
                                setTitle("Item Master");
                                currselected = R.id.nav_item_master;
                                break;
                            }
                            case R.id.nav_customer_master: {
                                fragment = new CustomerMasterFragment();
                                setTitle("Customer Master");
                                currselected = R.id.nav_customer_master;
                                break;
                            }
                            case R.id.nav_salesman_master: {
                                fragment = new CustomerMasterListFragment();
                                setTitle("Customer Master");
                                currselected = R.id.nav_customer_master;
                                break;
                            }
                            case R.id.nav_sales: {
                                fragment = new SalesOrderFragment();
                                setTitle("Sales Order");
                                currselected = R.id.nav_sales;
                                break;
                            }
                            case R.id.nav_sales_invoice: {
                                fragment = new SalesInvoiceFragment();
                                setTitle("Sales Invoice");
                                currselected = R.id.nav_sales_invoice;
                                break;
                            }
                            case R.id.nav_history: {
                                fragment = new HistoryFragment();
                                setTitle("History");
                                currselected = R.id.nav_history;
                                break;
                            }
                            case R.id.nav_order_list_Customer: {
                                fragment = new OrderCustomerFragment();
                                setTitle("Sales Order List");
                                currselected = R.id.nav_order_list;
                                break;
                            }
                            case R.id.nav_warehouse_order_list_Customer: {
                                fragment = new OrderWarehouseCustomerFragment();
                                setTitle("Warehouse Order List");
                                currselected = R.id.nav_warehouse_order_list_Customer;
                                break;
                            }
                            case R.id.nav_invoice_list: {
                                fragment = new InvoiceFragment();
                                setTitle("Invoice List");
                                currselected = R.id.nav_invoice_list;
                                break;
                            }
                            case R.id.nav_warehouse_invoice_list: {
                                fragment = new InvoiceWarehouseFragment();
                                setTitle("Warehouse Invoice List");
                                currselected = R.id.nav_warehouse_invoice_list;
                                break;
                            }
                            case R.id.nav_checkloc: {
                                checkPermissions();
                                session.setKeyLatitudeLongitude("0","0", 0);
                                if(flagGPS)
                                {
                                    fragment = new SignInFragment();
                                    setTitle("Check Location");
                                    currselected = R.id.nav_checkloc;
                                }
                                break;
                            }
                            case R.id.nav_logout: {
                                CoreApplication.getInstance().closeDay();
                                break;
                            }
                        }
                        activeMenuItem = navigationView.getMenu().findItem(lastselected);
                        activeMenuItem.setChecked(false);
                        lastselected = currselected;
                        try {
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.content_frame, fragment);
                            fragmentTransaction.commit();
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        drawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        return true;
                    }
                });
        if(session.getKeyUserRole().equalsIgnoreCase("sales"))
        {
            Fragment fragment = new SalesOrderFragment();
            setTitle("Sales Order");
            try {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment);
                fragmentTransaction.commit();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            getCount();
        }
        else
        {
            Fragment fragment = new OrderCustomerFragment();
            setTitle("Sales Order List");
            try {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment);
                fragmentTransaction.commit();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
    }

    public void hideItem(String role)
    {
        Menu navMenu = navigationView.getMenu();
        if(session.getKeyUserRole().equalsIgnoreCase("warehouse"))
        {
            navMenu.findItem(R.id.nav_sales).setVisible(false);
            navMenu.findItem(R.id.nav_item_master).setVisible(false);
            navMenu.findItem(R.id.nav_salesman_master).setVisible(false);
            navMenu.findItem(R.id.nav_dashboard).setVisible(false);
            navMenu.findItem(R.id.nav_order_list_Customer).setVisible(false);
            navMenu.findItem(R.id.nav_invoice_list).setVisible(false);
        }
        else
        {
//            navMenu.findItem(R.id.nav_warehouse_order_list_Customer).setVisible(false);
//            navMenu.findItem(R.id.nav_warehouse_invoice_list).setVisible(false);
//            navMenu.findItem(R.id.nav_order_list_Customer).setVisible(false);
//            navMenu.findItem(R.id.nav_invoice_list).setVisible(false);
        }
    }

    private void updateNotificationsBadge(int count) {
        mNotificationCount = count;

        // force the ActionBar to relayout its MenuItems.
        // onCreateOptionsMenu(Menu) will be called again.
        invalidateOptionsMenu();
    }

    public int getCount() {
        CartHelper helper = new CartHelper(this);
        int size = helper.getTotalItemInCart();
        updateNotificationsBadge(size);
        return size;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCount();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
            return true;
        }
        if(item.getItemId() == R.id.action_search){
            return false;
        }
        else if(item.getItemId() == R.id.action_cart)
        {
            Intent i = new Intent(MainActivity.this,
                    CartActivity.class);
            startActivity(i);
        }
        switch (item.getItemId()) {
            case android.R.id.home:
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_item_master: {
                Fragment myf = new ItemMasterListFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.content_frame, myf);
                transaction.commit();
                break;
            }
            case R.id.nav_customer_master: {
                Fragment myf = new CustomerMasterListFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.content_frame, myf);
                transaction.commit();
                break;
            }
            case R.id.nav_sales: {
                Fragment myf = new SalesOrderFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.content_frame, myf);
                transaction.commit();
                break;
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == 100)
            {

            }
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item_main, menu);
        MenuItem item = menu.findItem(R.id.action_cart);
        item.setIcon(buildCounterDrawable(mNotificationCount, R.drawable.baseline_shopping_cart_white_24));
        if(session.getKeyUserRole().equalsIgnoreCase("sales"))
        {
            item.setVisible(true);
        }
        else
        {
            item.setVisible(false);
        }
        return true;
    }

    private Drawable buildCounterDrawable(int count, int backgroundImageId) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.counter_menuitem_layout, null);
        view.setBackgroundResource(backgroundImageId);

        if (count == 0) {
            View counterTextPanel = view.findViewById(R.id.counterValuePanel);
            counterTextPanel.setVisibility(View.GONE);
        } else {
            TextView textView = view.findViewById(R.id.count);
            if(count > 99) {
                textView.setText("99+");
            } else {
                textView.setText("" + count);
            }
        }

        view.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        view.setDrawingCacheEnabled(true);
        view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);

        return new BitmapDrawable(getResources(), bitmap);
    }

    private void checkPermissions(){
        int permissionLocation = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
                flagGPS = false;
            }
        }
        else
        {
            flagGPS = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            flagGPS = true;
        }
        else
        {
            flagGPS = false;
        }
    }

    private void sendRegistrationToServer() {
        // Add custom implementation, as needed.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        SessionManagement session = CoreApplication.getInstance().getSession();
        int userId = session.getKeyUserId();
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<UpdateTokenFBResponse> call = apiService.updateTokenFB(userId, refreshedToken);
        call.enqueue(new Callback<UpdateTokenFBResponse>() {
            @Override
            public void onResponse(Call<UpdateTokenFBResponse> call, final Response<UpdateTokenFBResponse> response) {
                if(response.isSuccessful())
                {

                }
            }

            @Override
            public void onFailure(Call<UpdateTokenFBResponse> call, Throwable t) {

            }
        });
    }
}
