package com.example.testhana.Core;

import android.app.Application;
import android.content.Intent;


import com.example.testhana.Activity.LoginActivity;
import com.example.testhana.Helper.SessionManagement;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class CoreApplication extends Application {

    public static CoreApplication app;
    SessionManagement session;

    @Override
    public void onCreate() {
        super.onCreate();
        session = new SessionManagement(this);
        // The default Realm file is "default.realm" in Context.getFilesDir();
        // we'll change it to "myrealm.realm"
        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder()
                .name("POS.realm")
                .schemaVersion(0)
                .build();
        Realm.setDefaultConfiguration(realmConfig);
        app = this;
    }

    public static CoreApplication getInstance() {
        return app;
    }

    public SessionManagement getSession() {
        return session;
    }

    public void closeDay()
    {
        session.clearAll();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
