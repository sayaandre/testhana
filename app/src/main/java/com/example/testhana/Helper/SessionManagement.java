package com.example.testhana.Helper;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManagement {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    public static final String KEY_USER_PHONE = "userPhone";
    public static final String KEY_USER_ID = "userId";
    public static final String KEY_USER_NAME = "userName";
    public static final String KEY_USER_ROLE = "userRole";
    public static final String KEY_CUSTOMER_CART = "customerCart";
    public static final String KEY_SESSION_METADATA = "sessionMetaData";
    public static final String KEY_SESSION_ID = "sessionId";
    public static final String KEY_VERSION = "version";
    public static final String KEY_SESSION_TIMEOUT = "sessionTimeout";
    public static final String KEY_SESSION_PREV_ITEM = "prevItem";
    public static final String KEY_SESSION_CURR_ITEM = "currItem";
    public static final String KEY_SESSION_NEXT_ITEM = "nextItem";
    public static final String KEY_SESSION_PREV_CUST = "prevCust";
    public static final String KEY_SESSION_CURR_CUST = "currCust";
    public static final String KEY_SESSION_NEXT_CUST = "nextCust";
    public static final String KEY_LOCATION_ID = "locationId";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";

    int PRIVATE_MODE = 0;

    public SessionManagement(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences("POSVisIntech", PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setKeyLoginHana(String metadata, String sessionid, String version, String sessiontimeout) {
        editor.putString(KEY_SESSION_METADATA, metadata);
        editor.putString(KEY_SESSION_ID, sessionid);
        editor.putString(KEY_VERSION, version);
        editor.putString(KEY_SESSION_TIMEOUT, sessiontimeout);
        editor.commit();
    }

    public String getKeySessionTimeout() {
        return pref.getString(KEY_SESSION_TIMEOUT, "");
    }

    public String getKeySessionMetadata() {
        return pref.getString(KEY_SESSION_METADATA, "");
    }

    public String getKeySessionId() {
        return pref.getString(KEY_SESSION_ID, "");
    }

    public String getKeyVersion() {
        return pref.getString(KEY_VERSION, "");
    }

    public void setKeyCustomer(String customerId) {
        editor.putString(KEY_CUSTOMER_CART, customerId);
        editor.commit();
    }

    public String getKeyCustomer() {
        return pref.getString(KEY_CUSTOMER_CART, "");
    }

    public void setKeyUserPhone(String phone) {
        editor.putString(KEY_USER_PHONE, phone);
        editor.commit();
    }

    public String getKeyUserPhone() {
        return pref.getString(KEY_USER_PHONE, "");
    }

    public void setKeyUserId(int id) {
        editor.putInt(KEY_USER_ID, id);
        editor.commit();
    }

    public int getKeyUserId() {
        return pref.getInt(KEY_USER_ID, 0);
    }

    public void setKeyUserName(String name) {
        editor.putString(KEY_USER_NAME, name);
        editor.commit();
    }

    public String getKeyUserName() {
        return pref.getString(KEY_USER_NAME, "");
    }

    public void setKeyUserRole(String role) {
        editor.putString(KEY_USER_ROLE, role);
        editor.commit();
    }

    public String getKeyUserRole() {
        return pref.getString(KEY_USER_ROLE, "");
    }

    public void setKeyPrevItem(String name) {
        editor.putString(KEY_SESSION_PREV_ITEM, name);
        editor.commit();
    }

    public String getKeyPrevItem() {
        return pref.getString(KEY_SESSION_PREV_ITEM, "");
    }

    public void setKeyCurrItem(String name) {
        editor.putString(KEY_SESSION_CURR_ITEM, name);
        editor.commit();
    }

    public String getKeyCurrItem() {
        return pref.getString(KEY_SESSION_CURR_ITEM, "");
    }

    public void setKeyNextItem(String name) {
        editor.putString(KEY_SESSION_NEXT_ITEM, name);
        editor.commit();
    }

    public String getKeyNextItem() {
        return pref.getString(KEY_SESSION_NEXT_ITEM, "");
    }

    public void setKeyPrevCust(String name) {
        editor.putString(KEY_SESSION_PREV_CUST, name);
        editor.commit();
    }

    public String getKeyPrevCust() {
        return pref.getString(KEY_SESSION_PREV_CUST, "");
    }

    public void setKeyCurrCust(String name) {
        editor.putString(KEY_SESSION_CURR_CUST, name);
        editor.commit();
    }

    public String getKeyCurrCust() {
        return pref.getString(KEY_SESSION_CURR_CUST, "");
    }

    public void setKeyNextCust(String name) {
        editor.putString(KEY_SESSION_NEXT_CUST, name);
        editor.commit();
    }

    public String getKeyNextCust() {
        return pref.getString(KEY_SESSION_NEXT_CUST, "");
    }

    public void setKeyLatitudeLongitude(String latitude, String longitude, int locid) {
        editor.putInt(KEY_LOCATION_ID, locid);
        editor.putString(KEY_LATITUDE, latitude);
        editor.putString(KEY_LONGITUDE, longitude);
        editor.commit();
    }

    public int getKeyLocationId() {
        return pref.getInt(KEY_LOCATION_ID, 0);
    }

    public String getKeyLatitude() {
        return pref.getString(KEY_LATITUDE, "0");
    }

    public String getKeyLongitude() {
        return pref.getString(KEY_LONGITUDE, "0");
    }

    public void clearAll() {
        editor.putString(KEY_USER_PHONE, "");
        editor.putInt(KEY_USER_ID, 0);
        editor.putString(KEY_USER_NAME, "");
        editor.putString(KEY_CUSTOMER_CART, "");
        editor.putString(KEY_SESSION_METADATA, "");
        editor.putString(KEY_SESSION_ID, "");
        editor.putInt(KEY_VERSION, 0);
        editor.putString(KEY_SESSION_TIMEOUT, "");
        editor.putInt(KEY_LOCATION_ID, 0);
        editor.putString(KEY_LATITUDE, "0");
        editor.putString(KEY_LONGITUDE, "0");
        editor.commit();
    }
}
