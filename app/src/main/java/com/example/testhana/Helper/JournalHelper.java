package com.example.testhana.Helper;

import android.content.Context;


import com.example.testhana.Models.Model.JournalDetailModel;
import com.example.testhana.Models.Model.JournalModel;
import com.example.testhana.Models.Model.SalesOrderDetailModel;
import com.example.testhana.Models.Model.SalesOrderModel;
import com.example.testhana.Models.Realm.Journal;
import com.example.testhana.Models.Realm.JournalDetail;
import com.example.testhana.Models.Realm.SalesOrder;
import com.example.testhana.Models.Realm.SalesOrderDetail;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class JournalHelper {
    private Realm realm;

    public JournalHelper(Context context) {
        realm = Realm.getDefaultInstance();
    }

    public List<JournalModel> getHistory()
    {
        List<JournalModel> journalModels = new ArrayList<>();
        RealmResults<Journal> journals = realm.where(Journal.class).findAll()
                .sort("journalId", Sort.DESCENDING);
        for(int i = 0 ; i < journals.size() ; i++)
        {
            JournalModel journalModel = new JournalModel(journals.get(i).getJournalId(),
                    journals.get(i).getReceiptNumber(), journals.get(i).getTotal(),
                    journals.get(i).getRowCount(), journals.get(i).getTrxType(),
                    journals.get(i).getTrxDate(), journals.get(i).getTrxTime(),
                    journals.get(i).getCashierId(), journals.get(i).getCustomerId(),
                    journals.get(i).getRefNo(), journals.get(i).getDocentry(),
                    journals.get(i).getDocnum());
            journalModels.add(journalModel);
        }
        return journalModels;
    }

    public List<JournalDetailModel> getHistoryDetail(long journalId)
    {
        List<JournalDetailModel> journalDetailModels = new ArrayList<>();
        RealmResults<JournalDetail> journalDetails = realm.where(JournalDetail.class)
                .equalTo("journalId", journalId)
                .equalTo("journalType", "S").findAll();
        for(int i = 0 ; i < journalDetails.size() ; i++)
        {
            JournalDetailModel journalDetailModel = new JournalDetailModel(journalDetails.get(i).getJournalDetailId(),
                    journalDetails.get(i).getJournalId(), journalDetails.get(i).getItemId(),
                    journalDetails.get(i).getItemName(), journalDetails.get(i).getItemPrice(),
                    journalDetails.get(i).getItemQty(), journalDetails.get(i).getItemTotalPrice(),
                    journalDetails.get(i).getNotes(), journalDetails.get(i).getJournalType());
            journalDetailModels.add(journalDetailModel);
        }
        return journalDetailModels;
    }

    public List<SalesOrderModel> getSalesOrder()
    {
        List<SalesOrderModel> journalModels = new ArrayList<>();
        RealmResults<SalesOrder> journals = realm.where(SalesOrder.class).findAll()
                .sort("trxTime", Sort.ASCENDING);
        for(int i = 0 ; i < journals.size() ; i++)
        {
            SalesOrderModel journalModel = new SalesOrderModel(journals.get(i).getSalesOrderId(),
                    journals.get(i).getSalesOrderNum(), journals.get(i).getTotal(),
                    journals.get(i).getRowCount(),
                    journals.get(i).getTrxDate(), journals.get(i).getTrxTime(),
                    journals.get(i).getUserId(), journals.get(i).getCustomerId(),
                    journals.get(i).getStatus());
            journalModels.add(journalModel);
        }
        return journalModels;
    }

    public List<SalesOrderDetailModel> getSalesOrderDetail(long journalId)
    {
        List<SalesOrderDetailModel> journalDetailModels = new ArrayList<>();
        RealmResults<SalesOrderDetail> journalDetails = realm.where(SalesOrderDetail.class)
                .equalTo("salesOrderId", journalId).findAll();
        for(int i = 0 ; i < journalDetails.size() ; i++)
        {
            SalesOrderDetailModel journalDetailModel = new SalesOrderDetailModel(journalDetails.get(i).getSalesOrderDetailId(),
                    journalDetails.get(i).getSalesOrderId(), journalDetails.get(i).getItemId(),
                    journalDetails.get(i).getItemName(), journalDetails.get(i).getItemQty(),
                    journalDetails.get(i).getNotes(), journalDetails.get(i).getStatus(), journalDetails.get(i).getItemBatch());
            journalDetailModels.add(journalDetailModel);
        }
        return journalDetailModels;
    }
}
