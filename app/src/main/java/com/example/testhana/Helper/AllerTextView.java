package com.example.testhana.Helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.example.testhana.R;


public class AllerTextView extends AppCompatTextView
{
  public AllerTextView(Context context)
  {
    super(context);
    if(isInEditMode()) {
        return;
    }
    parseAttributes(null);
  }

  public AllerTextView(Context context, AttributeSet attrs)
  {
    super(context, attrs);
    if(isInEditMode()) {
        return;
    }
    parseAttributes(attrs);
  }

  public AllerTextView(Context context, AttributeSet attrs, int typeface)
  {
    super(context, attrs, typeface);
    if(isInEditMode()) {
        return;
    }
    parseAttributes(attrs);
  }

  private Typeface getAller(int typeface)
  {
    return getAller(getContext(), typeface);
  }

  public static Typeface getAller(Context context, int typeface)
  {
    Typeface localTypeface;
    switch (typeface)
    {
    	case Aller.ALLER_BOLD :
    		if(Aller.sAllerBold == null)
              Aller.sAllerBold = Typeface.createFromAsset(context.getAssets(), "fonts/Aller-Bold.ttf");
    		localTypeface = Aller.sAllerBold;break;
    	case Aller.ALLER_ITALIC :
    		if(Aller.sAllerItalic == null)
              Aller.sAllerItalic = Typeface.createFromAsset(context.getAssets(), "fonts/Aller-Italic.ttf");
    		localTypeface = Aller.sAllerItalic;break;
    	default :
    		if(Aller.sAllerReguler == null)
              Aller.sAllerReguler = Typeface.createFromAsset(context.getAssets(), "fonts/Aller-Reg.ttf");
    		localTypeface = Aller.sAllerReguler;break;
    }
    return localTypeface;
  }

  private void parseAttributes(AttributeSet attrs)
  {
	  int typeface;
	  if (attrs == null)
		  typeface = Aller.ALLER_REGULAR;
	  else {
		  TypedArray values = getContext().obtainStyledAttributes(attrs, R.styleable.AllerTextView);
		  typeface = values.getInt(R.styleable.AllerTextView_typeface, Aller.ALLER_REGULAR);
		  values.recycle();
	  }
	  setTypeface(getAller(typeface));
  }

  public void setTexGyreTypeface(int paramInt)
  {
	  setTypeface(getAller(paramInt));
  }

  public static class Aller
  {
    public static final int ALLER_BOLD = 2;
    public static final int ALLER_ITALIC = 3;
    public static final int ALLER_REGULAR = 8;
    private static Typeface sAllerBold;
    private static Typeface sAllerItalic;
    private static Typeface sAllerReguler;
  }
}