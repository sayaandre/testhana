package com.example.testhana.Helper;

import com.example.testhana.Models.Model.ItemMasterModel;
import com.example.testhana.Models.Model.LocationCheckInModel;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.Models.Realm.LocationCheckIn;

import java.util.ArrayList;
import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;

public class LocationHelper {

    Realm realm;

    public LocationHelper(Realm mRealm){
        realm = mRealm;
    }

    public List<LocationCheckInModel> getLocation()
    {
        List<LocationCheckInModel> locationCheckInModels = new ArrayList<>();
        RealmResults<LocationCheckIn> locationCheckIns = realm.where(LocationCheckIn.class).findAll();
        for(int i = 0 ; i < locationCheckIns.size() ; i++)
        {
            LocationCheckInModel locationCheckInModel = new LocationCheckInModel(locationCheckIns.get(i).getLocationId(),
                    locationCheckIns.get(i).getLocationName(), locationCheckIns.get(i).getLocationCity(),
                    locationCheckIns.get(i).getLocationAddress(), locationCheckIns.get(i).getLatitude(),
                    locationCheckIns.get(i).getLongitude(), locationCheckIns.get(i).isCheckIn());
            locationCheckInModels.add(locationCheckInModel);
        }
        return locationCheckInModels;
    }
}
