package com.example.testhana.Helper;

import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.ApiServiceHana;
import com.example.testhana.Rest.ModelClass.LoginHanaClass;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RealmHelper {

    SessionManagement session;
    ApiServiceHana apiHana;
    Realm realm;

    public RealmHelper(){
        realm = Realm.getDefaultInstance();
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        session = CoreApplication.getInstance().getSession();
    }

    public void doLoginHana()
    {
        LoginHanaClass loginHanaClass = new LoginHanaClass(Constant.COMPANY_DB, Constant.COMPANY_PASS, Constant.COMPANY_USERNAME);
        Call<ResponseBody> call = apiHana.login(loginHanaClass);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        res = new String(response.body().bytes());
                        JSONObject obj = new JSONObject(res);
                        if(obj.getString("SessionId") != null)
                        {
                            session.setKeyLoginHana(obj.getString("odata.metadata"),
                                    obj.getString("SessionId"), obj.getString("Version"),
                                    obj.getString("SessionTimeout"));
                        }
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                {

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
