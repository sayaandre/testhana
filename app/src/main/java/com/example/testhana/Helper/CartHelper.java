package com.example.testhana.Helper;

import android.content.Context;


import com.example.testhana.Models.Model.CartModel;
import com.example.testhana.Models.Model.CartOrderModel;
import com.example.testhana.Models.Model.Discount;
import com.example.testhana.Models.Realm.Cart;
import com.example.testhana.Models.Realm.CartOrder;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class CartHelper {
    private Realm realm;

    public CartHelper(Context context) {
        realm = Realm.getDefaultInstance();
    }

    public void addCart(String id, String name, int qty, String notes, double total, double price)
    {
        realm.beginTransaction();
        Cart cart = realm.where(Cart.class).equalTo("itemId", id).findFirst();
        if(cart == null)
        {
            cart = new Cart();
            cart.setRowNo(Tools.dapatkanId());
            cart.setItemId(id);
            cart.setItemDesc(name);
            cart.setItemPrice(price);
        }
        cart.setItemQty(qty);
        cart.setRemarks(notes);
        cart.setTotal(total);
        realm.copyToRealmOrUpdate(cart);
        realm.commitTransaction();
    }

    public void addCartOrder(String id, String name, int qty, String notes, double total, double price, String batch)
    {
        realm.beginTransaction();
        CartOrder cart = realm.where(CartOrder.class).equalTo("itemId", id).findFirst();
//        if(cart == null)
//        {
            cart = new CartOrder();
            cart.setRowNo(Tools.dapatkanId());
            cart.setItemId(id);
            cart.setItemDesc(name);
            cart.setItemPrice(price);
//        }
        cart.setItemBatch(batch.equalsIgnoreCase("choose") ? "" : batch);
        cart.setItemQty(qty);
        cart.setRemarks(notes);
        cart.setTotal(total);
        realm.copyToRealmOrUpdate(cart);
        realm.commitTransaction();
    }

    public void addCartFromHome(String id, String name, int qty, String notes, double total, double price, double aa)
    {
        realm.beginTransaction();
        Cart cart = realm.where(Cart.class).equalTo("itemId", id).findFirst();
        if(cart == null)
        {
            cart = new Cart();
            cart.setRowNo(Tools.dapatkanId());
            cart.setItemId(id);
            cart.setItemDesc(name);
            cart.setItemPrice(price);
        }
        cart.setItemQty(cart == null ? qty : cart.getItemQty() + qty);
        cart.setTotal(total);
        cart.setRemarks(notes);
        realm.copyToRealmOrUpdate(cart);
        realm.commitTransaction();
    }

    public void addCartOrderFromHome(String id, String name, int qty, String notes, double total, double price, String batch,
                                     double aa)
    {
        realm.beginTransaction();
        CartOrder cart = realm.where(CartOrder.class).equalTo("itemId", id).findFirst();
//        if(cart == null)
//        {
            cart = new CartOrder();
            cart.setRowNo(Tools.dapatkanId());
            cart.setItemId(id);
            cart.setItemDesc(name);
            cart.setItemPrice(price);
//        }
        cart.setItemBatch(batch.equalsIgnoreCase("choose") ? "" : batch);
        cart.setItemQty(cart == null ? qty : cart.getItemQty() + qty);
        cart.setTotal(total);
        cart.setRemarks(notes);
        realm.copyToRealmOrUpdate(cart);
        realm.commitTransaction();
    }

    public List<CartModel> getCart()
    {
        List<CartModel> cartModels = new ArrayList<>();
        RealmResults<Cart> carts = realm.where(Cart.class).findAll();
        for(int i = 0 ; i < carts.size() ; i++)
        {
            CartModel cartModel = new CartModel(carts.get(i).getRowNo(),
                    carts.get(i).getItemId(), carts.get(i).getItemDesc(),
                    carts.get(i).getItemPrice(),
                    carts.get(i).getItemQty(), carts.get(i).getRemarks(),
                    carts.get(i).getTotal(), new ArrayList<Discount>());
            cartModels.add(cartModel);
        }
        return cartModels;
    }

    public List<CartOrderModel> getCartOrder()
    {
        List<CartOrderModel> cartModels = new ArrayList<>();
        RealmResults<CartOrder> carts = realm.where(CartOrder.class).findAll();
        for(int i = 0 ; i < carts.size() ; i++)
        {
            CartOrderModel cartModel = new CartOrderModel(carts.get(i).getRowNo(),
                    carts.get(i).getItemId(), carts.get(i).getItemDesc(),
                    carts.get(i).getItemPrice(),
                    carts.get(i).getItemQty(), carts.get(i).getRemarks(),
                    carts.get(i).getTotal(), carts.get(i).getItemBatch(), new ArrayList<Discount>());
            cartModels.add(cartModel);
        }
        return cartModels;
    }

    public int getTotalItemInCart() {
        int total = realm.where(CartOrder.class)
                .sum("itemQty").intValue();
        return total;
    }

    public double getTotalInCart() {
        double total = realm.where(CartOrder.class)
                .sum("total").doubleValue();
        return total;
    }

    public int getTotalItemInCartInvoice() {
        int total = realm.where(Cart.class)
                .sum("itemQty").intValue();
        return total;
    }

    public double getTotalInCartInvoice() {
        double total = realm.where(Cart.class)
                .sum("total").doubleValue();
        return total;
    }
}
