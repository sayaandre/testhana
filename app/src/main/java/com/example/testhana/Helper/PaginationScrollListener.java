package com.example.testhana.Helper;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by andre.ferando on 8/9/2018.
 */

public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener{

    //LinearLayoutManager layoutManager;
    GridLayoutManager gridLayoutManager;

    /**
     * Supporting only LinearLayoutManager for now.
     *
     * @param gridLayoutManager
     */
    public PaginationScrollListener(GridLayoutManager gridLayoutManager) {
        //this.layoutManager = layoutManager;
        this.gridLayoutManager = gridLayoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        /*int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

        if (!isLoading() && !isLastPage()) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                    && firstVisibleItemPosition >= 0
                    && totalItemCount >= getTotalPageCount()) {
                loadMoreItems();
            }
        }*/

        //if(dy > 0) {

            int totalItemCount = gridLayoutManager.getItemCount();
            int lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();
            if (!isLoading() && !isLastPage() && totalItemCount <= (lastVisibleItem + 2)) {
                loadMoreItems();
            }
            /*int visibleItemCount = gridLayoutManager.getChildCount();
            int totalItemCount = gridLayoutManager.getItemCount();
            int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();

            if (!isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }*/
        //}

    }

    protected abstract void loadMoreItems();

    public abstract int getTotalPageCount();

    public abstract boolean isLastPage();

    public abstract boolean isLoading();
}
