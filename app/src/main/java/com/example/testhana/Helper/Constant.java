package com.example.testhana.Helper;

public class Constant {

    public static String baseUrl = "http://192.168.1.97:8888/api/";
    public static String baseUrlDirectHana = "http://192.168.1.97:2449/api/";
    public static String hanaUrl = "https://192.168.1.144:50000/b1s/v1/";
    public static String otpUrl = "https://api.wavecell.com/otp/v1/";

    public static int ADD = 1;
    public static int UPDATE = 2;
    public static int DELETE = 3;

    public static int GRP_PERCENTAGE = -3;
    public static int GRP_AMOUNT = -4;

    public static int ITEM_PERCENTAGE = -1;
    public static int ITEM_AMOUNT = -2;

    public static String COMPANY_DB = "SBODEMOAU";
    public static String COMPANY_PASS = "1234";
    public static String COMPANY_USERNAME = "VIT-1";

    public static String SELECT_ITEM = "ItemCode,ItemName,ForeignName,ItemType,ItemsGroupCode,BarCode,ItemPrices,ItemWarehouseInfoCollection";
    public static String SELECT_CUSTOMER = "CardCode,CardName,CardType,GroupCode,Currency,Phone1,Address,EmailAddress";

    public static int RADIUS = 30;
}
