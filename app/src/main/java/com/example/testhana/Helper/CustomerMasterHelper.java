package com.example.testhana.Helper;


import com.example.testhana.Models.Model.CustomerMasterModel;
import com.example.testhana.Models.Realm.CustomerCurrencyMaster;
import com.example.testhana.Models.Realm.CustomerGroupMaster;
import com.example.testhana.Models.Realm.CustomerMaster;
import com.example.testhana.Models.Realm.CustomerRoleMaster;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class CustomerMasterHelper {

    Realm realm;

    public CustomerMasterHelper(Realm mRealm){
        realm = mRealm;
    }

    public List<CustomerMasterModel> getCustomerMaster(int from, int to)
    {
        realm = Realm.getDefaultInstance();
        List<CustomerMasterModel> customerMasterModels = new ArrayList<>();
        RealmResults<CustomerMaster> customerMasters = realm.where(CustomerMaster.class).findAll();
        for(int i = from ; i < to ; i++)
        {
            CustomerMasterModel customerMasterModel = new CustomerMasterModel(customerMasters.get(i).getCustomerId(),
                    customerMasters.get(i).getCustomerCode(), customerMasters.get(i).getCustomerRoleId(),
                    customerMasters.get(i).getCustomerName(), customerMasters.get(i).getCustomerGroupId(),
                    customerMasters.get(i).getCustomerCurrencyId(), customerMasters.get(i).getCustomerPhone(),
                    customerMasters.get(i).getCustomerAddress(), customerMasters.get(i).getCustomerEmail(),
                    customerMasters.get(i).getCustomerImage(), customerMasters.get(i).getCustomerBarcode());
            customerMasterModels.add(customerMasterModel);
        }
        return customerMasterModels;
    }

    public String[] getCustomerRole()
    {
        RealmResults<CustomerRoleMaster> customerRoleMasters = realm.where(CustomerRoleMaster.class).findAll();
        String[] itemType = new String[customerRoleMasters.size()];
        for(int i = 0 ; i < customerRoleMasters.size() ; i++)
        {
            itemType[i] = customerRoleMasters.get(i).getRoleName();
        }
        return itemType;
    }

    public String[] getCustomerGroup()
    {
        RealmResults<CustomerGroupMaster> customerGroupMasters = realm.where(CustomerGroupMaster.class).findAll();
        String[] itemType = new String[customerGroupMasters.size()];
        for(int i = 0 ; i < customerGroupMasters.size() ; i++)
        {
            itemType[i] = customerGroupMasters.get(i).getCustomerGroupName();
        }
        return itemType;
    }

    public String[] getCustomerCurrency()
    {
        RealmResults<CustomerCurrencyMaster> customerCurrencyMasters = realm.where(CustomerCurrencyMaster.class).findAll();
        String[] itemType = new String[customerCurrencyMasters.size()];
        for(int i = 0 ; i < customerCurrencyMasters.size() ; i++)
        {
            itemType[i] = customerCurrencyMasters.get(i).getName();
        }
        return itemType;
    }
}
