package com.example.testhana.Helper;


import com.example.testhana.Models.Model.ItemMasterModel;
import com.example.testhana.Models.Realm.ItemGroupMaster;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.Models.Realm.ItemTypeMaster;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class ItemMasterHelper {

    Realm realm;

    public ItemMasterHelper(Realm mRealm){
        realm = mRealm;
    }

    public List<ItemMasterModel> getItemMaster(int from, int to)
    {
        List<ItemMasterModel> itemMasterModels = new ArrayList<>();
        RealmResults<ItemMaster> itemMasters = realm.where(ItemMaster.class).findAll();
        for(int i = 0 ; i < 20 ; i++)
        {
            ItemMasterModel itemMasterModel = new ItemMasterModel(itemMasters.get(i).getItemId(), itemMasters.get(i).getItemNo(),
                    itemMasters.get(i).getItemDesc(), itemMasters.get(i).getForeignName(), itemMasters.get(i).getItemTypeId(),
                    itemMasters.get(i).getItemGroupId(), itemMasters.get(i).getUOMGroupId(), itemMasters.get(i).getBarcode(),
                    itemMasters.get(i).getUnitPrice(), itemMasters.get(i).getItemImage(), itemMasters.get(i).getStock());
            itemMasterModels.add(itemMasterModel);
        }
        return itemMasterModels;
    }

    public List<ItemMasterModel> getItemMasterSales(int page)
    {
        List<ItemMasterModel> itemMasterModels = new ArrayList<>();
        RealmResults<ItemMaster> itemMasters = realm.where(ItemMaster.class).findAll().sort("itemDesc", Sort.ASCENDING);
        for(int i = 0 ; i < (itemMasters.size() != 0 ? page * 10 : itemMasters.size()) ; i++)
        {
            ItemMasterModel itemMasterModel = new ItemMasterModel(itemMasters.get(i).getItemId(), itemMasters.get(i).getItemNo(),
                    itemMasters.get(i).getItemDesc(), itemMasters.get(i).getForeignName(), itemMasters.get(i).getItemTypeId(),
                    itemMasters.get(i).getItemGroupId(), itemMasters.get(i).getUOMGroupId(), itemMasters.get(i).getBarcode(),
                    itemMasters.get(i).getUnitPrice(), itemMasters.get(i).getItemImage(), itemMasters.get(i).getStock());
            itemMasterModels.add(itemMasterModel);
        }
        return itemMasterModels;
    }

    public String[] getItemType()
    {
        RealmResults<ItemTypeMaster> itemMasters = realm.where(ItemTypeMaster.class).findAll();
        String[] itemType = new String[itemMasters.size()];
        for(int i = 0 ; i < itemMasters.size() ; i++)
        {
            itemType[i] = itemMasters.get(i).getItemTypeName();
        }
        return itemType;
    }

    public String[] getItemGroup()
    {
        RealmResults<ItemGroupMaster> itemMasters = realm.where(ItemGroupMaster.class).findAll();
        String[] itemType = new String[itemMasters.size()];
        for(int i = 0 ; i < itemMasters.size() ; i++)
        {
            itemType[i] = itemMasters.get(i).getItemGroupName();
        }
        return itemType;
    }
}
