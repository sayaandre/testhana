package com.example.testhana.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testhana.Adapter.LocationAdapter;
import com.example.testhana.Helper.LocationHelper;
import com.example.testhana.Helper.Tools;
import com.example.testhana.Helper.Util;
import com.example.testhana.Models.Model.LocationCheckInModel;
import com.example.testhana.Models.Realm.LocationCheckIn;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.Response.GetLocationResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationCheckInListActivity extends BaseActivity {

    @BindView(R.id.rvItemMaster) RecyclerView rvItem;
    @BindView(R.id.txtDataFound) TextView txtDataFound;
    @BindView(R.id.swipeLayout) SwipeRefreshLayout swipeLayout;
    SwipeRefreshLayout.OnRefreshListener refreshListener;
    private LocationAdapter adapter;
    List<LocationCheckInModel> itemMasterModelList;
    LocationHelper helper;
    ApiService api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_check_in_list);
        ButterKnife.bind(this);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        setTitle("Location Check In");
        Tools.onCreateSwipeToRefresh(swipeLayout);
        refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(true);
                getLocation();
            }
        };
        swipeLayout.setOnRefreshListener(refreshListener);
        api = ApiClient.getClient().create(ApiService.class);
        itemMasterModelList = new ArrayList<>();
        helper = new LocationHelper(realm);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        rvItem.setLayoutManager(mLayoutManager);
        rvItem.addItemDecoration(new GridSpacingItemDecoration(1, 0, true));
        rvItem.setItemAnimator(new DefaultItemAnimator());
        setAdapter();
    }

    public void getLocation()
    {
        Call<GetLocationResponse> call = api.getLocation(session.getKeyUserId(), Util.getDate());
        call.enqueue(new Callback<GetLocationResponse>() {
            @Override
            public void onResponse(Call<GetLocationResponse> call, final Response<GetLocationResponse> response) {
                swipeLayout.setRefreshing(false);
                if(response.isSuccessful())
                {
                    if(response.body().getResult().equalsIgnoreCase("ok"))
                    {
                        final List<LocationCheckIn> itemMasters = response.body().getLocations();
                        final Realm mRealm = Realm.getDefaultInstance();
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                setAdapter();
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<GetLocationResponse> call, Throwable t) {
                swipeLayout.setRefreshing(false);
                t.printStackTrace();
                Toast.makeText(LocationCheckInListActivity.this, "No Internet connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setAdapter()
    {
        itemMasterModelList.clear();
        itemMasterModelList.addAll(helper.getLocation());
        if(itemMasterModelList.size() > 0)
        {
            txtDataFound.setVisibility(View.GONE);
        }
        if(adapter == null) {
            adapter = new LocationAdapter(this, itemMasterModelList, new LocationAdapter.OnItemClickListener() {
                @Override
                public void onClick(LocationCheckInModel item) {
                    Intent i = new Intent(LocationCheckInListActivity.this, CheckInOutActivity.class);
                    i.putExtra("latitude", item.getLatitude());
                    i.putExtra("longitude", item.getLongitude());
                    i.putExtra("id", item.getLocationId());
                    startActivityForResult(i, 100);
                }
            });
            rvItem.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == 100)
            {
                session.setKeyLatitudeLongitude(String.valueOf(data.getStringExtra("latitude")),
                        String.valueOf(data.getStringExtra("longitude")), data.getIntExtra("id", 0));
                Intent intent = new Intent();
                intent.putExtra("location_id", data.getIntExtra("id", 0));
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
