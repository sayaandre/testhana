package com.example.testhana.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testhana.Adapter.CartOrderAdapter;
import com.example.testhana.Helper.CartHelper;
import com.example.testhana.Helper.Constant;
import com.example.testhana.Helper.RealmHelper;
import com.example.testhana.Helper.Tools;
import com.example.testhana.Helper.Util;
import com.example.testhana.MainActivity;
import com.example.testhana.Models.Model.CartOrderModel;
import com.example.testhana.Models.Realm.CartOrder;
import com.example.testhana.Models.Realm.SalesOrder;
import com.example.testhana.Models.Realm.SalesOrderDetail;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.ApiServiceHana;
import com.example.testhana.Rest.Response.SubmitSalesOrderResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends BaseActivity {

    @BindView(R.id.imgCancel) ImageButton imgCancel;
    @BindView(R.id.txtCustomer) TextView txtCustomer;
    @BindView(R.id.txtScan) ImageView ivScan;
    @BindView(R.id.txtSearch) ImageView ivSearch;
    @BindView(R.id.btnCustomAmount) Button btnCustomAmount;
    @BindView(R.id.btnClear) Button btnClear;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.btnPay) Button btnPay;
    @BindView(R.id.txtSubtotal) TextView txtSubtotal;
    private CartOrderAdapter adapter;
    List<CartOrderModel> cartModels;
    CartHelper helper;
    ApiServiceHana apiHana;
    RealmHelper realmHelper;
    ApiService api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        setTitle("Cart");
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        cartModels = new ArrayList<>();
        session.setKeyCustomer("");
        realmHelper = new RealmHelper();
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        api = ApiClient.getClient().create(ApiService.class);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        helper = new CartHelper(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, 0, true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        setAdapter();
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder kotakBuilder = new AlertDialog.Builder(CartActivity.this,
                        R.style.AppCompatAlertDialogStyle);
                kotakBuilder.setIcon(android.R.drawable.ic_dialog_alert);
                kotakBuilder.setTitle("Notice");
                kotakBuilder.setMessage("Are You Sure?");
                kotakBuilder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                realm.beginTransaction();
                                RealmResults<CartOrder> carts = realm.where(CartOrder.class).findAll();
                                carts.deleteAllFromRealm();
                                realm.commitTransaction();
                                setAdapter();
                            }
                        });
                kotakBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = kotakBuilder.create();
                dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.rounded_white));
                dialog.show();
            }
        });
        ivScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(CartActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(CartActivity.this, new String[]{Manifest.permission.CAMERA}, 20);
                }
                else
                {
                    Intent i = new Intent(CartActivity.this, ScannerActivity.class);
                    startActivityForResult(i, 95);
                }
            }
        });
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.setKeyCustomer("0");
                imgCancel.setVisibility(View.GONE);
                txtCustomer.setText("Customer");
            }
        });
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!session.getKeyCustomer().equalsIgnoreCase("") && !session.getKeyCustomer().equals("0"))
                {
                    AlertDialog.Builder kotakBuilder = new AlertDialog.Builder(CartActivity.this,
                            R.style.AppCompatAlertDialogStyle);
                    kotakBuilder.setIcon(android.R.drawable.ic_dialog_alert);
                    kotakBuilder.setTitle("Notice");
                    kotakBuilder.setMessage("Are You Sure to Create Order?");
                    kotakBuilder.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    submitSalesOrder();
                                }
                            });
                    kotakBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = kotakBuilder.create();
                    dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(CartActivity.this, R.drawable.rounded_white));
                    dialog.show();
                }
                else
                {
                    Toast.makeText(CartActivity.this, "Please Choose Customer", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 20: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent(CartActivity.this, ScannerActivity.class);
                    startActivityForResult(i, 95);
                } else {

                }
            }
        }
    }

    private void setAdapter()
    {
        cartModels.clear();
        cartModels.addAll(helper.getCartOrder());
        if(adapter == null) {
            adapter = new CartOrderAdapter(this, cartModels, new CartOrderAdapter.OnItemClickListener() {
                @Override
                public void onClick(CartOrderModel item) {
                    Intent i = new Intent(CartActivity.this, SalesOrderProductActivity.class);
                    i.putExtra("item_id", item.getItemId());
                    i.putExtra("mode", 1);
                    startActivityForResult(i, 100);
                }

                @Override
                public void onDeleteClick(CartOrderModel item) {

                }

                @Override
                public void onChangeAction(CartOrderModel item) {
                    AlertDialog.Builder kotakBuilder = new AlertDialog.Builder(CartActivity.this,
                            R.style.AppCompatAlertDialogStyle);
                    kotakBuilder.setIcon(android.R.drawable.ic_dialog_alert);
                    kotakBuilder.setTitle(item.getItemDesc());
                    kotakBuilder.setMessage("In Stock : 10\nIn Stock DreamLand : 50\nAvailable : 10\nCommited : 10\n\nPlease Choose Action Below..");
                    kotakBuilder.setPositiveButton("PreOrder",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    kotakBuilder.setNeutralButton("Request Order",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    kotakBuilder.setNegativeButton("Lost Sales", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog dialog = kotakBuilder.create();
                    dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(CartActivity.this, R.drawable.rounded_white));
                    dialog.show();
                }
            });
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
        setDisplay();
    }

    private void setDisplay()
    {
        txtSubtotal.setText(Tools.convertMoney(this, helper.getTotalInCart()));
        btnPay.setText("Submit");
        if(helper.getTotalInCart() > 0)
        {
            btnPay.setEnabled(true);
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK)
        {
            if(requestCode == 95)
            {
                getBusinessPartnerMasterByIdHana(data.getExtras().getString("content"));
            }
            else if(requestCode == 100)
            {
                setAdapter();
            }
            else if(requestCode == 101)
            {
                finish();
            }
        }
    }

    public void getBusinessPartnerMasterByIdHana(final String bpid)
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Search Customer..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.getBusinessPartnerById(bpid, Constant.SELECT_CUSTOMER, cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        res = new String(response.body().bytes());
                        JSONObject item = new JSONObject(res);
                        session.setKeyCustomer(item.getString("CardCode"));
                        txtCustomer.setText(item.getString("CardName"));
                        imgCancel.setVisibility(View.VISIBLE);
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if(response.code() == 404)
                {
                    Toast.makeText(CartActivity.this, "Not Found", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(CartActivity.this, "Try Again", Toast.LENGTH_SHORT).show();
                    realmHelper.doLoginHana();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(CartActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        session.setKeyCustomer("0");
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            session.setKeyCustomer("0");
            Intent intent = new Intent();
            setResult(Activity.RESULT_OK, intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void submitSalesOrder()
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Submit Payment..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        RealmResults<CartOrder> carts = realm.where(CartOrder.class).findAll();
        String jsonJournalDetail = "";
        final long id = Tools.dapatkanId();
        try
        {
            JSONArray jounralArr = new JSONArray();
            for(int i = 0 ; i < carts.size() ; i++)
            {
                JSONObject journalObj = new JSONObject();
                journalObj.put("itemId", carts.get(i).getItemId());
                journalObj.put("itemName", carts.get(i).getItemDesc());
                journalObj.put("itemQty", carts.get(i).getItemQty());
                journalObj.put("notes", carts.get(i).getRemarks());
                journalObj.put("itemBatch", carts.get(i).getItemBatch());
                journalObj.put("status", "Check Stock");
                jounralArr.put(journalObj);
            }
            jsonJournalDetail = jounralArr.toString();
        }catch (JSONException ex) {
            ex.printStackTrace();
        }
        Call<SubmitSalesOrderResponse> call = api.submitSalesOrder(id + "", helper.getTotalInCart(),
                helper.getTotalItemInCart(),
                Util.getDate(), Util.getTime(), session.getKeyUserId(), session.getKeyCustomer(),
                "Check Stock", jsonJournalDetail);
        call.enqueue(new Callback<SubmitSalesOrderResponse>() {
            @Override
            public void onResponse(Call<SubmitSalesOrderResponse> call, Response<SubmitSalesOrderResponse> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    if(response.body().getResult().equalsIgnoreCase("ok"))
                    {
                        realm.beginTransaction();
                        SalesOrder journal = new SalesOrder();
                        journal.setSalesOrderId(response.body().getId());
                        journal.setSalesOrderNum(id + "");
                        journal.setTotal(helper.getTotalInCart());
                        journal.setRowCount(helper.getTotalItemInCart());
                        journal.setTrxDate(Util.getDate());
                        journal.setTrxTime(Util.getTime());
                        journal.setUserId(session.getKeyUserId());
                        journal.setCustomerId(session.getKeyCustomer());
                        journal.setStatus("Check Stock");
                        realm.copyToRealmOrUpdate(journal);
                        RealmResults<CartOrder> carts = realm.where(CartOrder.class).findAll();
                        for(int i = 0 ; i < carts.size() ; i++)
                        {
                            SalesOrderDetail journalDetail = new SalesOrderDetail();
                            journalDetail.setSalesOrderDetailId(Tools.dapatkanId());
                            journalDetail.setSalesOrderId(response.body().getId());
                            journalDetail.setItemId(carts.get(i).getItemId());
                            journalDetail.setItemName(carts.get(i).getItemDesc());
                            journalDetail.setItemQty(carts.get(i).getItemQty());
                            journalDetail.setNotes(carts.get(i).getRemarks());
                            journalDetail.setStatus("Check Stock");
                            journalDetail.setItemBatch(carts.get(i).getItemBatch());
                            realm.copyToRealmOrUpdate(journalDetail);
                        }
                        carts.deleteAllFromRealm();
                        realm.commitTransaction();
                        session.setKeyCustomer("0");
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                    }
                }
            }

            @Override
            public void onFailure(Call<SubmitSalesOrderResponse> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(CartActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }
}
