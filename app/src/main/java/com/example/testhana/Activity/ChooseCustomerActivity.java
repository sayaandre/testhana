package com.example.testhana.Activity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.testhana.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChooseCustomerActivity extends BaseActivity {

    @BindView(R.id.spinnerCustomer) Spinner spinnerCustomer;
    @BindView(R.id.spinnerAddress) Spinner spinnerAddress;

    private String[] customerList;
    private String[] addressList;
    boolean tabletSize = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        int screenWidth = 0;
        int screenHeight = 0;
        setContentView(R.layout.activity_choose_customer);
        ButterKnife.bind(this);
        if(tabletSize) {
            screenWidth = (int) (metrics.widthPixels * 0.6);
            screenHeight = (int) (metrics.heightPixels * 0.5);
        } else {
            screenWidth = (int) (metrics.widthPixels * 0.85);
            screenHeight = (int) (metrics.heightPixels * 0.95);
        }
        getWindow().setLayout(screenWidth, screenHeight);
        customerList = new String[5];
        customerList[0] = "C101203 - Andrew Garfield";
        customerList[1] = "C101204 - Emma Stone";
        customerList[2] = "C101205 - Peter Parker";
        customerList[3] = "C101301 - Gwen Stacy";
        customerList[4] = "C201210 - Mary Jane";
        addressList = new String[2];
        addressList[0] = "Jl. Pangeran Sudirman No.12, Bandar Lampung, Lampung";
        addressList[1] = "Jl. Gajah Mada No. 40, Jakarta Barat, Jakarta";
        final ArrayAdapter<String> adaptergroup = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, customerList);
        spinnerCustomer.setAdapter(adaptergroup);
        spinnerCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        final ArrayAdapter<String> adapteraddress = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, addressList);
        spinnerAddress.setAdapter(adapteraddress);
        spinnerAddress.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
