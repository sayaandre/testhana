package com.example.testhana.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.testhana.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomerDetailActivity extends BaseActivity {

    @BindView(R.id.txtCustomerCode) EditText txtCustomerCode;
    @BindView(R.id.txtCustomerName) EditText txtCustomerName;
    @BindView(R.id.txtCustomerPhone) EditText txtCustomerPhone;
    @BindView(R.id.txtCustomerEmail) EditText txtCustomerEmail;
    @BindView(R.id.txtCustomerAddress) EditText txtCustomerAddress;
    boolean tabletSize = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        int screenWidth = 0;
        int screenHeight = 0;
        setContentView(R.layout.activity_customer_detail);
        ButterKnife.bind(this);
        if(tabletSize) {
            screenWidth = (int) (metrics.widthPixels * 0.6);
            screenHeight = (int) (metrics.heightPixels * 0.5);
        } else {
            screenWidth = (int) (metrics.widthPixels * 0.7);
            screenHeight = (int) (metrics.heightPixels * 0.55);
        }
        getWindow().setLayout(screenWidth, screenHeight);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null)
        {
            txtCustomerCode.setText(bundle.getString("code"));
            txtCustomerName.setText(bundle.getString("name"));
            txtCustomerPhone.setText(bundle.getString("phone"));
            txtCustomerEmail.setText(bundle.getString("email"));
            txtCustomerAddress.setText(bundle.getString("address"));
        }
    }
}
