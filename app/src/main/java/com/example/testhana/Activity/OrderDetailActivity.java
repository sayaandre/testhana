package com.example.testhana.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testhana.Adapter.OrderDetailAdapter;
import com.example.testhana.Helper.Constant;
import com.example.testhana.Helper.JournalHelper;
import com.example.testhana.Helper.RealmHelper;
import com.example.testhana.Helper.Tools;
import com.example.testhana.Models.Model.SalesOrderDetailModel;
import com.example.testhana.Models.Realm.JournalDetail;
import com.example.testhana.Models.Realm.SalesOrder;
import com.example.testhana.Models.Realm.SalesOrderDetail;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.ApiServiceHana;
import com.example.testhana.Rest.Response.GetSalesOrderDetailResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private JournalHelper helper;
    private TextView txtSubtotal, txtVATAmount, txtTotal, txtPayType, btnCustomer, txtPayCash, txtPayOVO, txtPayMobey,
            txtPayUnikas, txtPayOther, txtPayAmount, txtPayAmountCash, txtPayAmountOVO, txtPayAmountMobey, txtPayAmountUnikas,
            txtPayAmountOther, txtChange, labelChange, txtDiscountCashier, txtDiscountTenant, txtDiscountCustom, txtTaxName,
            txtTaxValue, txtRewardRedeem, txtRewardEarnRedeem, txtReceiptDateTimeRight, txtStatusOrder;
    private Button btnIssueReceipt, btnIssueRefund, btnCustomerRight, btnUpdateStatus;
    private TextView txtReceiptNumber, txtReceiptDateTime, txtNoReceipt, txtRefCode, txtCardNo, txtStatus, lblRefCode;
    private LinearLayout llCustomer, llChange, llStatusOrder, llSubtotal, llDiscountCashier, llDiscountTenant, llDiscountCustom, llVAT, llRef, llCard, llRewardRedeem, llRewardEarnRedeem, llNotSplit, llSplit, llPayCash, llPayOVO, llPayMobey, llPayUnikas, llPayOther;
    private RelativeLayout llReceipt;
    private OrderDetailAdapter adapter;
    @BindView(R.id.btnChangeStatus)
    Button btnChangeStatus;
    @BindView(R.id.cardRefund)
    CardView cardRefund;
    @BindView(R.id.txtAmountRefund) TextView txtAmountRefund;
    @BindView(R.id.txtReceiptRefund) TextView txtReceiptRefund;
    @BindView(R.id.txtRefundDateTime) TextView txtRefundDateTime;
    @BindView(R.id.txtReasonRefund) TextView txtReasonRefund;
    @BindView(R.id.txtTableNum) TextView txtTableNum;
    @BindView(R.id.btnScan) Button btnScan;
    @BindView(R.id.txtWaiter) TextView txtWaiter;
    @BindView(R.id.rvPayment)
    RecyclerView rvPayment;
    List<SalesOrderDetailModel> results = null;
    private SalesOrder header;
    long trxNo = 0;
    String orderId = "";
    int mode = 0;
    ApiService apiService;
    boolean isSummary = false, isCustomer = false;
    public boolean issplit = false;
    Bundle bundle;
    ApiServiceHana apiHana;
    RealmHelper realmHelper;

    public OrderDetailActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Order Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiService = ApiClient.getClient().create(ApiService.class);
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        realmHelper = new RealmHelper();
        helper = new JournalHelper(this);
        bundle = getIntent().getExtras();
        results = new ArrayList<SalesOrderDetailModel>();
        trxNo = bundle.getLong("journalId");
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        lblRefCode = findViewById(R.id.lblRefCode);
        txtSubtotal = findViewById(R.id.txtSubtotal);
        txtVATAmount = findViewById(R.id.VATAmount);
        txtTotal = findViewById(R.id.txtTotal);
        txtPayAmount = findViewById(R.id.txtPayAmount);
        txtPayType = findViewById(R.id.txtPayWith);
        txtPayAmountCash = findViewById(R.id.txtPayAmountCash);
        txtPayCash = findViewById(R.id.txtPayCash);
        txtPayAmountOVO = findViewById(R.id.txtPayAmountOVO);
        txtPayOVO = findViewById(R.id.txtPayOVO);
        txtPayAmountMobey = findViewById(R.id.txtPayAmountMobey);
        txtPayMobey = findViewById(R.id.txtPayMobey);
        txtPayAmountUnikas = findViewById(R.id.txtPayAmountUnikas);
        txtPayUnikas = findViewById(R.id.txtPayUnikas);
        txtPayAmountOther = findViewById(R.id.txtPayAmountOther);
        txtPayOther = findViewById(R.id.txtPayOther);
        llNotSplit = findViewById(R.id.llNotSplit);
        llSplit = findViewById(R.id.llSplits);
        llPayCash = findViewById(R.id.llPayCash);
        llPayOVO = findViewById(R.id.llPayOVO);
        llPayMobey = findViewById(R.id.llPayMobey);
        llPayUnikas = findViewById(R.id.llPayUnikas);
        llPayOther = findViewById(R.id.llPayOther);
        llChange = findViewById(R.id.llChange);
        btnIssueReceipt = findViewById(R.id.btnIssueReceipt);
        txtReceiptNumber = findViewById(R.id.txtReceiptNumber);
        txtReceiptDateTime = findViewById(R.id.txtReceiptDateTime);
        txtReceiptDateTimeRight = findViewById(R.id.txtReceiptDateTimeRight);
        btnCustomerRight = findViewById(R.id.txtCustomerRight);
        llReceipt = findViewById(R.id.llReceipt);
        txtNoReceipt = findViewById(R.id.txtNoReceipt);
        txtChange = findViewById(R.id.txtChangeAmount);
        labelChange = findViewById(R.id.txtChange);
        txtDiscountCashier = findViewById(R.id.txtDiscountCashier);
        txtDiscountTenant = findViewById(R.id.txtDiscountTenant);
        txtDiscountCustom = findViewById(R.id.txtDiscountCustom);
        txtRefCode = findViewById(R.id.txtRefCode);
        txtCardNo = findViewById(R.id.txtCardNo);
        txtTaxName = findViewById(R.id.txtTaxName);
        txtTaxValue = findViewById(R.id.txtTaxValue);
        txtRewardRedeem = findViewById(R.id.txtRewardRedeem);
        txtRewardEarnRedeem = findViewById(R.id.txtRewardEarnRedeem);
        txtStatusOrder = findViewById(R.id.txtStatusOrder);
        btnIssueRefund = findViewById(R.id.btnIssueRefund);
        btnCustomer = findViewById(R.id.txtCustomer);
        llStatusOrder = findViewById(R.id.llStatusOrder);
        llSubtotal = findViewById(R.id.llSubTotal);
        llVAT = findViewById(R.id.llVAT);
        llDiscountCashier = findViewById(R.id.llDiscountCashier);
        llDiscountTenant = findViewById(R.id.llDiscountTenant);
        llDiscountCustom = findViewById(R.id.llDiscountCustom);
        llRewardRedeem = findViewById(R.id.llRewardRedeem);
        llRewardEarnRedeem = findViewById(R.id.llRewardEarnRedeem);
        llRef = findViewById(R.id.llRef);
        llCard = findViewById(R.id.llCard);
        llCustomer = findViewById(R.id.llCustomer);
        btnUpdateStatus = findViewById(R.id.btnUpdateStatus);
        btnUpdateStatus.setVisibility(View.GONE);

        btnCustomerRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBusinessPartnerMasterByIdHana(btnCustomerRight.getText().toString());
            }
        });

        btnUpdateStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder kotakBuilder = new AlertDialog.Builder(OrderDetailActivity.this,
                        R.style.AppCompatAlertDialogStyle);
                kotakBuilder.setIcon(android.R.drawable.ic_dialog_alert);
                kotakBuilder.setTitle("Notice");
                kotakBuilder.setMessage("Sisa Limit Untuk Customer Andrew Garfield Telah Melebihi Batas, Silahkan Kurangi Barang Agar Sisa Limit Tidak Melebihi Batas..");
                kotakBuilder.setPositiveButton("Close",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialog = kotakBuilder.create();
                dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.rounded_white));
                dialog.show();
            }
        });

        rvPayment.setLayoutManager(new LinearLayoutManager(this));
        header = realm.where(SalesOrder.class).equalTo("salesOrderId", trxNo).findFirst();
        if(header != null)
        {
            getJournalDetail();
//            setRecyclerView();
        }
//        setRecyclerView();
//        setDisplayView();
    }

    public void getBusinessPartnerMasterByIdHana(final String bpid)
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Search Customer..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.getBusinessPartnerById(bpid, Constant.SELECT_CUSTOMER, cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        res = new String(response.body().bytes());
                        JSONObject item = new JSONObject(res);
                        Intent i = new Intent(OrderDetailActivity.this, CustomerDetailActivity.class);
                        i.putExtra("code", item.getString("CardCode"));
                        i.putExtra("name", item.getString("CardName"));
                        i.putExtra("phone", item.getString("Phone1"));
                        i.putExtra("email", item.getString("EmailAddress"));
                        i.putExtra("address", item.getString("Address"));
                        startActivity(i);
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if(response.code() == 404)
                {
                    Toast.makeText(OrderDetailActivity.this, "Not Found", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(OrderDetailActivity.this, "Try Again", Toast.LENGTH_SHORT).show();
                    realmHelper.doLoginHana();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(OrderDetailActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void setDisplayView()
    {
        txtNoReceipt.setVisibility(View.GONE);
        llReceipt.setVisibility(View.VISIBLE);
        txtReceiptNumber.setText("Order #" + header.getSalesOrderNum());
        txtReceiptDateTimeRight.setText(header.getTrxDate() + "/" + header.getTrxTime());
        if(header.getCustomerId() != null || !header.getCustomerId().equals(""))
        {
            llCustomer.setVisibility(View.VISIBLE);
//            btnCustomerRight.setText(header.getCustomerId());
            btnCustomerRight.setText("Andrew Garfield");
        }
        txtTotal.setText(Tools.convertMoney(this, header.getTotal()));
        JournalDetail journalPayment = realm.where(JournalDetail.class)
                .equalTo("journalId", trxNo)
                .equalTo("journalType", "P")
                .findFirst();
        if(journalPayment != null)
        {
            txtPayType.setText(journalPayment.getItemName());
            txtPayAmount.setText(Tools.convertMoney(this, journalPayment.getItemTotalPrice()));
        }
        JournalDetail journalChange = realm.where(JournalDetail.class)
                .equalTo("journalId", trxNo)
                .equalTo("journalType", "C")
                .findFirst();
        if(journalChange != null)
        {
            llChange.setVisibility(View.VISIBLE);
            txtChange.setText(Tools.convertMoney(this, journalChange.getItemTotalPrice()));
        }
        txtStatusOrder.setText("Open");
    }

    public void getJournalDetail()
    {
        Call<GetSalesOrderDetailResponse> call = apiService.getSalesOrderDetail(trxNo);
        call.enqueue(new Callback<GetSalesOrderDetailResponse>() {
            @Override
            public void onResponse(Call<GetSalesOrderDetailResponse> call, final Response<GetSalesOrderDetailResponse> response) {
                if(response.isSuccessful())
                {
                    if(response.body().getResult().equalsIgnoreCase("ok"))
                    {
                        RealmResults<SalesOrderDetail> salesOrderDetails = realm.where(SalesOrderDetail.class)
                                .equalTo("salesOrderId", trxNo).findAll();
                        realm.beginTransaction();
                        salesOrderDetails.deleteAllFromRealm();
                        realm.commitTransaction();
                        final List<SalesOrderDetail> itemMasters = response.body().getJournalDetails();
                        final Realm mRealm = Realm.getDefaultInstance();
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                setRecyclerView();
                                setDisplayView();
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<GetSalesOrderDetailResponse> call, Throwable t) {
                Toast.makeText(OrderDetailActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void setRecyclerView()
    {
        try{
            results.clear();
            results.addAll(helper.getSalesOrderDetail(trxNo));
        }catch (Exception e){
            e.printStackTrace();
        }
        if (adapter == null) {
            if(results.size() != 0)
            {
                adapter = new OrderDetailAdapter(this, results, new OrderDetailAdapter.OnItemClickListener() {
                    @Override
                    public void onClickDetail(SalesOrderDetailModel item) {
                        Intent i = new Intent(OrderDetailActivity.this, BatchItemListActivity.class);
                        startActivity(i);
                    }
                });
                recyclerView.setAdapter(adapter);
            }
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item_order_detail, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        if(item.getItemId() == R.id.action_customer)
        {
            Intent i = new Intent(this, ChooseCustomerActivity.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
}
