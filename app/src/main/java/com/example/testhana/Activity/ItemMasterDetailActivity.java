package com.example.testhana.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.testhana.Helper.ItemMasterHelper;
import com.example.testhana.Helper.Tools;
import com.example.testhana.Helper.Util;
import com.example.testhana.Models.Realm.ItemGroupMaster;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.Models.Realm.ItemPrice;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.ApiServiceHana;
import com.example.testhana.Rest.ModelClass.ItemMasterClass;
import com.example.testhana.Rest.ModelClass.ItemPriceClass;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;
import io.realm.RealmResults;
import io.realm.Sort;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemMasterDetailActivity extends BaseActivity {

    private String[] itemTypes, itemGroups;
    ItemMasterHelper helper;
    @BindView(R.id.txtItemNo) EditText txtItemNo;
    @BindView(R.id.txtItemDesc) EditText txtItemDesc;
    @BindView(R.id.txtForeignName) EditText txtItemName;
    @BindView(R.id.txtItemPrice) EditText txtItemPrice;
    @BindView(R.id.txtItemStock) EditText txtItemStock;
    @BindView(R.id.spinnerItemType) Spinner spinnerItemType;
    @BindView(R.id.spinnerItemGroup) Spinner spinnerItemGroup;
    @BindView(R.id.imgItem) ImageButton itemImage;
    protected static final int SELECT_FILE = 2;
    private static final int CROP_FROM_CAMERA = 3;
    private static final int SELECT_CATEGORY = 4;
    String img_str = "";
    private Uri mImageCaptureUri;
    ItemMaster itemMaster;
    Bundle bundle;
    ApiService api;
    ApiServiceHana apiHana;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_master_detail);
        ButterKnife.bind(this);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        helper = new ItemMasterHelper(realm);
        api = ApiClient.getClient().create(ApiService.class);
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        itemTypes = helper.getItemType();
        itemGroups = helper.getItemGroup();
        txtItemPrice.addTextChangedListener(new Util.NumberTextWatcher(txtItemPrice, this));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, itemTypes);
        spinnerItemType.setAdapter(adapter);
        spinnerItemType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        final ArrayAdapter<String> adaptergroup = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, itemGroups);
        spinnerItemGroup.setAdapter(adaptergroup);
        spinnerItemGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        itemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        bundle = getIntent().getExtras();
        if(bundle != null && bundle.containsKey("item_id"))
        {
            itemMaster = realm.where(ItemMaster.class).equalTo("itemNo", bundle.getString("item_id")).findFirst();
            if(itemMaster != null)
            {
                setTitle(itemMaster.getItemDesc());
                if(itemMaster.getItemImage() == null || (itemMaster.getItemImage() != null && itemMaster.getItemImage().equals("")))
                {

                }
                else
                {
                    String base64String = itemMaster.getItemImage();
                    String base64Image = base64String;
                    byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    itemImage.setImageBitmap(decodedByte);
                }
                Drawable drawable = DrawableCompat.wrap(ContextCompat.getDrawable(getApplicationContext(), R.drawable.katrol));
                itemImage.setImageDrawable(drawable);
                img_str = itemMaster.getItemImage();
                txtItemNo.setText(itemMaster.getItemNo());
                txtItemName.setText(itemMaster.getForeignName().equals("null") ? "" : itemMaster.getForeignName());
                txtItemDesc.setText(itemMaster.getItemDesc());
                txtItemStock.setText(itemMaster.getStock() + "");
                ItemPrice itemPrice = realm.where(ItemPrice.class)
                        .equalTo("itemId", itemMaster.getItemNo())
                        .sort("priceId", Sort.ASCENDING).findFirst();
                if(itemPrice != null)
                {
                    txtItemPrice.setText(Tools.convertMoney(this, itemPrice.getPrice()));
                }
                spinnerItemType.setSelection(itemMaster.getItemTypeId() - 1);
                ItemGroupMaster itemGroupMaster = realm.where(ItemGroupMaster.class)
                        .equalTo("itemGroupId", itemMaster.getItemGroupId()).findFirst();
                if(itemGroupMaster != null)
                {
                    int spinnerPosition = adaptergroup.getPosition(itemGroupMaster.getItemGroupName());
                    spinnerItemGroup.setSelection(spinnerPosition);
                }
                spinnerItemGroup.setEnabled(false);
                spinnerItemType.setEnabled(false);
                txtItemNo.setEnabled(false);
            }
        }
        else
        {
            setTitle("Add Item");
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Choose From Gallery",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(ItemMasterDetailActivity.this);
        builder.setTitle("Picture");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {

                } else if (items[item].equals("Choose From Gallery")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show().getWindow().setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.rounded_white));
    }

    private void doCrop() {
        CropImage.activity(mImageCaptureUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1,1)
                .setOutputCompressQuality(80)
                .start(this);
    }

    private void createItemMaster()
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Save Item..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        RealmResults<ItemGroupMaster> itemGroupMasters = realm.where(ItemGroupMaster.class).findAll();
        List<ItemPriceClass> itemPriceClasses = new ArrayList<>();
        ItemPriceClass itemPriceClass = new ItemPriceClass(1,
                Double.parseDouble(Tools.processMoney(txtItemPrice.getText().toString())),
                "IDR", "", "", "", "", 1, 1.5, new ArrayList());
        itemPriceClasses.add(itemPriceClass);
        for(int i = 3 ; i < 8 ; i++)
        {
            itemPriceClass = new ItemPriceClass(i,
                    Double.parseDouble(Tools.processMoney(txtItemPrice.getText().toString())),
                    "IDR", "", "", "", "", 1, 1.5, new ArrayList());
            itemPriceClasses.add(itemPriceClass);
        }
        ItemMasterClass itemMasterClass = new ItemMasterClass(txtItemNo.getText().toString(), txtItemDesc.getText().toString(),
                txtItemName.getText().toString(), spinnerItemType.getSelectedItemPosition() == 0 ? "itItems" :
                spinnerItemType.getSelectedItemPosition() == 1 ? "itLabor" : "itTravel",
                itemGroupMasters.get((int)spinnerItemGroup.getSelectedItemId()).getItemGroupId(),
                itemPriceClasses);
        Gson gson = new Gson();
        String json = gson.toJson(itemMasterClass);
        Call<ResponseBody> call = apiHana.createItem(cookie, itemMasterClass);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    realm.beginTransaction();
                    itemMaster = new ItemMaster();
                    itemMaster.setItemId(realm.where(ItemMaster.class).findAll().size() + 1);
                    itemMaster.setItemNo(txtItemNo.getText().toString());
                    itemMaster.setItemDesc(txtItemDesc.getText().toString());
                    itemMaster.setForeignName(txtItemName.getText().toString());
                    itemMaster.setUnitPrice(Integer.parseInt(Tools.processMoney(txtItemPrice.getText().toString())));
                    itemMaster.setItemTypeId(spinnerItemType.getSelectedItemPosition() + 1);
                    itemMaster.setItemGroupId(spinnerItemGroup.getSelectedItemPosition() + 1);
                    itemMaster.setItemImage(img_str);
                    ItemPrice itemPrice = new ItemPrice();
                    itemPrice.setPriceId(realm.where(ItemPrice.class).findAll().size() + 1);
                    itemPrice.setPriceList(1);
                    itemPrice.setPrice(Integer.parseInt(Tools.processMoney(txtItemPrice.getText().toString())));
                    itemPrice.setCurrency("IDR");
                    itemPrice.setItemId(txtItemNo.getText().toString());
                    realm.copyToRealmOrUpdate(itemMaster);
                    realm.copyToRealmOrUpdate(itemPrice);
                    realm.commitTransaction();
                    Intent intent = new Intent();
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
                else
                {
                    Toast.makeText(ItemMasterDetailActivity.this, "Fail To Save Items", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(ItemMasterDetailActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private void updateItemMaster()
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Save Item..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        RealmResults<ItemGroupMaster> itemGroupMasters = realm.where(ItemGroupMaster.class).findAll();
        List<ItemPriceClass> itemPriceClasses = new ArrayList<>();
        ItemPriceClass itemPriceClass = new ItemPriceClass(1,
                Double.parseDouble(Tools.processMoney(txtItemPrice.getText().toString())),
                "AUD", "", "", "", "", 1, 1.5, new ArrayList());
        itemPriceClasses.add(itemPriceClass);
        for(int i = 3 ; i < 8 ; i++)
        {
            itemPriceClass = new ItemPriceClass(i,
                    Double.parseDouble(Tools.processMoney(txtItemPrice.getText().toString())),
                    "AUD", "", "", "", "", 1, 1.5, new ArrayList());
            itemPriceClasses.add(itemPriceClass);
        }
        ItemMasterClass itemMasterClass = new ItemMasterClass(txtItemNo.getText().toString(), txtItemDesc.getText().toString(),
                txtItemName.getText().toString(), spinnerItemType.getSelectedItemPosition() == 0 ? "itItems" :
                spinnerItemType.getSelectedItemPosition() == 1 ? "itLabor" : "itTravel",
                itemGroupMasters.get((int)spinnerItemGroup.getSelectedItemId()).getItemGroupId(),
                itemPriceClasses);
        Call<ResponseBody> call = apiHana.updateItem(txtItemNo.getText().toString(), cookie, itemMasterClass);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    realm.beginTransaction();
                    itemMaster.setItemDesc(txtItemDesc.getText().toString());
                    itemMaster.setForeignName(txtItemName.getText().toString());
                    itemMaster.setUnitPrice(Integer.parseInt(Tools.processMoney(txtItemPrice.getText().toString())));
                    itemMaster.setItemTypeId(spinnerItemType.getSelectedItemPosition() + 1);
                    itemMaster.setItemGroupId(spinnerItemGroup.getSelectedItemPosition() + 1);
                    itemMaster.setItemImage(img_str);
                    ItemPrice itemPrice = new ItemPrice();
                    itemPrice.setPriceId(realm.where(ItemPrice.class).findAll().size() + 1);
                    itemPrice.setPriceList(1);
                    itemPrice.setPrice(Integer.parseInt(Tools.processMoney(txtItemPrice.getText().toString())));
                    itemPrice.setCurrency("IDR");
                    itemPrice.setItemId(txtItemNo.getText().toString());
                    realm.copyToRealmOrUpdate(itemMaster);
                    realm.copyToRealmOrUpdate(itemPrice);
                    realm.commitTransaction();
                    Intent intent = new Intent();
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
                else
                {
                    Toast.makeText(ItemMasterDetailActivity.this, "Fail To Save Items", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(ItemMasterDetailActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult resultCI = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = resultCI.getUri();
                    Bitmap bm = null;
                    File f = null;

                    f = new File(ItemMasterDetailActivity.this.getCacheDir(), "temp");
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    try {
                        bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    if(bm != null) {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bm.compress(Bitmap.CompressFormat.PNG, 85, stream);
                        byte[] image = stream.toByteArray();

                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(f);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        try {
                            fos.write(image);
                            fos.flush();
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Bitmap compressedImageBitmap = null;

                        try {
                            compressedImageBitmap = new Compressor(this).compressToBitmap(f);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                        compressedImageBitmap.compress(Bitmap.CompressFormat.PNG, 90, stream1);
                        byte[] image1 = stream1.toByteArray();

                        img_str = Base64.encodeToString(image1, Base64.NO_WRAP);
                        itemImage.setImageBitmap(compressedImageBitmap);
                    }

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = resultCI.getError();
                }
            }

            if (requestCode == SELECT_FILE) {
                mImageCaptureUri = data.getData();
                //processPhoto(mImageCaptureUri);
                doCrop();
            } else if (requestCode == CROP_FROM_CAMERA) {
                Bundle extras = data.getExtras();
                Bitmap bm = null;
                File f = null;
                if (extras != null) {
                    f = new File(ItemMasterDetailActivity.this.getCacheDir(), "temp");
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    bm = extras.getParcelable("data");
                } else {
                    f = new File(ItemMasterDetailActivity.this.getCacheDir(), "temp");
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Uri extraUri = data.getData();
                    if(extraUri != null) {
                        try {
                            bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), extraUri);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                }

                if(bm != null) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.PNG, 85, stream);
                    byte[] image = stream.toByteArray();

                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(f);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    try {
                        fos.write(image);
                        fos.flush();
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Bitmap compressedImageBitmap = null;

                    try {
                        compressedImageBitmap = new Compressor(this).compressToBitmap(f);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                    compressedImageBitmap.compress(Bitmap.CompressFormat.PNG, 90, stream1);
                    byte[] image1 = stream1.toByteArray();

                    img_str = Base64.encodeToString(image1, Base64.NO_WRAP);
                    itemImage.setImageBitmap(compressedImageBitmap);
                }
            }
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_item_master_detail, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent();
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
//        else if(item.getItemId() == R.id.action_add)
//        {
//            if(itemMaster != null)
//            {
//                updateItemMaster();
//            }
//            else
//            {
//                createItemMaster();
//            }
//        }

        return super.onOptionsItemSelected(item);
    }

    private boolean validate()
    {
        boolean valid = true;
        String no = txtItemNo.getText().toString().trim();
        String name = txtItemName.getText().toString().trim();
        String price = txtItemPrice.getText().toString().trim();
        if(no.isEmpty())
        {
            txtItemNo.setError("Fill Item No");
            valid = false;
        }
        else
        {
            txtItemNo.setError(null);
        }
        if(name.isEmpty())
        {
            txtItemName.setError("Fill Item Name");
            valid = false;
        }
        else
        {
            txtItemName.setError(null);
        }
        if(price.isEmpty())
        {
            txtItemPrice.setError("Fill Item Price");
            valid = false;
        }
        else
        {
            txtItemPrice.setError(null);
        }
        return valid;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
