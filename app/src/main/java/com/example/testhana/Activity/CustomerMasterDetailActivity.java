package com.example.testhana.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.testhana.Helper.Constant;
import com.example.testhana.Helper.CustomerMasterHelper;
import com.example.testhana.Models.Realm.CustomerCurrencyMaster;
import com.example.testhana.Models.Realm.CustomerGroupMaster;
import com.example.testhana.Models.Realm.CustomerMaster;
import com.example.testhana.Models.Realm.ItemGroupMaster;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.ApiServiceHana;
import com.example.testhana.Rest.Response.ManageCustomerResponse;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;
import io.realm.RealmResults;
import io.realm.Sort;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerMasterDetailActivity extends BaseActivity {

    private String[] customerRole, customerGroup, customerCurrency;
    CustomerMasterHelper helper;
    @BindView(R.id.txtCustomerCode) EditText txtCustomerCode;
    @BindView(R.id.txtCustomerName) EditText txtCustomerName;
    @BindView(R.id.txtCustomerPhone) EditText txtCustomerPhone;
    @BindView(R.id.txtCustomerEmail) EditText txtCustomerEmail;
    @BindView(R.id.txtCustomerAddress) EditText txtCustomerAddress;
    @BindView(R.id.spinnerCustomerRole) Spinner spinnerCustomerRole;
    @BindView(R.id.spinnerCustomerGroup) Spinner spinnerCustomerGroup;
    @BindView(R.id.spinnerCustomerCurrency) Spinner spinnerCustomerCurrency;
    @BindView(R.id.imgCustomer) ImageButton imgCustomer;
    protected static final int SELECT_FILE = 2;
    private static final int CROP_FROM_CAMERA = 3;
    private static final int SELECT_CATEGORY = 4;
    String img_str = "";
    private Uri mImageCaptureUri;
    CustomerMaster customerMaster;
    Bundle bundle;
    ApiService api;
    ApiServiceHana apiHana;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_master_detail);
        ButterKnife.bind(this);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        helper = new CustomerMasterHelper(realm);
        api = ApiClient.getClient().create(ApiService.class);
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        customerRole = helper.getCustomerRole();
        customerGroup = helper.getCustomerGroup();
        customerCurrency = helper.getCustomerCurrency();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, customerRole);
        spinnerCustomerRole.setAdapter(adapter);
        spinnerCustomerRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        final ArrayAdapter<String> adaptergroup = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, customerGroup);
        spinnerCustomerGroup.setAdapter(adaptergroup);
        spinnerCustomerGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        final ArrayAdapter<String> adaptercurr = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, customerCurrency);
        spinnerCustomerCurrency.setAdapter(adaptercurr);
        spinnerCustomerCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        imgCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        bundle = getIntent().getExtras();
        if(bundle != null && bundle.containsKey("customer_id"))
        {
            customerMaster = realm.where(CustomerMaster.class).equalTo("customerCode", bundle.getString("customer_id")).findFirst();
            if(customerMaster != null)
            {
                setTitle(customerMaster.getCustomerName());
                if(customerMaster.getCustomerImage() == null ||
                        (customerMaster.getCustomerImage() != null && customerMaster.getCustomerImage().equals("")))
                {

                }
                else
                {
                    String base64String = customerMaster.getCustomerImage();
                    String base64Image = base64String;
                    byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    imgCustomer.setImageBitmap(decodedByte);
                }
                img_str = customerMaster.getCustomerImage();
                txtCustomerCode.setText(customerMaster.getCustomerCode());
                txtCustomerName.setText(customerMaster.getCustomerName());
                txtCustomerPhone.setText(customerMaster.getCustomerPhone());
                txtCustomerEmail.setText(customerMaster.getCustomerEmail());
                txtCustomerAddress.setText(customerMaster.getCustomerAddress());
                spinnerCustomerRole.setSelection(customerMaster.getCustomerRoleId() - 1);
                CustomerGroupMaster customerGroupMaster = realm.where(CustomerGroupMaster.class)
                        .equalTo("customerGroupId", customerMaster.getCustomerGroupId()).findFirst();
                if(customerGroupMaster != null)
                {
                    int spinnerPosition = adaptergroup.getPosition(customerGroupMaster.getCustomerGroupName());
                    spinnerCustomerGroup.setSelection(spinnerPosition);
                }
                CustomerCurrencyMaster customerCurrencyMaster = realm.where(CustomerCurrencyMaster.class)
                        .equalTo("Code", customerMaster.getCustomerCurrencyId()).findFirst();
                if(customerCurrencyMaster != null)
                {
                    int spinnerPosition = adaptercurr.getPosition(customerCurrencyMaster.getName());
                    spinnerCustomerCurrency.setSelection(spinnerPosition);
                }
//                spinnerCustomerCurrency.setSelection(customerMaster.getCustomerCurrencyId() - 1);
                spinnerCustomerRole.setEnabled(false);
                txtCustomerCode.setEnabled(false);
            }
        }
        else
        {
            setTitle("Add Customer");
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Choose From Gallery",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(CustomerMasterDetailActivity.this);
        builder.setTitle("Picture");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {

                } else if (items[item].equals("Choose From Gallery")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show().getWindow().setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.rounded_white));
    }

    private void doCrop() {
        CropImage.activity(mImageCaptureUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1,1)
                .setOutputCompressQuality(80)
                .start(this);
    }

    private void createBusinessPartner()
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Save Bussiness Partner..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        RealmResults<CustomerGroupMaster> itemGroupMasters = realm.where(CustomerGroupMaster.class).findAll();
        RealmResults<CustomerCurrencyMaster> customerCurrencyMasters = realm.where(CustomerCurrencyMaster.class).findAll();
        Map<String, Object> map = new HashMap<>();
        map.put("CardCode", txtCustomerCode.getText().toString());
        map.put("CardName", txtCustomerName.getText().toString());
        map.put("CardType", spinnerCustomerRole.getSelectedItem().toString().charAt(0));
        map.put("Phone1", txtCustomerPhone.getText().toString());
        map.put("EmailAddress", txtCustomerEmail.getText().toString());
        map.put("Address", txtCustomerAddress.getText().toString());
        map.put("GroupCode", itemGroupMasters.get((int)spinnerCustomerGroup.getSelectedItemId()).getCustomerGroupId());
        map.put("Currency", customerCurrencyMasters.get((int)spinnerCustomerCurrency.getSelectedItemId()).getCode());
        Call<ResponseBody> call = apiHana.createBusinessPartner(cookie, map);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    realm.beginTransaction();
                    int lastid = realm.where(CustomerMaster.class).findAll().size() == 0 ? 1 : realm.where(CustomerMaster.class).sort("customerId", Sort.DESCENDING).findFirst().getCustomerId() + 1;
                    customerMaster = new CustomerMaster();
                    customerMaster.setCustomerId(lastid);
                    customerMaster.setCustomerCode(txtCustomerCode.getText().toString());
                    customerMaster.setCustomerName(txtCustomerName.getText().toString());
                    customerMaster.setCustomerPhone(txtCustomerPhone.getText().toString());
                    customerMaster.setCustomerEmail(txtCustomerEmail.getText().toString());
                    customerMaster.setCustomerAddress(txtCustomerAddress.getText().toString());
                    customerMaster.setCustomerRoleId(spinnerCustomerRole.getSelectedItemPosition() + 1);
                    customerMaster.setCustomerGroupId(spinnerCustomerGroup.getSelectedItemPosition() + 1);
//                        customerMaster.setCustomerCurrencyId(spinnerCustomerCurrency.getSelectedItemPosition() + 1);
                    customerMaster.setCustomerImage(img_str);
                    realm.copyToRealmOrUpdate(customerMaster);
                    realm.commitTransaction();
                    Intent intent = new Intent();
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
                else
                {
                    Toast.makeText(CustomerMasterDetailActivity.this, "Code Does Exist", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(CustomerMasterDetailActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private void updateBusinessPartner()
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Save Bussiness Partner..");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        final RealmResults<CustomerGroupMaster> itemGroupMasters = realm.where(CustomerGroupMaster.class).findAll();
        final RealmResults<CustomerCurrencyMaster> customerCurrencyMasters = realm.where(CustomerCurrencyMaster.class).findAll();
        Map<String, Object> map = new HashMap<>();
        map.put("CardName", txtCustomerName.getText().toString());
        map.put("CardType", spinnerCustomerRole.getSelectedItem().toString().charAt(0));
        map.put("Phone1", txtCustomerPhone.getText().toString());
        map.put("EmailAddress", txtCustomerEmail.getText().toString());
        map.put("Address", txtCustomerAddress.getText().toString());
        map.put("GroupCode", itemGroupMasters.get((int)spinnerCustomerGroup.getSelectedItemId()).getCustomerGroupId());
        map.put("Currency", customerCurrencyMasters.get((int)spinnerCustomerCurrency.getSelectedItemId()).getCode());
        Call<ResponseBody> call = apiHana.updateBusinessPartner(txtCustomerCode.getText().toString(), cookie, map);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissProgressDialog();
                if(response.isSuccessful())
                {
                    realm.beginTransaction();
                    customerMaster.setCustomerName(txtCustomerName.getText().toString());
                    customerMaster.setCustomerPhone(txtCustomerPhone.getText().toString());
                    customerMaster.setCustomerEmail(txtCustomerEmail.getText().toString());
                    customerMaster.setCustomerAddress(txtCustomerAddress.getText().toString());
                    customerMaster.setCustomerRoleId(spinnerCustomerRole.getSelectedItemPosition() + 1);
                    customerMaster.setCustomerGroupId(itemGroupMasters.get((int)spinnerCustomerGroup.getSelectedItemId()).getCustomerGroupId());
                    customerMaster.setCustomerCurrencyId(customerCurrencyMasters.get((int)spinnerCustomerCurrency.getSelectedItemId()).getCode());
                    customerMaster.setCustomerImage(img_str);
                    realm.copyToRealmOrUpdate(customerMaster);
                    realm.commitTransaction();
                    Intent intent = new Intent();
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(CustomerMasterDetailActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult resultCI = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = resultCI.getUri();
                    Bitmap bm = null;
                    File f = null;

                    f = new File(CustomerMasterDetailActivity.this.getCacheDir(), "temp");
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    try {
                        bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    if(bm != null) {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bm.compress(Bitmap.CompressFormat.PNG, 85, stream);
                        byte[] image = stream.toByteArray();

                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(f);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        try {
                            fos.write(image);
                            fos.flush();
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Bitmap compressedImageBitmap = null;

                        try {
                            compressedImageBitmap = new Compressor(this).compressToBitmap(f);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                        compressedImageBitmap.compress(Bitmap.CompressFormat.PNG, 90, stream1);
                        byte[] image1 = stream1.toByteArray();

                        img_str = Base64.encodeToString(image1, Base64.NO_WRAP);
                        imgCustomer.setImageBitmap(compressedImageBitmap);
                    }

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = resultCI.getError();
                }
            }

            if (requestCode == SELECT_FILE) {
                mImageCaptureUri = data.getData();
                //processPhoto(mImageCaptureUri);
                doCrop();
            } else if (requestCode == CROP_FROM_CAMERA) {
                Bundle extras = data.getExtras();
                Bitmap bm = null;
                File f = null;
                if (extras != null) {
                    f = new File(CustomerMasterDetailActivity.this.getCacheDir(), "temp");
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    bm = extras.getParcelable("data");
                } else {
                    f = new File(CustomerMasterDetailActivity.this.getCacheDir(), "temp");
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Uri extraUri = data.getData();
                    if(extraUri != null) {
                        try {
                            bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), extraUri);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                }

                if(bm != null) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.PNG, 85, stream);
                    byte[] image = stream.toByteArray();

                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(f);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    try {
                        fos.write(image);
                        fos.flush();
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Bitmap compressedImageBitmap = null;

                    try {
                        compressedImageBitmap = new Compressor(this).compressToBitmap(f);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                    compressedImageBitmap.compress(Bitmap.CompressFormat.PNG, 90, stream1);
                    byte[] image1 = stream1.toByteArray();

                    img_str = Base64.encodeToString(image1, Base64.NO_WRAP);
                    imgCustomer.setImageBitmap(compressedImageBitmap);
                }
            }
        }
    }

    private boolean validate()
    {
        boolean valid = true;
        String code = txtCustomerCode.getText().toString().trim();
        String phone = txtCustomerPhone.getText().toString().trim();
        String name = txtCustomerName.getText().toString().trim();
        if(code.isEmpty())
        {
            txtCustomerCode.setError("Fill Customer Code");
            valid = false;
        }
        else
        {
            txtCustomerCode.setError(null);
        }
        Pattern sPattern = Pattern.compile("^0[0-9]{6,20}$");
        if(phone.isEmpty())
        {
            txtCustomerPhone.setError("Fill Customer Phone");
            valid = false;
        }
//        else if (!sPattern.matcher(phone).matches()) {
//            txtCustomerPhone.setError("Phone Number must begin with \\'08\\' and 8 - 13 Character");
//            valid = false;
//        }
        else
        {
            txtCustomerPhone.setError(null);
        }
        if(name.isEmpty())
        {
            txtCustomerName.setError("Fill Customer Name");
            valid = false;
        }
        else
        {
            txtCustomerName.setError(null);
        }
        return valid;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item_master_detail, menu);

        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent();
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
        else if(item.getItemId() == R.id.action_add)
        {
            if(validate())
            {
                if(customerMaster != null)
                {
                    updateBusinessPartner();
                }
                else
                {
                    createBusinessPartner();
                }
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
