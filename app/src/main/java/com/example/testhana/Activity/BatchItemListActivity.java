package com.example.testhana.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;

import com.example.testhana.Adapter.BatchItemAdapter;
import com.example.testhana.Models.Model.BatchItemMasterModel;
import com.example.testhana.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BatchItemListActivity extends BaseActivity {

    @BindView(R.id.rvPaymentMaster) RecyclerView rvBatch;

    private BatchItemAdapter adapter;
    List<BatchItemMasterModel> batchItemMasterModels;
    Bundle bundle;
    boolean tabletSize = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        int screenWidth = 0;
        int screenHeight = 0;
        setContentView(R.layout.activity_batch_item_list);
        ButterKnife.bind(this);
        if(tabletSize) {
            screenWidth = (int) (metrics.widthPixels * 0.6);
            screenHeight = (int) (metrics.heightPixels * 0.5);
        } else {
            screenWidth = (int) (metrics.widthPixels * 0.7);
            screenHeight = (int) (metrics.heightPixels * 0.5);
        }
        getWindow().setLayout(screenWidth, screenHeight);
        ButterKnife.bind(this);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        rvBatch.setLayoutManager(mLayoutManager);
        rvBatch.addItemDecoration(new GridSpacingItemDecoration(1, 0, true));
        rvBatch.setItemAnimator(new DefaultItemAnimator());
        batchItemMasterModels = new ArrayList<>();
//        bundle = getIntent().getExtras();
//        if(bundle.containsKey("item_id"))
//        {
//            RealmResults<BatchItemMaster> batchItemMasters = realm.where(BatchItemMaster.class)
//                    .equalTo("itemcode", bundle.getString("item_id")).findAll();
//            for(int i = 0 ; i < batchItemMasters.size() ; i++)
//            {
//                BatchItemMasterModel batchItemMasterModel = new BatchItemMasterModel(batchItemMasters.get(i).getLogentry(),
//                        batchItemMasters.get(i).getItemcode(), batchItemMasters.get(i).getItemname(),
//                        batchItemMasters.get(i).getQty(), batchItemMasters.get(i).getQtyout(),
//                        batchItemMasters.get(i).getBatchno());
//                batchItemMasterModels.add(batchItemMasterModel);
//            }
//            setAdapter();
//        }
        BatchItemMasterModel batchItemMasterModel = new BatchItemMasterModel(1,
                "87 meter", "A001",
                1, 0,
                "04072019");
        batchItemMasterModels.add(batchItemMasterModel);
        batchItemMasterModel = new BatchItemMasterModel(2,
                "105 meter", "A001",
                1, 0,
                "03072019");
        batchItemMasterModels.add(batchItemMasterModel);
        batchItemMasterModel = new BatchItemMasterModel(3,
                "99 meter", "A001",
                1, 0,
                "02072019");
        batchItemMasterModels.add(batchItemMasterModel);
        setAdapter();
    }

    private void setAdapter()
    {
        if(adapter == null) {
            adapter = new BatchItemAdapter(this, batchItemMasterModels, new BatchItemAdapter.OnItemClickListener() {
                @Override
                public void onClick(BatchItemMasterModel item, int position) {
                    Intent intent = new Intent();
                    intent.putExtra("batch_no", item.getBatchno());
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }

                @Override
                public void onDelete(final BatchItemMasterModel item, int position) {

                }
            });
            rvBatch.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
}
