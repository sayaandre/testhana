package com.example.testhana.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Models.Realm.CustomerCurrencyMaster;
import com.example.testhana.Models.Realm.CustomerGroupMaster;
import com.example.testhana.Models.Realm.CustomerRoleMaster;
import com.example.testhana.Models.Realm.ItemGroupMaster;
import com.example.testhana.Models.Realm.ItemTypeMaster;
import com.example.testhana.Models.Realm.LocationCheckIn;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

import io.realm.Realm;

public class BaseActivity extends AppCompatActivity {

    Realm realm;
    SessionManagement session;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = CoreApplication.getInstance().getSession();
        realm = Realm.getDefaultInstance();
        insertMasterDataItemType();
        insertMasterDataCustomerRole();
//        insertLocation();
    }

    public void dismissProgressDialog() {
        if(!isFinishing() && progressDialog != null && progressDialog.isShowing()) {
            if(progressDialog.isShowing())
                progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        try {
            if(realm != null && !realm.isClosed())
                realm.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        dismissProgressDialog();
        super.onDestroy();
    }

    public void insertMasterDataItemType()
    {
        realm.beginTransaction();
        ItemTypeMaster itemTypeMaster = new ItemTypeMaster();
        itemTypeMaster.setItemTypeId(1);
        itemTypeMaster.setItemTypeName("Items");
        realm.copyToRealmOrUpdate(itemTypeMaster);
        itemTypeMaster = new ItemTypeMaster();
        itemTypeMaster.setItemTypeId(2);
        itemTypeMaster.setItemTypeName("Labor");
        realm.copyToRealmOrUpdate(itemTypeMaster);
        itemTypeMaster = new ItemTypeMaster();
        itemTypeMaster.setItemTypeId(3);
        itemTypeMaster.setItemTypeName("Travel");
        realm.copyToRealmOrUpdate(itemTypeMaster);
        realm.commitTransaction();
    }

    public void insertMasterDataCustomerRole()
    {
        realm.beginTransaction();
        CustomerRoleMaster customerMaster = new CustomerRoleMaster();
        customerMaster.setRoleId(1);
        customerMaster.setRoleName("Customer");
        realm.copyToRealmOrUpdate(customerMaster);
        customerMaster = new CustomerRoleMaster();
        customerMaster.setRoleId(2);
        customerMaster.setRoleName("Vendor");
        realm.copyToRealmOrUpdate(customerMaster);
        customerMaster = new CustomerRoleMaster();
        customerMaster.setRoleId(3);
        customerMaster.setRoleName("Lead");
        realm.copyToRealmOrUpdate(customerMaster);
        realm.commitTransaction();
    }

    public void insertLocation()
    {
        int size = realm.where(LocationCheckIn.class).findAll().size();
        if(size == 0)
        {
            realm.beginTransaction();
            LocationCheckIn locationCheckIn = new LocationCheckIn();
            locationCheckIn.setLocationId(1);
            locationCheckIn.setLocationName("PT Visi Inti Teknologi");
            locationCheckIn.setLocationCity("Jakarta Barat");
            locationCheckIn.setLocationAddress("Jl. Puri Kembangan Raya No.10E, RT.1/RW.8, South Kembangan, Kembangan, West Jakarta City, Jakarta 11610");
            locationCheckIn.setLatitude(-6.183951);
            locationCheckIn.setLongitude(106.753214);
            realm.copyToRealmOrUpdate(locationCheckIn);
            locationCheckIn = new LocationCheckIn();
            locationCheckIn.setLocationId(2);
            locationCheckIn.setLocationName("Metro TV");
            locationCheckIn.setLocationCity("Jakarta Barat");
            locationCheckIn.setLocationAddress("Jl. Pilar Mas Raya Kav. A-D Kedoya Selatan, Kebon Jeruk, Jakarta, Indonesia");
            locationCheckIn.setLatitude(-6.186889);
            locationCheckIn.setLongitude(106.758665);
            realm.copyToRealmOrUpdate(locationCheckIn);
            locationCheckIn = new LocationCheckIn();
            locationCheckIn.setLocationId(3);
            locationCheckIn.setLocationName("Fave Hotel");
            locationCheckIn.setLocationCity("Jakarta Barat");
            locationCheckIn.setLocationAddress("Jl. Kembang Abadi Utama Blok A1 No.1, RT.1/RW.8, Kembangan Sel., Kembangan, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11610");
            locationCheckIn.setLatitude(-6.182992);
            locationCheckIn.setLongitude(106.751418);
            realm.copyToRealmOrUpdate(locationCheckIn);
            locationCheckIn = new LocationCheckIn();
            locationCheckIn.setLocationId(4);
            locationCheckIn.setLocationName("Veranda Residence");
            locationCheckIn.setLocationCity("Jakarta Barat");
            locationCheckIn.setLocationAddress("Jl. Pesanggrahan No.28, RT.2/RW.7, Kembangan Sel., Kembangan, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11610");
            locationCheckIn.setLatitude(-6.189223);
            locationCheckIn.setLongitude(106.755972);
            realm.copyToRealmOrUpdate(locationCheckIn);
            locationCheckIn = new LocationCheckIn();
            locationCheckIn.setLocationId(5);
            locationCheckIn.setLocationName("Mutiara Kedoya");
            locationCheckIn.setLocationCity("Jakarta Barat");
            locationCheckIn.setLocationAddress("Jalan Puri Kembangan No.41, RT.11/RW.5, Kembangan Selatan, Kembangan, RT.11/RW.5, Kembangan Sel., Kembangan, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11610");
            locationCheckIn.setLatitude(-6.183521);
            locationCheckIn.setLongitude(106.755920);
            realm.copyToRealmOrUpdate(locationCheckIn);
            realm.commitTransaction();
        }
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }

            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude() );

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }
}
