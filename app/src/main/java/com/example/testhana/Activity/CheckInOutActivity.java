package com.example.testhana.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.testhana.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CheckInOutActivity extends BaseActivity {

    @BindView(R.id.btnSubmit) Button btnSubmit;
    boolean tabletSize = false;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        DisplayMetrics metrics = getResources().getDisplayMetrics();
//        tabletSize = getResources().getBoolean(R.bool.isTablet);
//        int screenWidth = 0;
//        int screenHeight = 0;
        setContentView(R.layout.activity_check_in_out);
//        if(tabletSize) {
//            screenWidth = (int) (metrics.widthPixels * 0.6);
//            screenHeight = (int) (metrics.heightPixels * 0.5);
//        } else {
//            screenWidth = (int) (metrics.widthPixels * 0.9);
//            screenHeight = (int) (metrics.heightPixels * 0.9);
//        }
//        getWindow().setLayout(screenWidth, screenHeight);
        ButterKnife.bind(this);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        setTitle("Submit Absence");
        bundle = getIntent().getExtras();
        if(bundle != null && bundle.containsKey("id"))
        {
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.putExtra("latitude", bundle.getInt("latitude"));
                    intent.putExtra("longitude", bundle.getInt("longitude"));
                    intent.putExtra("id", bundle.getInt("id"));
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent();
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
