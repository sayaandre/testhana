package com.example.testhana.Activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.testhana.Adapter.BatchItemAdapter;
import com.example.testhana.Helper.CartHelper;
import com.example.testhana.Helper.Tools;
import com.example.testhana.Models.Model.BatchItemMasterModel;
import com.example.testhana.Models.Realm.BatchItemMaster;
import com.example.testhana.Models.Realm.Cart;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.Models.Realm.ItemPrice;
import com.example.testhana.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;

public class SalesInvoiceProductActivity extends BaseActivity {

    @BindView(R.id.imgItem)
    ImageView imgItem;
    @BindView(R.id.txtPCName)
    TextView txtPCName;
    @BindView(R.id.txtPCPrice) TextView txtPCPrice;
    @BindView(R.id.llPCDesc)
    LinearLayout llPCDesc;
    @BindView(R.id.txtPCDescTitle) TextView txtPCDescTitle;
    @BindView(R.id.txtPCDesc) TextView txtPCDesc;
    @BindView(R.id.llPCSupplier) LinearLayout llPCSupplier;
    @BindView(R.id.txtPCSupplierTitle) TextView txtPCSupplierTitle;
    @BindView(R.id.txtPCSupplier) TextView txtPCSupplier;
    @BindView(R.id.btnMinus)
    Button btnMinus;
    @BindView(R.id.editQty)
    EditText txtQty;
    @BindView(R.id.btnPlus) Button btnPlus;
    @BindView(R.id.editDescription) EditText txtDesc;
    @BindView(R.id.llBatchItem) LinearLayout llBatchItem;
    @BindView(R.id.btnBatchItem) Button btnBatchItem;
    @BindView(R.id.txtTotal) TextView txtTotal;
    @BindView(R.id.txtPCLastUpdate) TextView txtPCLastUpdate;
    @BindView(R.id.btnBuy) Button btnBuy;
    Bundle bundle;
    ItemMaster itemMaster;
    int quantity = 1;
    double price = 0.0;
    String currency = "";
    CartHelper helper;
    private BatchItemAdapter adapter;
    List<BatchItemMasterModel> batchItemMasterModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_invoice_product);
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        getSupportActionBar().setTitle("Add Item Order");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        batchItemMasterModels = new ArrayList<>();
        helper = new CartHelper(this);
        bundle = getIntent().getExtras();
        if(bundle.containsKey("item_id"))
        {
            itemMaster = realm.where(ItemMaster.class).equalTo("itemNo", bundle.getString("item_id")).findFirst();
            if(itemMaster != null)
            {
                if(itemMaster.getItemImage() == null || (itemMaster.getItemImage() != null && itemMaster.getItemImage().equals("")))
                {
                    imgItem.setVisibility(View.GONE);
                }
                else
                {
                    String base64String = itemMaster.getItemImage();
                    String base64Image = base64String;
                    byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    imgItem.setImageBitmap(decodedByte);
                }
                Cart cart = realm.where(Cart.class).equalTo("itemId", bundle.getString("item_id")).findFirst();
                if(cart != null)
                {
                    quantity = cart.getItemQty();
                    txtQty.setText(String.valueOf(quantity));
                    txtDesc.setText(cart.getRemarks());
                }
                txtPCDesc.setText(itemMaster.getForeignName().equalsIgnoreCase("null") ? "" : itemMaster.getForeignName());
                txtPCName.setText(itemMaster.getItemDesc());
                RealmResults<ItemPrice> itemPrices = realm.where(ItemPrice.class).equalTo("itemId", itemMaster.getItemNo()).findAll();
                if(itemPrices.size() >= 1)
                {
                    txtPCPrice.setText(Tools.convertMoney(this, itemPrices.first().getPrice()) + " " + itemPrices.first().getCurrency());
                    txtTotal.setText(Tools.convertMoney(this, itemPrices.first().getPrice() * Double.parseDouble(txtQty.getText().toString())) + " " + itemPrices.first().getCurrency());
                    price = itemPrices.first().getPrice();
                    currency = itemPrices.first().getCurrency();
                }
                RealmResults<BatchItemMaster> batchItemMasters = realm.where(BatchItemMaster.class)
                        .equalTo("itemcode", itemMaster.getItemNo()).findAll();
                if(batchItemMasters.size() == 0)
                {
                    llBatchItem.setVisibility(View.GONE);
                }
                else
                {
                    llBatchItem.setVisibility(View.VISIBLE);
                }
                btnBatchItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(SalesInvoiceProductActivity.this, BatchItemListActivity.class);
                        i.putExtra("item_id", bundle.getString("item_id"));
                        startActivityForResult(i, 100);
                    }
                });
            }
            txtQty.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    int pos = txtQty.getText().length();
                    txtQty.setSelection(pos);
                    if(txtQty.getText().toString().length() >= 7)
                    {
                        txtQty.setText("999999");
                        Double total = Double.parseDouble(txtQty.getText().toString().equals("") ? "1" : txtQty.getText().toString()) * price;
                        txtTotal.setText(Tools.convertMoney(SalesInvoiceProductActivity.this, total) + " " + currency);
                    }
                    else
                    {
                        if(txtQty.getText().toString().equals("0"))
                        {
                            txtQty.setText("1");
                            Double total = Double.parseDouble(txtQty.getText().toString().equals("") ? "1" : txtQty.getText().toString()) * price;
                            txtTotal.setText(Tools.convertMoney(SalesInvoiceProductActivity.this, total) + " " + currency);
                        }
                        else
                        {
                            Double total = Double.parseDouble(txtQty.getText().toString().equals("") ? "1" : txtQty.getText().toString()) * price;
                            txtTotal.setText(Tools.convertMoney(SalesInvoiceProductActivity.this, total) + " " + currency);
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            btnPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Tools.dismissKeyboard(SalesInvoiceProductActivity.this);
                    if(!txtQty.getText().toString().isEmpty()) {
                        quantity = Integer.valueOf(txtQty.getText().toString());
                        quantity += 1;
                        if(String.valueOf(quantity).length() >= 7) {
                            quantity = 999999;
                        }
                    } else {
                        quantity = 1;
                    }
                    txtQty.setText(String.valueOf(quantity));
                }
            });
            btnMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Tools.dismissKeyboard(SalesInvoiceProductActivity.this);
                    if(!txtQty.getText().toString().isEmpty()) {
                        quantity = Integer.valueOf(txtQty.getText().toString());
                        if (quantity > 1)
                            quantity -= 1;
                    } else {
                        quantity = 1;
                    }
                    txtQty.setText(String.valueOf(quantity));
                }
            });
            btnBuy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(bundle.getInt("mode") == 0)
                    {
                        AlertDialog.Builder kotakBuilder = new AlertDialog.Builder(SalesInvoiceProductActivity.this,
                                R.style.AppCompatAlertDialogStyle);
                        kotakBuilder.setIcon(android.R.drawable.ic_dialog_alert);
                        kotakBuilder.setTitle("Notice");
                        kotakBuilder.setMessage("Do You Want to Add To Cart?");
                        kotakBuilder.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        helper.addCartFromHome(itemMaster.getItemNo(), itemMaster.getItemDesc(), quantity, txtDesc.getText().toString(),
                                                Double.parseDouble(Tools.processMoney(txtTotal.getText().toString().split(" ")[0])),
                                                price, 0);
                                        Intent intent = new Intent();
                                        setResult(Activity.RESULT_OK, intent);
                                        finish();
                                    }
                                });
                        kotakBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = kotakBuilder.create();
                        dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.rounded_white));
                        dialog.show();
                    }
                    else
                    {
                        helper.addCart(itemMaster.getItemNo(), itemMaster.getItemDesc(), quantity, txtDesc.getText().toString(),
                                Double.parseDouble(Tools.processMoney(txtTotal.getText().toString().split(" ")[0])),
                                itemMaster.getUnitPrice());
                        Intent intent = new Intent();
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK)
        {
            if(requestCode == 100)
            {
                btnBatchItem.setText(data.getStringExtra("batch_no"));
            }
        }
    }
}

