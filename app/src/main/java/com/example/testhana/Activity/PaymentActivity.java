package com.example.testhana.Activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;


import com.example.testhana.Fragment.CartViewOnlyFragment;
import com.example.testhana.Fragment.PaymentFragment;
import com.example.testhana.R;

import butterknife.ButterKnife;

public class PaymentActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);
        setTitle("Payment Transaction");
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentManager fragmentManager1 = getSupportFragmentManager();

        if(savedInstanceState == null) {
            boolean isTablet = getResources().getBoolean(R.bool.isTablet);
            if(isTablet) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
            if (findViewById(R.id.fragment_container_right) != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                PaymentFragment payMDSFragment = new PaymentFragment();

                FragmentTransaction fragmentTransaction1 = fragmentManager1.beginTransaction();
                CartViewOnlyFragment cartFragment = new CartViewOnlyFragment();

                Bundle bundle = getIntent().getExtras();

                payMDSFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.fragment_container_right, payMDSFragment, "payment");

                fragmentTransaction.commit();

                if (getResources().getBoolean(R.bool.isTablet)) {
                    fragmentTransaction1.replace(R.id.fragment_container_left, cartFragment, "cart");
                    fragmentTransaction1.commit();
                }

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
