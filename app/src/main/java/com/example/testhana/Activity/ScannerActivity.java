package com.example.testhana.Activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testhana.R;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CompoundBarcodeView;

import java.util.List;

public class ScannerActivity extends BaseActivity {

    private CompoundBarcodeView barcodeView;
    private TextView tvResult;
    ImageButton ibManageFlashOn;
    ImageButton ibManageFlashOff;
    private boolean isLightOn = false;

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            tvResult = findViewById(R.id.tvResult);
            if (result.getText() != null) {
                barcodeView.setStatusText(result.getText());
                Intent i = getIntent();
                i.putExtra("content", result.getText());
                setResult(RESULT_OK, i);
                finish();
            } else {
                tvResult.setText("Not Found");
            }
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        barcodeView = findViewById(R.id.barcode_scanner);
        barcodeView.decodeContinuous(callback);

        initView();
    }

    private void initView() {
        ibManageFlashOn = findViewById(R.id.ibManageFlashOn);
        ibManageFlashOff = findViewById(R.id.ibManageFlashOff);
        ibManageFlashOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkButtonStatus();
            }
        });
        ibManageFlashOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkButtonStatus();
            }
        });
    }

    private boolean isFlashSupported() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    private void checkButtonStatus() {
        if (isFlashSupported()) {
            if (ibManageFlashOff.getVisibility() == View.VISIBLE) {
                ibManageFlashOff.setVisibility(View.GONE);
                ibManageFlashOn.setVisibility(View.VISIBLE);
                isLightOn = true;
                flashingOut();
            } else {
                ibManageFlashOn.setVisibility(View.GONE);
                ibManageFlashOff.setVisibility(View.VISIBLE);
                isLightOn = false;
                flashingOut();
            }
        } else {
            Toast.makeText(this, "Your device hardware does not support flashlight!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        barcodeView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        barcodeView.pause();
    }

    private void flashingOut() {
        if (isLightOn)
            barcodeView.setTorchOn();
        else
            barcodeView.setTorchOff();
    }
}
