package com.example.testhana.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.testhana.Helper.Constant;
import com.example.testhana.Helper.TelephonyInfo;
import com.example.testhana.MainActivity;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiClientDirectHana;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.ApiServiceDirectHana;
import com.example.testhana.Rest.ApiServiceHana;
import com.example.testhana.Rest.ModelClass.LoginHanaClass;
import com.example.testhana.Rest.Response.LoginResponse;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    EditText txtUsername;
    EditText txtPassword;
    EditText txtImei;
    Button btnLogin;
    ApiService api;
    ApiServiceHana apiHana;
    ApiServiceDirectHana apiDirectHana;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        api = ApiClient.getClient().create(ApiService.class);
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        apiDirectHana = ApiClientDirectHana.getClient().create(ApiServiceDirectHana.class);
        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassword);
        txtImei = findViewById(R.id.txtImei);
        btnLogin = findViewById(R.id.btnLogin);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            btnLogin.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_SMS, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        } else {
            txtImei.setEnabled(false);
            TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
            String imei1 = telephonyInfo.getImsiSIM1() == null ? "" : telephonyInfo.getImsiSIM1();
            String imei2 = telephonyInfo.getImsiSIM2() == null ? "" : telephonyInfo.getImsiSIM2();

            //Imei 1 tidak mungkin kosong, jika kosong akan mengambil default getDeviceId()
            int compare = imei1.compareTo(imei2);
            // Smaller
            if (compare < 0) {
                txtImei.setText(imei1);
            } else {

                txtImei.setText(imei2);
            }

            if (imei2 == "") {
                txtImei.setText(imei1);
            }

            if (txtImei.getText().toString().equalsIgnoreCase("")) {
                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                txtImei.setText(telephonyManager.getDeviceId());
            }
        }
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validate())
                {
//                    doLoginHana();
                    session.setKeyUserPhone(txtUsername.getText().toString());
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 0: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    btnLogin.setEnabled(true);
                } else {

                }
            }
        }
    }

    public void doLoginHana()
    {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        }
        progressDialog.setMessage("Login");
        progressDialog.setCancelable(false);
        if(progressDialog != null) {
            progressDialog.show();
        }
        LoginHanaClass loginHanaClass = new LoginHanaClass(Constant.COMPANY_DB, Constant.COMPANY_PASS, Constant.COMPANY_USERNAME);
        Call<ResponseBody> call = apiHana.login(loginHanaClass);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        res = new String(response.body().bytes());
                        JSONObject obj = new JSONObject(res);
                        if(obj.getString("SessionId") != null)
                        {
                            session.setKeyLoginHana(obj.getString("odata.metadata"),
                                    obj.getString("SessionId"), obj.getString("Version"),
                                    obj.getString("SessionTimeout"));
                            doLogin();
                        }
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                        dismissProgressDialog();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        dismissProgressDialog();
                    }
                }
                else
                {
                    dismissProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
            }
        });
    }

    public void doLogin()
    {
        Call<LoginResponse> call = api.login(txtUsername.getText().toString(), txtPassword.getText().toString(),
                "865931024035487");
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, final Response<LoginResponse> response) {
                if(response.isSuccessful())
                {
                    dismissProgressDialog();
                    if(response.body().getResult().equalsIgnoreCase("ok"))
                    {
                        session.setKeyUserPhone(txtUsername.getText().toString());
                        session.setKeyUserId(response.body().getUserId());
                        session.setKeyUserName(response.body().getUserName());
                        session.setKeyUserRole(response.body().getUserRole());
                        Intent intent = new Intent(LoginActivity.this, InitDataActivity.class);
                        intent.putExtra("phone", txtUsername.getText().toString());
                        intent.putExtra("username", response.body().getUserName());
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Toast.makeText(LoginActivity.this, "Wrong Phone or Password", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
            }
        });
    }

    private boolean validate()
    {
        boolean valid = true;
        String username = txtUsername.getText().toString();
        String password = txtPassword.getText().toString();
        if(username.isEmpty())
        {
            valid = false;
            txtUsername.setError("Fill Phone");
        }
        else
        {
            txtUsername.setError(null);
        }
        if(password.isEmpty())
        {
            valid = false;
            txtPassword.setError("Fill Password");
        }
        else
        {
            txtPassword.setError(null);
        }
        return valid;
    }
}
