package com.example.testhana.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.testhana.Helper.Tools;
import com.example.testhana.MainActivity;
import com.example.testhana.Models.Realm.CustomerMaster;
import com.example.testhana.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReceiptActivity extends BaseActivity {

    @BindView(R.id.txtChangeRef) TextView txtChangeRef;
    @BindView(R.id.labelChangeRef) TextView labelChangeRef;
    @BindView(R.id.labelChangeCard) TextView labelChangeCard;
    @BindView(R.id.txtChangeCard) TextView txtChangeCard;
    @BindView(R.id.llRefNo) LinearLayout llRef;
    @BindView(R.id.txtOutOfAmount) TextView txtOutOfAmount;
    @BindView(R.id.llChange) LinearLayout llChange;
    @BindView(R.id.editEmail) EditText editEmail;
    @BindView(R.id.btnSend) Button btnSend;
    @BindView(R.id.btnPrintReceipt) Button btnPrint;
    @BindView(R.id.btnPrintItem) Button btnPrintItem;
    @BindView(R.id.btnNoThanks) Button btnNoThanks;
    @BindView(R.id.llReceipt) LinearLayout llReceipt;
    @BindView(R.id.txtHow) TextView txtHow;
    @BindView(R.id.txtTotalAmount) TextView txtTotalAmount;
    @BindView(R.id.tvPaymentMethod) TextView txtPaymentMethod;
    @BindView(R.id.llForMerchant) LinearLayout llForMerchant;
    @BindView(R.id.llForEvent) LinearLayout llForEvent;
    @BindView(R.id.tvKasirSentralHint) TextView tvKasirSentralHint;
    @BindView(R.id.tvKasirSentralOrderNumber) TextView tvKasirSentralOrderNumber;
    @BindView(R.id.tvKasirSentralOrderNumberRight) TextView tvKasirSentralOrderNumberRight;
    @BindView(R.id.tvKasirSentralOrderNumber2) TextView tvKasirSentralOrderNumber2;
    @BindView(R.id.tvKasirSentralOrderNumberRight2) TextView tvKasirSentralOrderNumberRight2;
    @BindView(R.id.btnScan) Button btnScan;
    @BindView(R.id.btnPrintMobey) Button btnPrintMobey;

    Bundle bundle;
    String payType, refno;
    double total = 0.0, change = 0.0, amount = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);
        ButterKnife.bind(this);
        setTitle("Receipt");
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        btnPrint.setEnabled(false);
        bundle = getIntent().getExtras();
        if(bundle != null)
        {
            payType = bundle.getString("payType", "");
            total = bundle.getDouble("total", 0.0);
            amount = bundle.getDouble("amount", 0.0);
            change = bundle.getDouble("change", 0.0);
            refno = bundle.getString("refNo", "");
            txtPaymentMethod.setText(payType);
            txtTotalAmount.setText(Tools.convertMoney(this, total));
            txtOutOfAmount.setText(Tools.convertMoney(this, amount));
            if(change <= 0)
            {
                llChange.setVisibility(View.GONE);
            }
            else
            {
                txtChangeRef.setText(Tools.convertMoney(this, change));
            }
            if(refno.length() > 0)
            {
                llRef.setVisibility(View.VISIBLE);
                txtChangeCard.setText(refno);
            }
            CustomerMaster customerMaster = realm.where(CustomerMaster.class)
                    .equalTo("customerCode", session.getKeyCustomer())
                    .findFirst();
            if(customerMaster != null)
            {
                editEmail.setText(customerMaster.getCustomerEmail() == null ? "" : customerMaster.getCustomerEmail());
            }
        }
        btnNoThanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
        btnSend.setEnabled(true);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{editEmail.getText().toString()});
                email.putExtra(Intent.EXTRA_SUBJECT, "E-Receipt");
                email.putExtra(Intent.EXTRA_TEXT, "");
                email.setType("message/rfc822");
                startActivity(Intent.createChooser(email, "Choose an Email client :"));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
