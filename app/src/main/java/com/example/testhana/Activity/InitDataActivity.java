package com.example.testhana.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.Constant;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Helper.Util;
import com.example.testhana.MainActivity;
import com.example.testhana.Models.Realm.BatchItemMaster;
import com.example.testhana.Models.Realm.CustomerCurrencyMaster;
import com.example.testhana.Models.Realm.CustomerGroupMaster;
import com.example.testhana.Models.Realm.CustomerMaster;
import com.example.testhana.Models.Realm.ItemGroupMaster;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.Models.Realm.ItemPrice;
import com.example.testhana.Models.Realm.Journal;
import com.example.testhana.Models.Realm.LocationCheckIn;
import com.example.testhana.Models.Realm.SalesOrder;
import com.example.testhana.Models.Realm.UOMGroupMaster;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiClientDirectHana;
import com.example.testhana.Rest.ApiClientHana;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.ApiServiceDirectHana;
import com.example.testhana.Rest.ApiServiceHana;
import com.example.testhana.Rest.Response.GetBatchItemResponse;
import com.example.testhana.Rest.Response.GetJournalResponse;
import com.example.testhana.Rest.Response.GetLocationResponse;
import com.example.testhana.Rest.Response.GetSalesOrderResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import okhttp3.ResponseBody;
import pl.tajchert.sample.DotsTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InitDataActivity extends Activity {

    @BindView(R.id.txtTitle) TextView txtTitle;
    @BindView(R.id.tvProgress) TextView txtProgress;
    @BindView(R.id.dots) DotsTextView dots;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    SessionManagement session;
    Bundle bundle;
    ApiService api;
    ApiServiceHana apiHana;
    ApiServiceDirectHana apiDirectHana;
    boolean isConnection = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init_data);
        ButterKnife.bind(this);
//        txtTitle.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/Quicksand-Bold.otf"));
        session = CoreApplication.getInstance().getSession();
        bundle = getIntent().getExtras();
        apiHana = ApiClientHana.getClient().create(ApiServiceHana.class);
        apiDirectHana = ApiClientDirectHana.getClient().create(ApiServiceDirectHana.class);
        api = ApiClient.getClient().create(ApiService.class);
        refreshData("journal");
    }

    public void refreshData(String type) {
        txtProgress.setText("Loading " + type + " Data");
        dots.setVisibility(View.VISIBLE);
        if(type.equalsIgnoreCase("item")) {
            getItemMasterHana();
        }
        else if(type.equalsIgnoreCase("item group")) {
            getItemGroupHana();
        }
        else if(type.equalsIgnoreCase("item batch")) {
            getItemBatch();
        }
        else if(type.equalsIgnoreCase("uom")) {
            getUoMGroupHana();
        }
        else if(type.equalsIgnoreCase("customer group")) {
            getCustomerMasterGroupHana();
        }
        else if(type.equalsIgnoreCase("currency")) {
            getCustomerMasterCurrencyHana();
        }
        else if (type.equalsIgnoreCase("customer")) {
            getCustomerMasterHana();
        }
        else if(type.equalsIgnoreCase("journal")) {
            getJournal();
        }
        else if(type.equalsIgnoreCase("order"))
        {
            getOrder();
        }
        else if(type.equalsIgnoreCase("locationcheckin")) {
            getLocation();
        }
    }

    public void getItemBatch()
    {
        Call<GetBatchItemResponse> call = apiDirectHana.getBatchItem("");
        call.enqueue(new Callback<GetBatchItemResponse>() {
            @Override
            public void onResponse(Call<GetBatchItemResponse> call, final Response<GetBatchItemResponse> response) {
                if(response.isSuccessful())
                {
                    if(response.body().getResult().equalsIgnoreCase("ok"))
                    {
                        final List<BatchItemMaster> itemMasters = response.body().getBatchitemlist();
                        final Realm mRealm = Realm.getDefaultInstance();
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                txtProgress.setText("Batch Item Loaded");
                                dots.setVisibility(View.GONE);
                                refreshData("item group");
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<GetBatchItemResponse> call, Throwable t) {
                Intent i = new Intent(InitDataActivity.this, LoginActivity.class);
                i.putExtra("from", "init");
                startActivity(i);
                finish();
            }
        });
    }

    public void getLocation()
    {
        Call<GetLocationResponse> call = api.getLocation(session.getKeyUserId(), Util.getDate());
        call.enqueue(new Callback<GetLocationResponse>() {
            @Override
            public void onResponse(Call<GetLocationResponse> call, final Response<GetLocationResponse> response) {
                if(response.isSuccessful())
                {
                    if(response.body().getResult().equalsIgnoreCase("ok"))
                    {
                        final List<LocationCheckIn> itemMasters = response.body().getLocations();
                        final Realm mRealm = Realm.getDefaultInstance();
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                txtProgress.setText("Location Loaded");
                                dots.setVisibility(View.GONE);
                                refreshData("item batch");
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<GetLocationResponse> call, Throwable t) {
                Intent i = new Intent(InitDataActivity.this, LoginActivity.class);
                i.putExtra("from", "init");
                startActivity(i);
                finish();
            }
        });
    }

    public void getJournal()
    {
        Call<GetJournalResponse> call = api.getJournal(Util.getDate(),session.getKeyUserId());
        call.enqueue(new Callback<GetJournalResponse>() {
            @Override
            public void onResponse(Call<GetJournalResponse> call, final Response<GetJournalResponse> response) {
                if(response.isSuccessful())
                {
                    if(response.body().getResult().equalsIgnoreCase("ok"))
                    {
                        final List<Journal> itemMasters = response.body().getJournals();
                        final Realm mRealm = Realm.getDefaultInstance();
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                txtProgress.setText("Journals Loaded");
                                dots.setVisibility(View.GONE);
                                refreshData("order");
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<GetJournalResponse> call, Throwable t) {
                Intent i = new Intent(InitDataActivity.this, LoginActivity.class);
                i.putExtra("from", "init");
                startActivity(i);
                finish();
            }
        });
    }

    public void getOrder()
    {
        Call<GetSalesOrderResponse> call = api.getSalesOrder(session.getKeyUserId(), session.getKeyUserRole());
        call.enqueue(new Callback<GetSalesOrderResponse>() {
            @Override
            public void onResponse(Call<GetSalesOrderResponse> call, final Response<GetSalesOrderResponse> response) {
                if(response.isSuccessful())
                {
                    if(response.body().getResult().equalsIgnoreCase("ok"))
                    {
                        final List<SalesOrder> itemMasters = response.body().getDetails();
                        final Realm mRealm = Realm.getDefaultInstance();
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                txtProgress.setText("Orders Loaded");
                                dots.setVisibility(View.GONE);
                                refreshData("locationcheckin");
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<GetSalesOrderResponse> call, Throwable t) {
                Intent i = new Intent(InitDataActivity.this, LoginActivity.class);
                i.putExtra("from", "init");
                startActivity(i);
                finish();
            }
        });
    }

    public void getItemGroupHana()
    {
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.getItemGroup(cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        final Realm realm = Realm.getDefaultInstance();
                        res = new String(response.body().bytes());
                        JSONObject obj = new JSONObject(res);
                        JSONArray value = obj.getJSONArray("value");
                        final List<ItemGroupMaster> customerMasters = new ArrayList<>();
                        for(int i = 0 ; i < value.length() ; i++)
                        {
                            JSONObject customer = value.getJSONObject(i);
                            ItemGroupMaster itemGroupMaster = new ItemGroupMaster();
                            JSONArray itemgroupwarehouses = customer.getJSONArray("ItemGroupsWarehouseInfos");
                            for(int j = 0 ; j < itemgroupwarehouses.length() ; j++)
                            {
                                JSONObject itemgroupwarehouse = itemgroupwarehouses.getJSONObject(j);
                                itemGroupMaster.setItemGroupId(itemgroupwarehouse.getInt("ItmsGrpCod"));
                            }
                            itemGroupMaster.setItemGroupName(customer.getString("GroupName"));
                            customerMasters.add(itemGroupMaster);
                        }
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(customerMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                txtProgress.setText("Items Group Loaded");
                                dots.setVisibility(View.GONE);
                                refreshData("uom");
                            }
                        });
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Intent i = new Intent(InitDataActivity.this, LoginActivity.class);
                i.putExtra("from", "init");
                startActivity(i);
                finish();
            }
        });
    }

    public void getUoMGroupHana()
    {
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.getUoMGroup(cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        final Realm realm = Realm.getDefaultInstance();
                        res = new String(response.body().bytes());
                        JSONObject obj = new JSONObject(res);
                        JSONArray value = obj.getJSONArray("value");
                        final List<UOMGroupMaster> customerMasters = new ArrayList<>();
                        for(int i = 0 ; i < value.length() ; i++)
                        {
                            JSONObject customer = value.getJSONObject(i);
                            UOMGroupMaster itemGroupMaster = new UOMGroupMaster();
                            itemGroupMaster.setUOMGroupId(customer.getInt("BaseUoM"));
                            itemGroupMaster.setUOMGroupName(customer.getString("Name"));
                            itemGroupMaster.setUOMGroupCode(customer.getString("Code"));
                            customerMasters.add(itemGroupMaster);
                        }
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(customerMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                txtProgress.setText("UoM Group Loaded");
                                dots.setVisibility(View.GONE);
                                refreshData("item");
                            }
                        });
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Intent i = new Intent(InitDataActivity.this, LoginActivity.class);
                i.putExtra("from", "init");
                startActivity(i);
                finish();
            }
        });
    }


    public void getItemMasterHana()
    {
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.getFirstItems(Constant.SELECT_ITEM, cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        final Realm realm = Realm.getDefaultInstance();
                        res = new String(response.body().bytes());
                        JSONObject obj = new JSONObject(res);
                        if(obj.getString("odata.nextLink") != null)
                        {
                            String[] nextlink = obj.getString("odata.nextLink").split("=");
                            session.setKeyCurrItem("0");
                            session.setKeyNextItem(nextlink[2]);
                        }
                        JSONArray value = obj.getJSONArray("value");
                        final List<ItemMaster> itemMasters = new ArrayList<>();
                        for(int i = 0 ; i < value.length() ; i++)
                        {
                            JSONObject item = value.getJSONObject(i);
                            ItemMaster itemMaster = new ItemMaster();
                            itemMaster.setItemId(i + 1);
                            itemMaster.setItemNo(item.getString("ItemCode"));
                            itemMaster.setItemDesc(item.getString("ItemName"));
                            JSONArray itemwarehouse = item.getJSONArray("ItemWarehouseInfoCollection");
                            int stock = 0;
                            for(int j = 0 ; j < itemwarehouse.length() ; j++)
                            {
                                JSONObject warehouse = itemwarehouse.getJSONObject(j);
                                stock = stock + warehouse.getInt("InStock");
                            }
                            itemMaster.setStock(stock);
                            itemMaster.setForeignName(item.getString("ForeignName"));
                            itemMaster.setItemGroupId(item.getInt("ItemsGroupCode"));
                            itemMaster.setItemTypeId(item.getString("ItemType").equalsIgnoreCase("ititems") ? 1 :
                                    item.getString("ItemType").equalsIgnoreCase("itlabor") ? 2 : 3);
                            itemMaster.setBarcode(item.getString("BarCode"));
                            itemMasters.add(itemMaster);
                            JSONArray itemprice = item.getJSONArray("ItemPrices");
                            for(int j = 0 ; j < itemprice.length() ; j++)
                            {
                                JSONObject price = itemprice.getJSONObject(j);
                                realm.beginTransaction();
                                ItemPrice itemPrice = new ItemPrice();
                                itemPrice.setPriceId(realm.where(ItemPrice.class).findAll().size() + 1);
                                itemPrice.setPriceList(price.getInt("PriceList"));
                                itemPrice.setPrice(price.getString("Price").equalsIgnoreCase("null") ? 0 : price.getInt("Price"));
                                itemPrice.setCurrency(price.getString("Currency"));
                                itemPrice.setItemId(item.getString("ItemCode"));
                                realm.copyToRealmOrUpdate(itemPrice);
                                realm.commitTransaction();
                            }
                        }
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                txtProgress.setText("Items Loaded");
                                dots.setVisibility(View.GONE);
                                refreshData("customer group");
                            }
                        });
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Intent i = new Intent(InitDataActivity.this, LoginActivity.class);
                i.putExtra("from", "init");
                startActivity(i);
                finish();
            }
        });
    }

    public void getCustomerMasterGroupHana()
    {
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.getBusinessPartnerGroup(cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        final Realm realm = Realm.getDefaultInstance();
                        res = new String(response.body().bytes());
                        JSONObject obj = new JSONObject(res);
                        JSONArray value = obj.getJSONArray("value");
                        final List<CustomerGroupMaster> customerMasters = new ArrayList<>();
                        for(int i = 0 ; i < value.length() ; i++)
                        {
                            JSONObject customer = value.getJSONObject(i);
                            CustomerGroupMaster customerMaster = new CustomerGroupMaster();
                            customerMaster.setCustomerGroupId(customer.getInt("Code"));
                            customerMaster.setCustomerGroupName(customer.getString("Name"));
                            customerMaster.setCustomerGroupType(customer.getString("Type"));
                            customerMasters.add(customerMaster);
                        }
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(customerMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                txtProgress.setText("Customer Group Loaded");
                                dots.setVisibility(View.GONE);
                                refreshData("currency");
                            }
                        });
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Intent i = new Intent(InitDataActivity.this, LoginActivity.class);
                i.putExtra("from", "init");
                startActivity(i);
                finish();
            }
        });
    }

    public void getCustomerMasterCurrencyHana()
    {
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.getBusinessPartnerCurrency(cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        final Realm realm = Realm.getDefaultInstance();
                        res = new String(response.body().bytes());
                        JSONObject obj = new JSONObject(res);
                        JSONArray value = obj.getJSONArray("value");
                        final List<CustomerCurrencyMaster> customerMasters = new ArrayList<>();
                        for(int i = 0 ; i < value.length() ; i++)
                        {
                            JSONObject customer = value.getJSONObject(i);
                            CustomerCurrencyMaster customerMaster = new CustomerCurrencyMaster();
                            customerMaster.setCode(customer.getString("Code"));
                            customerMaster.setName(customer.getString("Name"));
                            customerMaster.setDocumentsCode(customer.getString("DocumentsCode"));
                            customerMaster.setInternationalDescription(customer.getString("InternationalDescription"));
                            customerMaster.setHundredthName(customer.getString("HundredthName"));
                            customerMaster.setEnglishName(customer.getString("EnglishName"));
                            customerMaster.setEnglishHundredthName(customer.getString("EnglishHundredthName"));
                            customerMaster.setPluralInternationalDescription(customer.getString("PluralInternationalDescription"));
                            customerMaster.setPluralHundredthName(customer.getString("PluralHundredthName"));
                            customerMaster.setPluralEnglishName(customer.getString("PluralEnglishName"));
                            customerMaster.setPluralEnglishHundredthName(customer.getString("PluralEnglishHundredthName"));
                            customerMaster.setDecimals(customer.getString("Decimals"));
                            customerMaster.setRounding(customer.getString("Rounding"));
                            customerMaster.setRoundingInPayment(customer.getString("RoundingInPayment"));
                            customerMaster.setMaxIncomingAmtDiff(customer.getString("MaxIncomingAmtDiff").equalsIgnoreCase("null") ?
                                    0 : customer.getDouble("MaxIncomingAmtDiff"));
                            customerMaster.setMaxOutgoingAmtDiff(customer.getString("MaxOutgoingAmtDiff").equalsIgnoreCase("null") ?
                                    0 : customer.getDouble("MaxOutgoingAmtDiff"));
                            customerMaster.setMaxIncomingAmtDiffPercent(customer.getString("MaxIncomingAmtDiffPercent").equalsIgnoreCase("null") ?
                                    0 : customer.getDouble("MaxIncomingAmtDiffPercent"));
                            customerMaster.setMaxOutgoingAmtDiffPercent(customer.getString("MaxOutgoingAmtDiffPercent").equalsIgnoreCase("null") ?
                                    0 : customer.getDouble("MaxOutgoingAmtDiffPercent"));
                            customerMasters.add(customerMaster);
                        }
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(customerMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                txtProgress.setText("Currencies Loaded");
                                dots.setVisibility(View.GONE);
                                refreshData("customer");
                            }
                        });
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Intent i = new Intent(InitDataActivity.this, LoginActivity.class);
                i.putExtra("from", "init");
                startActivity(i);
                finish();
            }
        });
    }

    public void getCustomerMasterHana()
    {
        String cookie = "B1SESSION=" + session.getKeySessionId() + ";  ROUTEID=.node0";
        Call<ResponseBody> call = apiHana.getFirstBusinessPartner(Constant.SELECT_CUSTOMER, cookie);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if(response.isSuccessful())
                {
                    String res = null;
                    try {
                        final Realm realm = Realm.getDefaultInstance();
                        res = new String(response.body().bytes());
                        JSONObject obj = new JSONObject(res);
                        {
                            String[] nextlink = obj.getString("odata.nextLink").split("=");
                            session.setKeyCurrCust("0");
                            session.setKeyNextCust(nextlink[2]);
                        }
                        JSONArray value = obj.getJSONArray("value");
                        final List<CustomerMaster> customerMasters = new ArrayList<>();
                        for(int i = 0 ; i < value.length() ; i++)
                        {
                            JSONObject customer = value.getJSONObject(i);
                            CustomerMaster customerMaster = new CustomerMaster();
                            customerMaster.setCustomerId(i + 1);
                            customerMaster.setCustomerCode(customer.getString("CardCode"));
                            customerMaster.setCustomerName(customer.getString("CardName"));
                            customerMaster.setCustomerRoleId(customer.getString("CardType").equalsIgnoreCase("ccustomer") ? 1 :
                                    customer.getString("CardType").equalsIgnoreCase("clid") ? 2 : 3);
                            customerMaster.setCustomerGroupId(customer.getInt("GroupCode"));
                            customerMaster.setCustomerCurrencyId(customer.getString("Currency"));
                            customerMaster.setCustomerPhone(customer.getString("Phone1"));
                            customerMaster.setCustomerAddress(customer.getString("Address"));
                            customerMaster.setCustomerEmail(customer.getString("EmailAddress"));
                            customerMasters.add(customerMaster);
                        }
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(customerMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                txtProgress.setText("Customers Loaded");
                                dots.setVisibility(View.GONE);
                                Intent i = new Intent(InitDataActivity.this, MainActivity.class);
                                startActivity(i);
                                finish();
                            }
                        });
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Intent i = new Intent(InitDataActivity.this, LoginActivity.class);
                i.putExtra("from", "init");
                startActivity(i);
                finish();
            }
        });
    }
}
