package com.example.testhana.Activity;

import android.content.Intent;
import android.os.Bundle;

import com.example.testhana.MainActivity;
import com.example.testhana.R;


public class SplashScreenActivity extends BaseActivity {
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        if(session.getKeyUserPhone().equals(""))
        {
            intent = new Intent(this, LoginActivity.class);
        }
        else
        {
            intent = new Intent(this, MainActivity.class);
        }
        startActivity(intent);
        finish();
    }
}
