package com.example.testhana.Models.Model;

import java.util.List;

public class CartModel {
    private long rowNo;
    private String itemId;
    private String itemDesc;
    private double itemPrice;
    private int itemQty;
    private String remarks;
    private double total;
    private List<Discount> discounts;

    public CartModel(long row, String id, String desc, double price, int qty, String note, double total, List<Discount> discounts)
    {
        this.rowNo = row;
        this.itemId = id;
        this.itemDesc = desc;
        this.itemPrice = price;
        this.itemQty = qty;
        this.remarks = note;
        this.total = total;
        this.discounts = discounts;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public List<Discount> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<Discount> discounts) {
        this.discounts = discounts;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public long getRowNo() {
        return rowNo;
    }

    public void setRowNo(long rowNo) {
        this.rowNo = rowNo;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public int getItemQty() {
        return itemQty;
    }

    public void setItemQty(int itemQty) {
        this.itemQty = itemQty;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
