package com.example.testhana.Models.Model;

public class ItemPriceModel {
    private int PriceList;
    private int Price;
    private String Currency;
    private String itemId;

    public ItemPriceModel(int pl, int p, String curr, String ii)
    {
        this.PriceList = pl;
        this.Price = p;
        this.Currency = curr;
        this.itemId = ii;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public int getPriceList() {
        return PriceList;
    }

    public void setPriceList(int priceList) {
        PriceList = priceList;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }
}
