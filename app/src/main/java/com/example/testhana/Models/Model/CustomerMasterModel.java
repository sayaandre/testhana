package com.example.testhana.Models.Model;

public class CustomerMasterModel {
    private int customerId;
    private String customerCode;
    private int customerRoleId;
    private String customerName;
    private int customerGroupId;
    private String customerCurrencyId;
    private String customerPhone;
    private String customerAddress;
    private String customerEmail;
    private String customerImage;
    private String customerBarcode;

    public CustomerMasterModel(int id, String code, int role, String name, int group, String curr, String phone, String addres,
                               String email, String image, String barcode)
    {
        this.customerId = id;
        this.customerCode = code;
        this.customerRoleId = role;
        this.customerName = name;
        this.customerGroupId = group;
        this.customerCurrencyId = curr;
        this.customerPhone = phone;
        this.customerAddress = addres;
        this.customerEmail = email;
        this.customerImage = image;
        this.customerBarcode = barcode;
    }

    public String getCustomerBarcode() {
        return customerBarcode;
    }

    public void setCustomerBarcode(String customerBarcode) {
        this.customerBarcode = customerBarcode;
    }

    public String getCustomerImage() {
        return customerImage;
    }

    public void setCustomerImage(String customerImage) {
        this.customerImage = customerImage;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public int getCustomerRoleId() {
        return customerRoleId;
    }

    public void setCustomerRoleId(int customerRoleId) {
        this.customerRoleId = customerRoleId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getCustomerGroupId() {
        return customerGroupId;
    }

    public void setCustomerGroupId(int customerGroupId) {
        this.customerGroupId = customerGroupId;
    }

    public String getCustomerCurrencyId() {
        return customerCurrencyId;
    }

    public void setCustomerCurrencyId(String customerCurrencyId) {
        this.customerCurrencyId = customerCurrencyId;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }
}
