package com.example.testhana.Models.Model;

public class JournalDetailModel {
    private long journalDetailId;
    private long journalId;
    private String itemId;
    private String itemName;
    private double itemPrice;
    private double itemQty;
    private double itemTotalPrice;
    private String notes;
    private String journalType;

    public JournalDetailModel(long id, long jid, String iid, String name, double price, double qty, double total, String note,
                              String jt)
    {
        this.journalDetailId = id;
        this.journalId = jid;
        this.itemId = iid;
        this.itemName = name;
        this.itemPrice = price;
        this.itemQty = qty;
        this.itemTotalPrice = total;
        this.notes = note;
        this.journalType = jt;
    }

    public String getJournalType() {
        return journalType;
    }

    public void setJournalType(String journalType) {
        this.journalType = journalType;
    }

    public long getJournalDetailId() {
        return journalDetailId;
    }

    public void setJournalDetailId(long journalDetailId) {
        this.journalDetailId = journalDetailId;
    }

    public long getJournalId() {
        return journalId;
    }

    public void setJournalId(long journalId) {
        this.journalId = journalId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public double getItemQty() {
        return itemQty;
    }

    public void setItemQty(double itemQty) {
        this.itemQty = itemQty;
    }

    public double getItemTotalPrice() {
        return itemTotalPrice;
    }

    public void setItemTotalPrice(double itemTotalPrice) {
        this.itemTotalPrice = itemTotalPrice;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
