package com.example.testhana.Models.Model;

public class BatchItemMasterModel {
    private int logentry;
    private String itemname;
    private String itemcode;
    private int qty;
    private int qtyout;
    private String batchno;

    public BatchItemMasterModel(int le, String in, String ic, int q, int qo, String bn)
    {
        this.logentry = le;
        this.itemname = in;
        this.itemcode = ic;
        this.qty = q;
        this.qtyout = qo;
        this.batchno = bn;
    }

    public int getLogentry() {
        return logentry;
    }

    public void setLogentry(int logentry) {
        this.logentry = logentry;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getQtyout() {
        return qtyout;
    }

    public void setQtyout(int qtyout) {
        this.qtyout = qtyout;
    }

    public String getBatchno() {
        return batchno;
    }

    public void setBatchno(String batchno) {
        this.batchno = batchno;
    }
}
