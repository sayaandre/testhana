package com.example.testhana.Models.Realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ItemPrice extends RealmObject {
    @PrimaryKey
    private int priceId;
    private int PriceList;
    private int Price;
    private String Currency;
    private String itemId;

    public int getPriceId() {
        return priceId;
    }

    public void setPriceId(int priceId) {
        this.priceId = priceId;
    }

    public int getPriceList() {
        return PriceList;
    }

    public void setPriceList(int priceList) {
        PriceList = priceList;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
}
