package com.example.testhana.Models.Model;


public class LocationCheckInModel {
    private int locationId;
    private String locationName;
    private String locationCity;
    private String locationAddress;
    private double latitude;
    private double longitude;
    private boolean isCheckIn;

    public LocationCheckInModel(int li, String ln, String lc, String la, double lat, double lon, boolean ici)
    {
        this.locationId = li;
        this.locationName = ln;
        this.locationCity = lc;
        this.locationAddress = la;
        this.latitude = lat;
        this.longitude = lon;
        this.isCheckIn = ici;
    }

    public boolean isCheckIn() {
        return isCheckIn;
    }

    public void setCheckIn(boolean checkIn) {
        isCheckIn = checkIn;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationCity() {
        return locationCity;
    }

    public void setLocationCity(String locationCity) {
        this.locationCity = locationCity;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
