package com.example.testhana.Models.Model;

public class SalesOrderDetailModel {
    private long salesOrderDetailId;
    private long salesOrderId;
    private String itemId;
    private String itemName;
    private double itemQty;
    private String notes;
    private String status;
    private String itemBatch;

    public SalesOrderDetailModel(long sodi, long soi, String ii, String in, double iq, String n, String s, String ib)
    {
        this.salesOrderDetailId = sodi;
        this.salesOrderId = soi;
        this.itemId = ii;
        this.itemName = in;
        this.itemQty = iq;
        this.notes = n;
        this.status = s;
        this.itemBatch = ib;
    }

    public String getItemBatch() {
        return itemBatch;
    }

    public void setItemBatch(String itemBatch) {
        this.itemBatch = itemBatch;
    }

    public long getSalesOrderDetailId() {
        return salesOrderDetailId;
    }

    public void setSalesOrderDetailId(long salesOrderDetailId) {
        this.salesOrderDetailId = salesOrderDetailId;
    }

    public long getSalesOrderId() {
        return salesOrderId;
    }

    public void setSalesOrderId(long salesOrderId) {
        this.salesOrderId = salesOrderId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getItemQty() {
        return itemQty;
    }

    public void setItemQty(double itemQty) {
        this.itemQty = itemQty;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
