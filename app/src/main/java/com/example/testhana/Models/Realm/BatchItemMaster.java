package com.example.testhana.Models.Realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class BatchItemMaster extends RealmObject {
    @PrimaryKey
    private int logentry;
    private String itemcode;
    private String itemname;
    private int qty;
    private int qtyout;
    private String batchno;

    public int getLogentry() {
        return logentry;
    }

    public void setLogentry(int logentry) {
        this.logentry = logentry;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getQtyout() {
        return qtyout;
    }

    public void setQtyout(int qtyout) {
        this.qtyout = qtyout;
    }

    public String getBatchno() {
        return batchno;
    }

    public void setBatchno(String batchno) {
        this.batchno = batchno;
    }
}
