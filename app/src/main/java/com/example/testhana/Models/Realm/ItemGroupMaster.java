package com.example.testhana.Models.Realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ItemGroupMaster extends RealmObject {
    @PrimaryKey
    private int itemGroupId;
    private String itemGroupName;

    public int getItemGroupId() {
        return itemGroupId;
    }

    public void setItemGroupId(int itemGroupId) {
        this.itemGroupId = itemGroupId;
    }

    public String getItemGroupName() {
        return itemGroupName;
    }

    public void setItemGroupName(String itemGroupName) {
        this.itemGroupName = itemGroupName;
    }
}
