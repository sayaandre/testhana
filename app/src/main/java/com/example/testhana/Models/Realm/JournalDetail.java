package com.example.testhana.Models.Realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class JournalDetail extends RealmObject {
    @PrimaryKey
    private long journalDetailId;
    private long journalId;
    private String itemId;
    private String itemName;
    private Double itemPrice;
    private double itemQty;
    private Double itemTotalPrice;
    private String notes;
    private String journalType;

    public String getJournalType() {
        return journalType;
    }

    public void setJournalType(String journalType) {
        this.journalType = journalType;
    }

    public long getJournalDetailId() {
        return journalDetailId;
    }

    public void setJournalDetailId(long journalDetailId) {
        this.journalDetailId = journalDetailId;
    }

    public long getJournalId() {
        return journalId;
    }

    public void setJournalId(long journalId) {
        this.journalId = journalId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(Double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public double getItemQty() {
        return itemQty;
    }

    public void setItemQty(double itemQty) {
        this.itemQty = itemQty;
    }

    public Double getItemTotalPrice() {
        return itemTotalPrice;
    }

    public void setItemTotalPrice(Double itemTotalPrice) {
        this.itemTotalPrice = itemTotalPrice;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
