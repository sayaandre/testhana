package com.example.testhana.Models.Model;

public class Discount {
    public String promo_name;
    public double percentage;
    public double amount;
    public int affectedItem;
    public int promo_code;

    public int getPromo_code() {
        return promo_code;
    }

    public void setPromo_code(int promo_code) {
        this.promo_code = promo_code;
    }

    public String getPromo_name() {
        return promo_name;
    }

    public void setPromo_name(String promo_name) {
        this.promo_name = promo_name;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getAffectedItem() {
        return affectedItem;
    }

    public void setAffectedItem(int affectedItem) {
        this.affectedItem = affectedItem;
    }
}
