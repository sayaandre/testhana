package com.example.testhana.Models.Realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CustomerGroupMaster extends RealmObject {
    @PrimaryKey
    private int customerGroupId;
    private String customerGroupName;
    private String customerGroupType;

    public String getCustomerGroupType() {
        return customerGroupType;
    }

    public void setCustomerGroupType(String customerGroupType) {
        this.customerGroupType = customerGroupType;
    }

    public int getCustomerGroupId() {
        return customerGroupId;
    }

    public void setCustomerGroupId(int customerGroupId) {
        this.customerGroupId = customerGroupId;
    }

    public String getCustomerGroupName() {
        return customerGroupName;
    }

    public void setCustomerGroupName(String customerGroupName) {
        this.customerGroupName = customerGroupName;
    }
}
