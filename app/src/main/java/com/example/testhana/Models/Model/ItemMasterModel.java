package com.example.testhana.Models.Model;

public class ItemMasterModel {
    private int itemId;
    private String itemNo;
    private String itemDesc;
    private String foreignName;
    private int itemTypeId;
    private int itemGroupId;
    private int UOMGroupId;
    private String barcode;
    private double unitPrice;
    private String itemImage;
    private int stock;

    public ItemMasterModel()
    {

    }

    public ItemMasterModel(int id, String no, String desc, String name, int typeid, int groupid, int uomgroupid, String barcode,
                           double price, String img, int st)
    {
        this.itemId = id;
        this.itemNo = no;
        this.itemDesc = desc;
        this.foreignName = name;
        this.itemTypeId = typeid;
        this.itemGroupId = groupid;
        this.UOMGroupId = uomgroupid;
        this.barcode = barcode;
        this.unitPrice = price;
        this.itemImage = img;
        this.stock = st;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemNo() {
        return itemNo;
    }

    public void setItemNo(String itemNo) {
        this.itemNo = itemNo;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getForeignName() {
        return foreignName;
    }

    public void setForeignName(String foreignName) {
        this.foreignName = foreignName;
    }

    public int getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(int itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public int getItemGroupId() {
        return itemGroupId;
    }

    public void setItemGroupId(int itemGroupId) {
        this.itemGroupId = itemGroupId;
    }

    public int getUOMGroupId() {
        return UOMGroupId;
    }

    public void setUOMGroupId(int UOMGroupId) {
        this.UOMGroupId = UOMGroupId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }
}
