package com.example.testhana.Models.Realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CustomerCurrencyMaster extends RealmObject {
    @PrimaryKey
    private String Code;
    private String Name;
    private String DocumentsCode;
    private String InternationalDescription;
    private String HundredthName;
    private String EnglishName;
    private String EnglishHundredthName;
    private String PluralInternationalDescription;
    private String PluralHundredthName;
    private String PluralEnglishName;
    private String PluralEnglishHundredthName;
    private String Decimals;
    private String Rounding;
    private String RoundingInPayment;
    private double MaxIncomingAmtDiff;
    private double MaxOutgoingAmtDiff;
    private double MaxIncomingAmtDiffPercent;
    private double MaxOutgoingAmtDiffPercent;

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDocumentsCode() {
        return DocumentsCode;
    }

    public void setDocumentsCode(String documentsCode) {
        DocumentsCode = documentsCode;
    }

    public String getInternationalDescription() {
        return InternationalDescription;
    }

    public void setInternationalDescription(String internationalDescription) {
        InternationalDescription = internationalDescription;
    }

    public String getHundredthName() {
        return HundredthName;
    }

    public void setHundredthName(String hundredthName) {
        HundredthName = hundredthName;
    }

    public String getEnglishName() {
        return EnglishName;
    }

    public void setEnglishName(String englishName) {
        EnglishName = englishName;
    }

    public String getEnglishHundredthName() {
        return EnglishHundredthName;
    }

    public void setEnglishHundredthName(String englishHundredthName) {
        EnglishHundredthName = englishHundredthName;
    }

    public String getPluralInternationalDescription() {
        return PluralInternationalDescription;
    }

    public void setPluralInternationalDescription(String pluralInternationalDescription) {
        PluralInternationalDescription = pluralInternationalDescription;
    }

    public String getPluralHundredthName() {
        return PluralHundredthName;
    }

    public void setPluralHundredthName(String pluralHundredthName) {
        PluralHundredthName = pluralHundredthName;
    }

    public String getPluralEnglishName() {
        return PluralEnglishName;
    }

    public void setPluralEnglishName(String pluralEnglishName) {
        PluralEnglishName = pluralEnglishName;
    }

    public String getPluralEnglishHundredthName() {
        return PluralEnglishHundredthName;
    }

    public void setPluralEnglishHundredthName(String pluralEnglishHundredthName) {
        PluralEnglishHundredthName = pluralEnglishHundredthName;
    }

    public String getDecimals() {
        return Decimals;
    }

    public void setDecimals(String decimals) {
        Decimals = decimals;
    }

    public String getRounding() {
        return Rounding;
    }

    public void setRounding(String rounding) {
        Rounding = rounding;
    }

    public String getRoundingInPayment() {
        return RoundingInPayment;
    }

    public void setRoundingInPayment(String roundingInPayment) {
        RoundingInPayment = roundingInPayment;
    }

    public double getMaxIncomingAmtDiff() {
        return MaxIncomingAmtDiff;
    }

    public void setMaxIncomingAmtDiff(double maxIncomingAmtDiff) {
        MaxIncomingAmtDiff = maxIncomingAmtDiff;
    }

    public double getMaxOutgoingAmtDiff() {
        return MaxOutgoingAmtDiff;
    }

    public void setMaxOutgoingAmtDiff(double maxOutgoingAmtDiff) {
        MaxOutgoingAmtDiff = maxOutgoingAmtDiff;
    }

    public double getMaxIncomingAmtDiffPercent() {
        return MaxIncomingAmtDiffPercent;
    }

    public void setMaxIncomingAmtDiffPercent(double maxIncomingAmtDiffPercent) {
        MaxIncomingAmtDiffPercent = maxIncomingAmtDiffPercent;
    }

    public double getMaxOutgoingAmtDiffPercent() {
        return MaxOutgoingAmtDiffPercent;
    }

    public void setMaxOutgoingAmtDiffPercent(double maxOutgoingAmtDiffPercent) {
        MaxOutgoingAmtDiffPercent = maxOutgoingAmtDiffPercent;
    }
}
