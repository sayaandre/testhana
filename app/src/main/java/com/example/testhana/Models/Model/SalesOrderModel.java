package com.example.testhana.Models.Model;

public class SalesOrderModel {
    private long salesOrderId;
    private String salesOrderNum;
    private Double total;
    private Integer rowCount;
    private String trxDate;
    private String trxTime;
    private int userId;
    private String customerId;
    private String status;

    public SalesOrderModel(long soi, String son, double t, int rc, String td, String tt, int ui, String ci, String s)
    {
        this.salesOrderId = soi;
        this.salesOrderNum = son;
        this.total = t;
        this.rowCount = rc;
        this.trxDate = td;
        this.trxTime = tt;
        this.userId = ui;
        this.customerId = ci;
        this.status = s;
    }

    public String getSalesOrderNum() {
        return salesOrderNum;
    }

    public void setSalesOrderNum(String salesOrderNum) {
        this.salesOrderNum = salesOrderNum;
    }

    public long getSalesOrderId() {
        return salesOrderId;
    }

    public void setSalesOrderId(long salesOrderId) {
        this.salesOrderId = salesOrderId;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Integer getRowCount() {
        return rowCount;
    }

    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
    }

    public String getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(String trxDate) {
        this.trxDate = trxDate;
    }

    public String getTrxTime() {
        return trxTime;
    }

    public void setTrxTime(String trxTime) {
        this.trxTime = trxTime;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
