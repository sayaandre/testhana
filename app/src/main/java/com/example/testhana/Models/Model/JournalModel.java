package com.example.testhana.Models.Model;

public class JournalModel {
    private long journalId;
    private String receiptNumber;
    private Double total;
    private Integer rowCount;
    private String trxType;
    private String trxDate;
    private String trxTime;
    private String cashierId;
    private String customerId;
    private String refNo;
    private int docEntry;
    private int docNum;

    public JournalModel(long id, String rno, double total, int rcount, String type, String date, String time,
                        String cid, String custid, String refn, int docEntry, int docNum)
    {
        this.journalId = id;
        this.receiptNumber = rno;
        this.total = total;
        this.rowCount = rcount;
        this.trxType = type;
        this.trxDate = date;
        this.trxTime = time;
        this.cashierId = cid;
        this.customerId = custid;
        this.refNo = refn;
        this.docEntry = docEntry;
        this.docNum = docNum;
    }

    public int getDocEntry() {
        return docEntry;
    }

    public void setDocEntry(int docEntry) {
        this.docEntry = docEntry;
    }

    public int getDocNum() {
        return docNum;
    }

    public void setDocNum(int docNum) {
        this.docNum = docNum;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public long getJournalId() {
        return journalId;
    }

    public void setJournalId(long journalId) {
        this.journalId = journalId;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Integer getRowCount() {
        return rowCount;
    }

    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public String getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(String trxDate) {
        this.trxDate = trxDate;
    }

    public String getTrxTime() {
        return trxTime;
    }

    public void setTrxTime(String trxTime) {
        this.trxTime = trxTime;
    }

    public String getCashierId() {
        return cashierId;
    }

    public void setCashierId(String cashierId) {
        this.cashierId = cashierId;
    }
}
