package com.example.testhana.Models.Realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UOMGroupMaster extends RealmObject {
    @PrimaryKey
    private int UOMGroupId;
    private String UOMGroupName;
    private String UOMGroupCode;

    public String getUOMGroupCode() {
        return UOMGroupCode;
    }

    public void setUOMGroupCode(String UOMGroupCode) {
        this.UOMGroupCode = UOMGroupCode;
    }

    public int getUOMGroupId() {
        return UOMGroupId;
    }

    public void setUOMGroupId(int UOMGroupId) {
        this.UOMGroupId = UOMGroupId;
    }

    public String getUOMGroupName() {
        return UOMGroupName;
    }

    public void setUOMGroupName(String UOMGroupName) {
        this.UOMGroupName = UOMGroupName;
    }
}
