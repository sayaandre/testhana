package com.example.testhana.Firebase;

import android.util.Log;

import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.Response.UpdateTokenFBResponse;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseInstanceService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
        SessionManagement session = CoreApplication.getInstance().getSession();
        int userId = session.getKeyUserId();
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<UpdateTokenFBResponse> call = apiService.updateTokenFB(userId, token);
        call.enqueue(new Callback<UpdateTokenFBResponse>() {
            @Override
            public void onResponse(Call<UpdateTokenFBResponse> call, final Response<UpdateTokenFBResponse> response) {
                if(response.isSuccessful())
                {

                }
            }

            @Override
            public void onFailure(Call<UpdateTokenFBResponse> call, Throwable t) {

            }
        });
    }
}
