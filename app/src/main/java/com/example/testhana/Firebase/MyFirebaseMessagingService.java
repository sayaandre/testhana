package com.example.testhana.Firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.example.testhana.Core.CoreApplication;
import com.example.testhana.Helper.SessionManagement;
import com.example.testhana.MainActivity;
import com.example.testhana.Models.Realm.SalesOrder;
import com.example.testhana.R;
import com.example.testhana.Rest.ApiClient;
import com.example.testhana.Rest.ApiService;
import com.example.testhana.Rest.Response.GetSalesOrderResponse;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    private static final String NOTIFICATION_CHANNEL_ID = "POSVIT_Channel";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getData().size());
        SessionManagement sessionManagement = CoreApplication.getInstance().getSession();
        if(!sessionManagement.getKeyUserRole().equalsIgnoreCase(""))
        {
            final Map<String, String> data = remoteMessage.getData();
            String title = data.get("title");
            String body = data.get("body");
            String detail = data.get("detail");
            String flag = data.get("flag");
            if(!title.equals("") && !body.equals("")){
                sendNotificationData(title, body);
                getJournal(title, body);
            }
        }
    }

    public void getJournal(final String title, final String body)
    {
        ApiService api = ApiClient.getClient().create(ApiService.class);
        final Realm realm = Realm.getDefaultInstance();
        SessionManagement session = CoreApplication.getInstance().getSession();
        Call<GetSalesOrderResponse> call = api.getSalesOrder(session.getKeyUserId(), session.getKeyUserRole());
        call.enqueue(new Callback<GetSalesOrderResponse>() {
            @Override
            public void onResponse(Call<GetSalesOrderResponse> call, final Response<GetSalesOrderResponse> response) {
                if(response.isSuccessful())
                {
                    if(response.body().getResult().equalsIgnoreCase("ok"))
                    {
                        final Realm mRealm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        RealmResults<SalesOrder> journals = realm.where(SalesOrder.class).findAll();
                        journals.deleteAllFromRealm();
                        realm.commitTransaction();
                        final List<SalesOrder> itemMasters = response.body().getDetails();
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(itemMasters);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {

                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<GetSalesOrderResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void sendNotificationData(String messageTitle,String messageBody) {
        Intent resultIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        NotificationCompat.Builder notificationBuilder;
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_DEFAULT);

            // Configure the notification channel.
            notificationChannel.setDescription("POSVIT Notification");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);

            notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.drawable.iconcash)
                    .setContentTitle(messageTitle)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(resultPendingIntent);
            notificationManager.notify(0, notificationBuilder.build());
        }
        else
        {
            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.iconcash)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                            R.drawable.iconcash))
                    .setContentTitle(messageTitle)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(resultPendingIntent);
            NotificationManager notificationManagers =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManagers.notify(0, notificationBuilder.build());
        }
    }
}