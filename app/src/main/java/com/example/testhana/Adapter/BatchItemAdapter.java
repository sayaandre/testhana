package com.example.testhana.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.testhana.Models.Model.BatchItemMasterModel;
import com.example.testhana.R;

import java.util.ArrayList;
import java.util.List;

public class BatchItemAdapter extends RecyclerView.Adapter<BatchItemAdapter.ViewHolder>{

    private final OnItemClickListener listener;
    private List<BatchItemMasterModel> header;
    private List<BatchItemMasterModel> headerOri;
    private SparseBooleanArray selectedItems;
    boolean tabletSize = false;
    Context mContext;
    private int focusedItem = -1;

    public BatchItemAdapter(Context context, List<BatchItemMasterModel> header, OnItemClickListener listener) {
        this.header = header;
        this.listener = listener;
        this.headerOri = new ArrayList<BatchItemMasterModel>(header);
        selectedItems = new SparseBooleanArray(1);
        this.mContext = context;
        tabletSize = mContext.getResources().getBoolean(R.bool.isTablet);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_batch, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.click(header.get(position), listener, position);
        final BatchItemMasterModel album = header.get(position);
        holder.txtName.setText("Ukuran : " + header.get(position).getItemname());
        holder.txtAcc.setText("Batch No. : " + header.get(position).getBatchno());
        holder.txtNo.setText("Qty : " + header.get(position).getQty());
        holder.txtNo.setVisibility(View.GONE);
        if(tabletSize)
        {
            holder.itemView.setSelected(focusedItem == position);
            if(focusedItem == position && focusedItem != -1)
            {
                holder.cardRow.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.material_blue_200));
            }
            else
            {
                holder.cardRow.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            }
        }
    }

    @Override
    public int getItemCount() {
        return header.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtName, txtNo, txtAcc;
        LinearLayout llReport;
        CardView cardRow;

        public ViewHolder(View convertView) {
            super(convertView);

            txtName = convertView.findViewById(R.id.txtDesc);
            txtAcc = convertView.findViewById(R.id.txtName);
            txtNo = convertView.findViewById(R.id.txtPrice);
            llReport = convertView.findViewById(R.id.llItem);
            cardRow = convertView.findViewById(R.id.cardRow);
        }

        public void click(final BatchItemMasterModel dataModel, final OnItemClickListener listener, final int position){
            cardRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notifyItemChanged(getAdapterPosition());
                    focusedItem = getLayoutPosition();
                    notifyItemChanged(getAdapterPosition());
                    notifyDataSetChanged();
                    listener.onClick(dataModel, position);
                }
            });
        }

    }

    public void updateData(List<BatchItemMasterModel> headerList) {
        headerOri.clear();
        headerOri.addAll(headerList);
    }

    public BatchItemMasterModel getItem(int position) {
        if(header.size() > 0)
            return header.get(position);
        return null;
    }

    public interface OnItemClickListener{
        void onClick(BatchItemMasterModel item, int position);
        void onDelete(BatchItemMasterModel item, int position);
    }
}
