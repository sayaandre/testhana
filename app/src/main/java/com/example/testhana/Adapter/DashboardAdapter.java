package com.example.testhana.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.testhana.Helper.Tools;
import com.example.testhana.Models.Model.ItemMasterModel;
import com.example.testhana.Models.Realm.ItemPrice;
import com.example.testhana.R;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder>{

    private final DashboardAdapter.OnItemClickListener listener;
    private Context mContext;
    private List<ItemMasterModel> albumList;
    private Realm realm;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public MyViewHolder(View view) {
            super(view);
        }

        public void click(final ItemMasterModel dataModel, final DashboardAdapter.OnItemClickListener listener){

        }
    }

    public DashboardAdapter(Context mContext, List<ItemMasterModel> albumList, DashboardAdapter.OnItemClickListener listener) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.listener = listener;
        this.realm = Realm.getDefaultInstance();
    }

    @Override
    public DashboardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dashboard, parent, false);

        return new DashboardAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DashboardAdapter.MyViewHolder holder, int position) {
        final ItemMasterModel album = albumList.get(position);
        holder.click(album, listener);
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public interface OnItemClickListener{
        void onClick(ItemMasterModel item);
    }
}
