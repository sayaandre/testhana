package com.example.testhana.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.testhana.Models.Model.SalesOrderModel;
import com.example.testhana.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder>{

    private final OrderAdapter.OnItemClickListener listener;
    private List<SalesOrderModel> header;
    private List<SalesOrderModel> headerOri;
    private SparseBooleanArray selectedItems;
    boolean tabletSize = false;
    Context mContext;
    Realm realm;

    public OrderAdapter(Context context, List<SalesOrderModel> header, OrderAdapter.OnItemClickListener listener) {
        this.header = header;
        this.listener = listener;
        this.headerOri = new ArrayList<SalesOrderModel>(header);
        selectedItems = new SparseBooleanArray(1);
        this.mContext = context;
        tabletSize = mContext.getResources().getBoolean(R.bool.isTablet);
        realm = Realm.getDefaultInstance();
    }

    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order, parent, false);
        OrderAdapter.ViewHolder vh = new OrderAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(OrderAdapter.ViewHolder holder, int position) {
        holder.click(header.get(position), listener);
        holder.trxNo.setText("Receipt : " + header.get(position).getSalesOrderNum());
        Locale current = mContext.getResources().getConfiguration().locale;
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(current);
        DecimalFormat formatter = new DecimalFormat("#,###,###", symbols);
//        holder.total.setText("Status Order : " + header.get(position).getStatus());
        holder.total.setText("Date : " + header.get(position).getTrxDate());
        holder.totalItem.setVisibility(View.VISIBLE);
        Drawable drawable;
        if(header.get(position).getStatus().equals("Receive")){
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.baseline_assignment_late_black_24));
            holder.imgPayType.setImageDrawable(drawable);
        }
        else if(header.get(position).getStatus().equals("Partial Fulfill")){
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.baseline_assignment_returned_black_24));
            holder.imgPayType.setImageDrawable(drawable);
        }
        else if(header.get(position).getStatus().equals("Fulfill")){
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.baseline_assignment_turned_in_black_24));
            holder.imgPayType.setImageDrawable(drawable);
        }
        else if(header.get(position).getStatus().equals("Cancelled")){
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.baseline_delete_black_24));
            holder.imgPayType.setImageDrawable(drawable);
        }
        holder.imgPayType.setVisibility(View.GONE);
        holder.ivOffline.setVisibility(View.GONE);
        holder.txtTime.setGravity(Gravity.CENTER);
        holder.totalItem.setText("Total : " + formatter.format(header.get(position).getTotal()));
        holder.txtTime.setText(header.get(position).getTrxTime().trim().substring(0,5));
        if(tabletSize)
            holder.llHistory.setSelected(selectedItems.get(position, false));
    }

    @Override
    public int getItemCount() {
        return header.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView trxNo;
        TextView total;
        TextView totalItem;
        TextView txtTime;
        ImageView imgPayType;
        LinearLayout llHistory;
        TextView txtRef;
        CardView cardRow;
        ImageView imgIndicator;
        ImageView ivOffline;
        TextView txtInitial;

        public ViewHolder(View convertView) {
            super(convertView);
            trxNo = convertView.findViewById(R.id.txtTrxNo);
            total = convertView.findViewById(R.id.txtTotal);
            totalItem = convertView.findViewById(R.id.txtTotalItem);
            txtTime = convertView.findViewById(R.id.txtTime);
            imgPayType = convertView.findViewById(R.id.imgPayType);
            llHistory = convertView.findViewById(R.id.llHistory);
            txtRef = convertView.findViewById(R.id.txtOVORef);
            cardRow = convertView.findViewById(R.id.cardRow);
            imgIndicator = convertView.findViewById(R.id.indicator);
            ivOffline = convertView.findViewById(R.id.ivOffline);
            txtInitial = convertView.findViewById(R.id.txtInitial);
        }

        public void click(final SalesOrderModel dataModel, final OrderAdapter.OnItemClickListener listener){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int indexLastSelected = selectedItems.indexOfValue(true);
                    int lastSelected = indexLastSelected == -1 ? -1 : selectedItems.keyAt(indexLastSelected);

                    if(lastSelected != -1 && lastSelected != getAdapterPosition()) {
                        selectedItems.put(lastSelected, false);
                        notifyItemChanged(lastSelected);
                        selectedItems.delete(lastSelected);
                    }
                    selectedItems.put(getAdapterPosition(), true);
                    notifyItemChanged(getAdapterPosition());
                    listener.onClick(dataModel, getAdapterPosition());
                }
            });
        }
    }

    public void updateData(List<SalesOrderModel> headerList) {
        headerOri.clear();
        headerOri.addAll(headerList);
    }

    public SalesOrderModel getItem(int position) {
        if(header.size() > 0)
            return header.get(position);
        return null;
    }


    public interface OnItemClickListener{
        void onClick(SalesOrderModel item, int position);
    }
}
