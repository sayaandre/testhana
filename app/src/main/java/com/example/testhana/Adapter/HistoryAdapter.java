package com.example.testhana.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.testhana.Models.Model.JournalModel;
import com.example.testhana.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder>{

    private final OnItemClickListener listener;
    private List<JournalModel> header;
    private List<JournalModel> headerOri;
    private SparseBooleanArray selectedItems;
    boolean tabletSize = false;
    Context mContext;
    Realm realm;

    public HistoryAdapter(Context context, List<JournalModel> header, OnItemClickListener listener) {
        this.header = header;
        this.listener = listener;
        this.headerOri = new ArrayList<JournalModel>(header);
        selectedItems = new SparseBooleanArray(1);
        this.mContext = context;
        tabletSize = mContext.getResources().getBoolean(R.bool.isTablet);
        realm = Realm.getDefaultInstance();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.click(header.get(position), listener);
        holder.trxNo.setText("Receipt : " + header.get(position).getReceiptNumber() + " / " + header.get(position).getDocNum());
        Locale current = mContext.getResources().getConfiguration().locale;
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(current);
        DecimalFormat formatter = new DecimalFormat("#,###,###", symbols);
        holder.total.setText("Rp. " + formatter.format(header.get(position).getTotal()));
        if(header.get(position).getRefNo() != null || header.get(position).getTrxType().equals("Cash")) {
            holder.txtRef.setVisibility(View.GONE);
            holder.totalItem.setVisibility(View.VISIBLE);
        }
        else {
            holder.txtRef.setVisibility(View.GONE); // sementara
            holder.txtRef.setText("Ref. : " + header.get(position).getRefNo());
            holder.totalItem.setVisibility(View.VISIBLE);
        }

        Drawable drawable;
        if(header.get(position).getTrxType().toLowerCase().equals("cash")){
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.iconcash));
            holder.imgPayType.setImageDrawable(drawable);
        } else {
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.iconother));
            holder.imgPayType.setImageDrawable(drawable);
        }

        holder.ivOffline.setVisibility(View.GONE);
        holder.txtTime.setGravity(Gravity.CENTER);


        /*
         * need to use the filter | https://stackoverflow.com/a/30880522/2170109
         * (even if compat should use it for pre-API21-devices | https://stackoverflow.com/a/27812472/2170109)
         */
        // ColorStateList color = ContextCompat.getColorStateList(mContext, R.color.history_icon_color);
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        //DrawableCompat.setTintList(drawable, color);

        /*} else {
            drawable.mutate().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        }*/
        /*if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            holder.imgIndicator.setColorFilter(ContextCompat.getColor(mContext, R.color.jordy_blue));
            holder.imgPayType.setColorFilter(ContextCompat.getColor(mContext, R.color.jordy_blue));
        }*/
        holder.totalItem.setText("Total Item : " + header.get(position).getRowCount());
        holder.txtTime.setText(header.get(position).getTrxTime().trim().substring(0,5));
        if(tabletSize)
            holder.llHistory.setSelected(selectedItems.get(position, false));
        /*if(holder.cardRow.isSelected()) {
            holder.imgIndicator.setVisibility(View.VISIBLE);
        } else {
            holder.imgIndicator.setVisibility(View.GONE);
        }*/
    }

    @Override
    public int getItemCount() {
        return header.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView trxNo;
        TextView total;
        TextView totalItem;
        TextView txtTime;
        ImageView imgPayType;
        LinearLayout llHistory;
        TextView txtRef;
        CardView cardRow;
        ImageView imgIndicator;
        ImageView ivOffline;

        public ViewHolder(View convertView) {
            super(convertView);
            trxNo = convertView.findViewById(R.id.txtTrxNo);
            total = convertView.findViewById(R.id.txtTotal);
            totalItem = convertView.findViewById(R.id.txtTotalItem);
            txtTime = convertView.findViewById(R.id.txtTime);
            imgPayType = convertView.findViewById(R.id.imgPayType);
            llHistory = convertView.findViewById(R.id.llHistory);
            txtRef = convertView.findViewById(R.id.txtOVORef);
            cardRow = convertView.findViewById(R.id.cardRow);
            imgIndicator = convertView.findViewById(R.id.indicator);
            ivOffline = convertView.findViewById(R.id.ivOffline);
        }

        public void click(final JournalModel dataModel, final OnItemClickListener listener){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int indexLastSelected = selectedItems.indexOfValue(true);
                    int lastSelected = indexLastSelected == -1 ? -1 : selectedItems.keyAt(indexLastSelected);

                    if(lastSelected != -1 && lastSelected != getAdapterPosition()) {
                        selectedItems.put(lastSelected, false);
                        notifyItemChanged(lastSelected);
                        selectedItems.delete(lastSelected);
                    }
                    selectedItems.put(getAdapterPosition(), true);
                    notifyItemChanged(getAdapterPosition());
                    listener.onClick(dataModel, getAdapterPosition());
                }
            });
        }
    }

    public void updateData(List<JournalModel> headerList) {
        headerOri.clear();
        headerOri.addAll(headerList);
    }

    public JournalModel getItem(int position) {
        if(header.size() > 0)
            return header.get(position);
        return null;
    }


    public interface OnItemClickListener{
        void onClick(JournalModel item, int position);
    }
}
