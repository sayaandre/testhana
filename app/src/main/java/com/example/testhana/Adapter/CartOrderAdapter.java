package com.example.testhana.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.testhana.Helper.Constant;
import com.example.testhana.Models.Model.CartOrderModel;
import com.example.testhana.Models.Model.Discount;
import com.example.testhana.Models.Realm.Cart;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.Models.Realm.ItemPrice;
import com.example.testhana.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class CartOrderAdapter extends RecyclerView.Adapter<CartOrderAdapter.MyViewHolder>{

    private final CartOrderAdapter.OnItemClickListener listener;
    private List<CartOrderModel> cartsList;
    Context mContext;
    Realm realm;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView skuid, qty, desc, price, txtInitial, product, txtgivespace, txtNotes, finalprice, txtStatus, txtTextAction;
        public EditText txtAction;
        public ImageButton imgDelete;
        public LinearLayout llCart, llDiscount, llDiscountP, llAction, llBtnAction, llStokDreamland, llStokBMPT,
                llETA, llIncomingPreOrder;
        public ImageView imgItem;
        public RelativeLayout viewBackground, viewForeground;
        public Button btnAction;

        public MyViewHolder(View view) {
            super(view);
            skuid = view.findViewById(R.id.txtSKUId);
            product = view.findViewById(R.id.txtProductName);
            txtgivespace = view.findViewById(R.id.txtGiveSpace);
            qty = view.findViewById(R.id.txtQty);
            desc = view.findViewById(R.id.txtPLUDesc);
            price = view.findViewById(R.id.txtPrice);
            imgDelete = view.findViewById(R.id.imgDelete);
            llCart = view.findViewById(R.id.llCart);
            txtInitial = view.findViewById(R.id.txtInitial);
            imgItem = view.findViewById(R.id.imgItem);
            llDiscount = view.findViewById(R.id.layoutDiscount);
            llDiscountP = view.findViewById(R.id.layoutDiscountProduct);
            llAction = view.findViewById(R.id.llAction);
            llBtnAction = view.findViewById(R.id.llBtnAction);
            txtNotes = view.findViewById(R.id.txtNotes);
            finalprice = view.findViewById(R.id.txtFinalPrice);
            txtStatus = view.findViewById(R.id.txtStatus);
            txtAction = view.findViewById(R.id.txtAction);
            btnAction = view.findViewById(R.id.btnAction);
            viewBackground = view.findViewById(R.id.llBackground);
            viewForeground = view.findViewById(R.id.llForeground);
            txtTextAction = view.findViewById(R.id.txtTextAction);
            llStokDreamland = view.findViewById(R.id.llStokDreamland);
            llStokBMPT = view.findViewById(R.id.llStokBMPT);
            llIncomingPreOrder = view.findViewById(R.id.llIncomingPreOrder);
            llETA = view.findViewById(R.id.llETA);
        }
        public void click(final CartOrderModel cartModel, final CartOrderAdapter.OnItemClickListener listener){
            llCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(cartModel);
                }
            });

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDeleteClick(cartModel);
                }
            });

            btnAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onChangeAction(cartModel);
                }
            });
        }
    }

    public CartOrderAdapter(Context context, List<CartOrderModel> cartsList, CartOrderAdapter.OnItemClickListener listener) {
        this.cartsList = cartsList;
        this.listener = listener;
        this.mContext = context;
        realm = Realm.getDefaultInstance();
    }

    @Override
    public CartOrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cart, parent, false);

        return new CartOrderAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CartOrderAdapter.MyViewHolder holder, int position) {
        CartOrderModel cart = cartsList.get(position);
        Locale current = mContext.getResources().getConfiguration().locale;
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(current);
        DecimalFormat formatter = new DecimalFormat("#,###,###", symbols);
        // Cart cart = cart;
        holder.click(cart, listener);
        //RealmResults<ProductModel> variantModels = mrealm.where(ProductModel.class).equalTo("product_id",productModel.getProduct_id()).findAll();
        if(cart.getItemDesc().contains("Discount"))
            holder.qty.setVisibility(View.GONE);
        String[] namepart = cart.getItemDesc().split("\\|");
        String desc = "";
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
                holder.desc.getLayoutParams();
        holder.txtgivespace.setVisibility(View.GONE);
        holder.product.setVisibility(View.VISIBLE);
        holder.product.setText(namepart[0]);
        holder.desc.setText(Html.fromHtml("<b>" + namepart[0] + "</b>"));
        params.weight = 0.45f;
        //holder.desc.setTextSize(mContext.getResources().getDimension(R.dimen.font_normal));
        desc = namepart[0];
        holder.desc.setLayoutParams(params);
        ItemMaster productModels = realm.where(ItemMaster.class)
                .equalTo("itemNo", cart.getItemId())
                .findFirst();
        holder.skuid.setVisibility(View.GONE);
        holder.qty.setText(" x " + String.valueOf(cart.getItemQty()));
        RealmResults<ItemPrice> itemPrices = realm.where(ItemPrice.class).equalTo("itemId", cart.getItemId()).findAll();
        if(itemPrices.size() >= 1)
        {
            holder.price.setText(formatter.format(itemPrices.first().getPrice()));
        }
        holder.txtNotes.setTypeface(holder.txtNotes.getTypeface(), Typeface.ITALIC);
        if(cart.getRemarks() != null && !cart.getRemarks().isEmpty()) {
            holder.txtNotes.setText(cart.getRemarks());
            holder.txtNotes.setVisibility(View.VISIBLE);
        } else {
            holder.txtNotes.setVisibility(View.GONE);
        }
        if(position == 0 || position == 1)
        {
            holder.txtStatus.setText("Available");
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, R.color.material_green_500));
            holder.llBtnAction.setVisibility(View.GONE);
            holder.llStokDreamland.setVisibility(View.GONE);
            holder.llIncomingPreOrder.setVisibility(View.GONE);
            holder.llETA.setVisibility(View.GONE);
            if(position == 1)
            {
                holder.llStokBMPT.setVisibility(View.GONE);
            }
        }
        else if(position == 2)
        {
            holder.txtStatus.setText("Not Available");
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, R.color.material_red_500));
            holder.llBtnAction.setVisibility(View.GONE);
            holder.txtAction.setText("2");
            holder.txtTextAction.setText("Request Transfer");
            holder.llAction.setVisibility(View.VISIBLE);
            holder.llStokDreamland.setVisibility(View.VISIBLE);
            holder.llIncomingPreOrder.setVisibility(View.GONE);
            holder.llETA.setVisibility(View.GONE);
            holder.llStokBMPT.setVisibility(View.GONE);
        }
        else
        {
            holder.txtStatus.setText("Not Available");
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, R.color.material_red_500));
            holder.llBtnAction.setVisibility(View.GONE);
            holder.txtAction.setText("3");
            holder.txtTextAction.setText("PreOrder");
            holder.llAction.setVisibility(View.VISIBLE);
            holder.llStokDreamland.setVisibility(View.GONE);
            holder.llIncomingPreOrder.setVisibility(View.VISIBLE);
            holder.llETA.setVisibility(View.VISIBLE);
            holder.llStokBMPT.setVisibility(View.GONE);
        }
        holder.txtInitial.setText(desc.length() > 2 ? desc.substring(0,2) : desc);
        holder.llDiscount.removeAllViews();
        List<Discount> discount = cart.getDiscounts();
        double fprice = cart.getTotal();
        for(int i = 0; i<discount.size();i++) {
            fprice = fprice - discount.get(i).getAmount();
            View view = LayoutInflater.from(mContext).inflate(R.layout.discount_list_row,holder.llDiscount, false);
            ((TextView) view.findViewById(R.id.txtPromoDesc)).setText(discount.get(i).getPromo_name());
            ((TextView) view.findViewById(R.id.txtDiscAmt)).setText("-"+formatter.format(discount.get(i).getAmount()));
            int affectedItem = discount.get(i).getAffectedItem();
            ((TextView) view.findViewById(R.id.txtDiscPerc)).setText((discount.get(i).getPromo_code() == Constant.ITEM_PERCENTAGE || discount.get(i).getPromo_code() == Constant.GRP_PERCENTAGE ?
                    (String.format(Locale.US, "%.1f", discount.get(i).getPercentage())+"%") : "(-)") + (affectedItem > 0 ? "(" + affectedItem + ") " : ""));
            if(discount.get(i).getPromo_code() > 0 && discount.get(i).getPercentage() != 0)
                ((TextView) view.findViewById(R.id.txtDiscPerc)).setText(((int) discount.get(i).getPercentage())+"%");
            holder.llDiscount.addView(view);
        }
        holder.finalprice.setText(formatter.format(fprice < 0 ? 0 : fprice));
        if(holder.itemView.getContext().getClass().toString().contains("PaymentActivity")) {
            holder.imgDelete.setVisibility(View.GONE);
        }
    }

    public void removeItem(int position) {
        CartOrderModel item = cartsList.get(position);
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Cart> carts = realm.where(Cart.class)
                .beginGroup()
                .equalTo("linkRowNo", item.getRowNo())
                .or()
                .equalTo("row_no", item.getRowNo())
                .endGroup()
                .findAll();
        realm.beginTransaction();
        carts.deleteAllFromRealm();
        realm.commitTransaction();
        cartsList.remove(position);
        notifyItemRemoved(position);
    }

    public void removeLayerItem(int position) {
        CartOrderModel item = cartsList.get(position);
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Cart> carts = realm.where(Cart.class)
                .beginGroup()
                .equalTo("linkRowNo", item.getRowNo())
                .or()
                .equalTo("row_no", item.getRowNo())
                .endGroup()
                .findAll().sort("row_no", Sort.DESCENDING);
        realm.beginTransaction();
        carts.first().deleteFromRealm();
        realm.commitTransaction();
//        cartsList.remove(position);
//        notifyItemRemoved(position);
    }

    public void restoreItem(CartOrderModel item, int position) {
        cartsList.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    @Override
    public int getItemCount() {
        return cartsList.size();
    }

    public interface OnItemClickListener{
        void onClick(CartOrderModel item);
        void onDeleteClick(CartOrderModel item);
        void onChangeAction(CartOrderModel item);
    }
}
