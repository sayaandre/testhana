package com.example.testhana.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.testhana.Helper.Tools;
import com.example.testhana.Models.Model.ItemMasterModel;
import com.example.testhana.Models.Realm.ItemPrice;
import com.example.testhana.R;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class ItemMasterAdapter extends RecyclerView.Adapter<ItemMasterAdapter.MyViewHolder>{

    private final OnItemClickListener listener;
    private Context mContext;
    private List<ItemMasterModel> albumList;
    private Realm realm;
    boolean tabletSize = false;
    private SparseBooleanArray selectedItems;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtItemName, txtItemPrice, txtItemCode;
        public ImageView ivImageItem, overflow;
        public TextView txtInitial;
        CardView cardRow;
        LinearLayout llItemMaster;
        ImageButton ivDelete;

        public MyViewHolder(View view) {
            super(view);
            txtItemName = (TextView) view.findViewById(R.id.txtItemName);
            txtItemPrice = (TextView) view.findViewById(R.id.txtItemPrice);
            txtItemCode = view.findViewById(R.id.txtItemCode);
            ivImageItem = (ImageView) view.findViewById(R.id.ivImageItem);
            txtInitial = view.findViewById(R.id.txtInitial);
            cardRow = view.findViewById(R.id.cardRow);
            llItemMaster = view.findViewById(R.id.llItemMaster);
            ivDelete = view.findViewById(R.id.ivDelete);
        }

        public void click(final ItemMasterModel dataModel, final OnItemClickListener listener, final int position){
            cardRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int indexLastSelected = selectedItems.indexOfValue(true);
                    int lastSelected = indexLastSelected == -1 ? -1 : selectedItems.keyAt(indexLastSelected);

                    if(lastSelected != -1 && lastSelected != getAdapterPosition()) {
                        selectedItems.put(lastSelected, false);
                        notifyItemChanged(lastSelected);
                        selectedItems.delete(lastSelected);
                    }
                    selectedItems.put(getAdapterPosition(), true);
                    notifyItemChanged(getAdapterPosition());
                    listener.onClick(dataModel);
                }
            });
            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDelete(dataModel, position);
                }
            });
        }
    }

    public ItemMasterAdapter(Context mContext, List<ItemMasterModel> albumList, OnItemClickListener listener) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.listener = listener;
        this.realm = Realm.getDefaultInstance();
        selectedItems = new SparseBooleanArray(1);
        tabletSize = mContext.getResources().getBoolean(R.bool.isTablet);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_item_master, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final ItemMasterModel album = albumList.get(position);
        holder.click(album, listener, position);
        holder.txtItemCode.setText(album.getItemNo());
        holder.txtItemName.setText(album.getItemDesc());
        RealmResults<ItemPrice> itemPrices = realm.where(ItemPrice.class).equalTo("itemId", album.getItemNo()).findAll();
        holder.txtItemPrice.setText(itemPrices.size() >= 1 ?
                Tools.convertMoney(mContext, itemPrices.first().getPrice()) + " " + itemPrices.first().getCurrency():
                itemPrices.size() + " Prices");
        Drawable drawable;
        if(position == 0)
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.katrol));
        else if(position == 1)
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.cone));
        else if(position == 2)
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.sarungtangan));
        else if(position == 3)
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.sepatuboots));
        else if(position == 4)
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.kacamata));
        else if(position == 5)
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.rompi));
        else if(position == 6)
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.tali));
        else
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.helmet));
        holder.ivImageItem.setImageDrawable(drawable);
        holder.ivImageItem.setVisibility(View.VISIBLE);

//        if(album.getItemImage() == null || (album.getItemImage() != null && album.getItemImage().equals(""))) {
//            holder.txtInitial.setVisibility(View.VISIBLE);
//            holder.ivImageItem.setVisibility(View.GONE);
//        } else {
//            holder.ivImageItem.setVisibility(View.VISIBLE);
//            holder.txtInitial.setVisibility(View.GONE);
//            Picasso.with(mContext)
//                    .load(R.drawable.placeholder) // thumbnail url goes here
//                    .placeholder(R.drawable.placeholder)
//                    .into(holder.ivImageItem, new com.squareup.picasso.Callback() {
//                        @Override
//                        public void onSuccess() {
//                            Picasso.with(mContext)
//                                    .load(album.getItemImage())
//                                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                                    .placeholder(holder.ivImageItem.getDrawable())
//                                    .into(holder.ivImageItem);
//
//                            Picasso.with(mContext)
//                                    .invalidate(album.getItemImage());
//                        }
//
//                        @Override
//                        public void onError() {
//                        }
//                    });
//            String base64String = album.getItemImage();
//            String base64Image = base64String;
//
//            byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
//            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//
//            holder.ivImageItem.setImageBitmap(decodedByte);
//        }
        holder.txtInitial.setVisibility(View.GONE);
        String skuName = album.getItemDesc();
        if(skuName != null) {
            holder.txtInitial.setText(skuName.length() > 2 ? skuName.substring(0, 2) : skuName);
        }
        if(tabletSize)
            holder.llItemMaster.setSelected(selectedItems.get(position, false));
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public interface OnItemClickListener{
        void onClick(ItemMasterModel item);
        void onDelete(ItemMasterModel item, int position);
    }

    public void updateData(List<ItemMasterModel> headerList) {
        albumList.clear();
        albumList.addAll(headerList);
    }

}
