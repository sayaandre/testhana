package com.example.testhana.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.testhana.Models.Model.SalesOrderDetailModel;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.MyViewHolder> {

    private List<SalesOrderDetailModel> journalList;
    Context mContext;
    Realm realm;
    private final OrderDetailAdapter.OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView skuid, qty, desc, price, disc_perc, disc_amt, txtInitial, txtproduct, txtgivespace, txtNotes, finalprice,
                txtBatchNumber, txtTotal;
        public ImageButton imgDelete;
        public LinearLayout llCart, llDiscount;

        public MyViewHolder(View view) {
            super(view);
            skuid = view.findViewById(R.id.txtSKUId);
            txtgivespace = view.findViewById(R.id.txtGiveSpace);
            txtproduct = view.findViewById(R.id.txtProductName);
            qty = view.findViewById(R.id.txtQty);
            desc = view.findViewById(R.id.txtPLUDesc);
            price = view.findViewById(R.id.txtPrice);
            disc_perc = view.findViewById(R.id.txtDiscPerc);
            disc_amt= view.findViewById(R.id.txtDiscAmt);
            imgDelete = view.findViewById(R.id.imgDelete);
            llCart = view.findViewById(R.id.llCart);
            txtInitial = view.findViewById(R.id.txtInitial);
            llDiscount = view.findViewById(R.id.layoutDiscount);
            txtNotes = view.findViewById(R.id.txtNotes);
            finalprice = view.findViewById(R.id.txtFinalPrice);
            txtBatchNumber = view.findViewById(R.id.txtBatchNumber);
            txtTotal = view.findViewById(R.id.txtTotal);
        }
        public void click(final SalesOrderDetailModel cartModel, final OrderDetailAdapter.OnItemClickListener listener){
            txtBatchNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickDetail(cartModel);
                }
            });
        }
    }


    public OrderDetailAdapter(Context context, List<SalesOrderDetailModel> journal, OrderDetailAdapter.OnItemClickListener listener) {
        this.journalList = journal;
        this.mContext = context;
        realm = Realm.getDefaultInstance();
        this.listener = listener;
    }

    @Override
    public OrderDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order_detail, parent, false);

        return new OrderDetailAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OrderDetailAdapter.MyViewHolder holder, int position) {
        SalesOrderDetailModel journal = journalList.get(position);
        holder.click(journal, listener);
        Locale current = mContext.getResources().getConfiguration().locale;
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(current);
        DecimalFormat formatter = new DecimalFormat("#,###,###", symbols);
        // Cart cart = cartsList.get(position);
        holder.imgDelete.setVisibility(View.GONE);
        if(journal.getItemName().contains("Discount"))
            holder.qty.setVisibility(View.GONE);
        holder.qty.setText(" x " + String.valueOf((int) journal.getItemQty()));
        String desc = "";
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
                holder.desc.getLayoutParams();
        holder.txtgivespace.setVisibility(View.GONE);
        holder.txtproduct.setVisibility(View.GONE);
        holder.desc.setText(Html.fromHtml("<b>" + journal.getItemName() + (journal.getItemBatch().equals("") ? "" : (" - " + journal.getItemBatch())) + "</b>"));
        params.weight = 0.5f;
        desc = journal.getItemName();
        holder.desc.setLayoutParams(params);
        ItemMaster productModels = realm.where(ItemMaster.class)
                .equalTo("itemNo", journal.getItemId())
                .findFirst();
        if(productModels != null)
        {
            holder.skuid.setText(productModels.getItemNo());
        }
        holder.txtNotes.setVisibility(View.VISIBLE);
        holder.txtNotes.setText("Ukuran kisaran 80m-90m");
        holder.txtBatchNumber.setVisibility(View.GONE);
        holder.txtTotal.setVisibility(View.GONE);
        if(position == 0)
        {
            holder.txtNotes.setText("Warna Merah");
            holder.txtBatchNumber.setText("04072019 - 87m\n03072019 - 105m\n02072019 - 99m");
            holder.price.setText("50.000");
            holder.txtTotal.setText("Total Meter : 291" + "\n" + "Total : 1.400.000");
        }
        else
        {
            holder.txtNotes.setText("Ukuran lebih dari 100m");
            holder.txtNotes.setVisibility(View.GONE);
            holder.txtBatchNumber.setText("04022019 - 84m");
            holder.price.setText("95.000");
            holder.txtTotal.setText("Total Meter : 84" + "\n" + "Total Amount : 600.000");
        }

        //holder.desc.setText(journal.getVRDesc());


        boolean tabletSize = mContext.getResources().getBoolean(R.bool.isTablet);
        if(!tabletSize) {
            holder.txtInitial.setVisibility(View.GONE);
        }

        holder.txtNotes.setTypeface(holder.txtNotes.getTypeface(), Typeface.ITALIC);
        if(journal.getNotes() != null && !journal.getNotes().isEmpty()) {
            holder.txtNotes.setText(journal.getNotes());
            holder.txtNotes.setVisibility(View.VISIBLE);
        } else {
//            holder.txtNotes.setVisibility(View.GONE);
        }

        holder.txtInitial.setText(desc.length() > 2 ?
                desc.substring(0,2) : desc);

        holder.llDiscount.removeAllViews();
//            List<Discount> discount = journal.get();
//            for(int i = 0; i<discount.size();i++) {
//                fprice = fprice - discount.get(i).getAmount();
//                View view = LayoutInflater.from(mContext).inflate(R.layout.discount_list_row,holder.llDiscount, false);
//                ((TextView) view.findViewById(R.id.txtPromoDesc)).setText(discount.get(i).getPromo_name());
//                ((TextView) view.findViewById(R.id.txtDiscAmt)).setText(formatter.format(discount.get(i).getAmount() * -1));
//                int affectedItem = discount.get(i).getAffectedItem();
//                ((TextView) view.findViewById(R.id.txtDiscPerc)).setText((affectedItem > 0 ? "(" + affectedItem + ") " : "") + String.format(Locale.US, "%.1f", discount.get(i).getPercentage())+"%");
//                holder.llDiscount.addView(view);
//            }
        holder.finalprice.setText("");
    }

    @Override
    public int getItemCount() {
        return journalList.size();
    }

    public interface OnItemClickListener{
        void onClickDetail(SalesOrderDetailModel item);
    }
}
