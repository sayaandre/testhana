package com.example.testhana.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.testhana.Helper.Tools;
import com.example.testhana.Models.Model.ItemMasterModel;
import com.example.testhana.Models.Model.LocationCheckInModel;
import com.example.testhana.Models.Realm.ItemPrice;
import com.example.testhana.R;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.MyViewHolder>{

    private final LocationAdapter.OnItemClickListener listener;
    private Context mContext;
    private List<LocationCheckInModel> albumList;
    private Realm realm;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtLocationName, txtLocationAddress;
        LinearLayout llLocation;
        ImageView ivStatus;

        public MyViewHolder(View view) {
            super(view);
            txtLocationName = (TextView) view.findViewById(R.id.txtLocationName);
            txtLocationAddress = (TextView) view.findViewById(R.id.txtLocationAddress);
            llLocation = view.findViewById(R.id.llLocation);
            ivStatus = view.findViewById(R.id.ivStatus);
        }

        public void click(final LocationCheckInModel dataModel, final LocationAdapter.OnItemClickListener listener, final int position){
            llLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(dataModel);
                }
            });
        }
    }

    public LocationAdapter(Context mContext, List<LocationCheckInModel> albumList, LocationAdapter.OnItemClickListener listener) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.listener = listener;
        this.realm = Realm.getDefaultInstance();
    }

    @Override
    public LocationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_location, parent, false);

        return new LocationAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final LocationAdapter.MyViewHolder holder, int position) {
        final LocationCheckInModel album = albumList.get(position);
        holder.click(album, listener, position);
        holder.txtLocationName.setText(album.getLocationName());
        holder.txtLocationAddress.setText(album.getLocationAddress());
        Drawable drawable;
        if(album.isCheckIn())
        {
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.ic_baseline_done_all));
            holder.ivStatus.setImageDrawable(drawable);
        }
        else
        {
            drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.ic_baseline_info_red));
            holder.ivStatus.setImageDrawable(drawable);
        }
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public interface OnItemClickListener{
        void onClick(LocationCheckInModel item);
    }
}
