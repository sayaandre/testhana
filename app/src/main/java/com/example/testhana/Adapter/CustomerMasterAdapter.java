package com.example.testhana.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.testhana.Models.Model.CustomerMasterModel;
import com.example.testhana.R;

import java.util.ArrayList;
import java.util.List;

public class CustomerMasterAdapter extends RecyclerView.Adapter<CustomerMasterAdapter.ViewHolder>{
    private final CustomerMasterAdapter.OnItemClickListener listener;
    private List<CustomerMasterModel> header;
    private List<CustomerMasterModel> headerOri;
    private SparseBooleanArray selectedItems;
    boolean tabletSize = false;
    Context mContext;
    private int focusedItem = -1;

    public CustomerMasterAdapter(Context context, List<CustomerMasterModel> header, CustomerMasterAdapter.OnItemClickListener listener) {
        this.header = header;
        this.listener = listener;
        this.headerOri = new ArrayList<CustomerMasterModel>(header);
        selectedItems = new SparseBooleanArray(1);
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_customer_master, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.click(header.get(position), listener, position);
        final CustomerMasterModel album = header.get(position);
        holder.txtName.setText(header.get(position).getCustomerName());
        holder.txtAcc.setText("Phone : " + header.get(position).getCustomerPhone());
        holder.txtNo.setText("Code : " + header.get(position).getCustomerCode());
        if(album.getCustomerImage() == null || (album.getCustomerImage() != null && album.getCustomerImage().equals("")))
        {
            holder.txtInitial.setVisibility(View.VISIBLE);
            holder.ivCustomerImg.setVisibility(View.GONE);
        }
        else
        {
            holder.ivCustomerImg.setVisibility(View.VISIBLE);
            holder.txtInitial.setVisibility(View.GONE);
            String base64String = album.getCustomerImage();
            String base64Image = base64String;

            byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            holder.ivCustomerImg.setImageBitmap(decodedByte);
        }
        String skuName = album.getCustomerName();
        if(skuName != null) {
            holder.txtInitial.setText(skuName.length() > 2 ? skuName.substring(0, 2) : skuName);
        }
        if(tabletSize)
        {
            holder.itemView.setSelected(focusedItem == position);
            if(focusedItem == position && focusedItem != -1)
            {
                holder.cardRow.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.material_blue_200));
            }
            else
            {
                holder.cardRow.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            }
        }
    }

    @Override
    public int getItemCount() {
        return header.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtName, txtNo, txtAcc;
        LinearLayout llReport;
        CardView cardRow;
        TextView txtInitial;
        ImageView ivCustomerImg;
        ImageButton ivDelete;

        public ViewHolder(View convertView) {
            super(convertView);

            txtName = convertView.findViewById(R.id.txtDesc);
            txtAcc = convertView.findViewById(R.id.txtName);
            txtNo = convertView.findViewById(R.id.txtPrice);
            txtInitial = convertView.findViewById(R.id.txtInitial);
            llReport = convertView.findViewById(R.id.llItem);
            cardRow = convertView.findViewById(R.id.cardRow);
            ivCustomerImg = convertView.findViewById(R.id.ivImageCustomer);
            ivDelete = convertView.findViewById(R.id.ivDelete);
        }

        public void click(final CustomerMasterModel dataModel, final OnItemClickListener listener, final int position){
            cardRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notifyItemChanged(getAdapterPosition());
                    focusedItem = getLayoutPosition();
                    notifyItemChanged(getAdapterPosition());
                    notifyDataSetChanged();
                    listener.onClick(dataModel, position);
                }
            });
            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDelete(dataModel, position);
                }
            });
        }

    }

    public void updateData(List<CustomerMasterModel> headerList) {
        headerOri.clear();
        headerOri.addAll(headerList);
    }

    public CustomerMasterModel getItem(int position) {
        if(header.size() > 0)
            return header.get(position);
        return null;
    }

    public interface OnItemClickListener{
        void onClick(CustomerMasterModel item, int position);
        void onDelete(CustomerMasterModel item, int position);
    }
}
