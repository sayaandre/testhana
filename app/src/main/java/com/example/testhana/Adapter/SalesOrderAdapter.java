package com.example.testhana.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.testhana.Helper.Tools;
import com.example.testhana.Models.Model.ItemMasterModel;
import com.example.testhana.Models.Realm.ItemPrice;
import com.example.testhana.R;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class SalesOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final SalesOrderAdapter.OnItemClickListener listener;
    private Context mContext;
    private List<ItemMasterModel> albumList;
    private Realm realm;
    private boolean isLoadingAdded = false;
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtItemName, txtItemPrice;
        public ImageView ivImageItem, overflow;
        public TextView txtInitial;
        CardView cardRow;
        LinearLayout llItemMaster;

        public MyViewHolder(View view) {
            super(view);
            txtItemName = (TextView) view.findViewById(R.id.txtItemName);
            txtItemPrice = (TextView) view.findViewById(R.id.txtItemPrice);
            ivImageItem = (ImageView) view.findViewById(R.id.ivImageItem);
            txtInitial = view.findViewById(R.id.txtInitial);
            cardRow = view.findViewById(R.id.card_view);
            llItemMaster = view.findViewById(R.id.llItemMaster);
        }

        public void click(final ItemMasterModel dataModel, final SalesOrderAdapter.OnItemClickListener listener){
            cardRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(dataModel);
                }
            });
        }
    }

    public SalesOrderAdapter(Context mContext, List<ItemMasterModel> albumList, SalesOrderAdapter.OnItemClickListener listener) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.listener = listener;
        this.realm = Realm.getDefaultInstance();
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {
        public LoadingVH(View itemView) {
            super(itemView);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (viewType) {
            case ITEM:
                view = inflater.inflate(R.layout.item_sales_order, parent, false);
                viewHolder = new SalesOrderAdapter.MyViewHolder(view);
                break;
            case LOADING:
                view = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new SalesOrderAdapter.LoadingVH(view);
                break;
        }
        return viewHolder;
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.item_sales_order, parent, false);
//
//        return new SalesOrderAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final ItemMasterModel album = albumList.get(position);
        switch (getItemViewType(position))
        {
            case ITEM:
                final SalesOrderAdapter.MyViewHolder movieVH = (SalesOrderAdapter.MyViewHolder) holder;
                movieVH.click(album, listener);
                movieVH.txtItemName.setText(album.getItemDesc());
                RealmResults<ItemPrice> itemPrices = realm.where(ItemPrice.class).equalTo("itemId", album.getItemNo()).findAll();
                movieVH.txtItemPrice.setText(itemPrices.size() >= 1 ?
                        ("Price : " + Tools.convertMoney(mContext, itemPrices.first().getPrice())):
                        itemPrices.size() + " Prices");
//                if(album.getItemImage() == null || (album.getItemImage() != null && album.getItemImage().equals(""))) {
//                    movieVH.txtInitial.setVisibility(View.GONE);
//                    movieVH.ivImageItem.setVisibility(View.GONE);
//                } else {
//                    movieVH.ivImageItem.setVisibility(View.GONE);
//                    movieVH.txtInitial.setVisibility(View.GONE);
//                    String base64String = album.getItemImage();
//                    String base64Image = base64String;
//
//                    byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
//                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//
//                    movieVH.ivImageItem.setImageBitmap(decodedByte);
//                }
                Drawable drawable;
                if(position == 0)
                    drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.katrol));
                else if(position == 1)
                    drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.cone));
                else if(position == 2)
                    drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.sarungtangan));
                else if(position == 3)
                    drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.sepatuboots));
                else if(position == 4)
                    drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.kacamata));
                else if(position == 5)
                    drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.rompi));
                else if(position == 6)
                    drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.tali));
                else
                    drawable = DrawableCompat.wrap(ContextCompat.getDrawable(mContext, R.drawable.helmet));
                movieVH.ivImageItem.setImageDrawable(drawable);
                String skuName = album.getItemDesc();
                if(skuName != null) {
                    movieVH.txtInitial.setText(skuName.length() > 2 ? skuName.substring(0, 2) : skuName);
                }
                break;
            case LOADING:
                break;
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ItemMasterModel());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = albumList.size() - 1;
        ItemMasterModel result = getItem(position);

        if (result != null) {
            albumList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public ItemMasterModel getItem(int position) {
        return albumList.get(position);
    }

    public void add(ItemMasterModel mc) {
        albumList.add(mc);
        notifyItemInserted(albumList.size() - 1);
    }

    @Override
    public int getItemViewType(int position) {
        return (position == albumList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public interface OnItemClickListener{
        void onClick(ItemMasterModel item);
    }
}
