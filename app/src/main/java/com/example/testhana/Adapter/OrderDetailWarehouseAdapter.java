package com.example.testhana.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.testhana.Models.Model.SalesOrderDetailModel;
import com.example.testhana.Models.Realm.ItemMaster;
import com.example.testhana.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;

public class OrderDetailWarehouseAdapter extends RecyclerView.Adapter<OrderDetailWarehouseAdapter.MyViewHolder> {

    private List<SalesOrderDetailModel> journalList;
    Context mContext;
    Realm realm;
    private final OrderDetailWarehouseAdapter.OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView skuid, qty, desc, disc_perc, disc_amt, txtInitial, txtproduct, txtgivespace, txtNotes, txtBatchNumber;
        public ImageButton imgDelete;
        public LinearLayout llCart, llDiscount;
        public Button btnChangeStatus;

        public MyViewHolder(View view) {
            super(view);
            skuid = view.findViewById(R.id.txtSKUId);
            txtgivespace = view.findViewById(R.id.txtGiveSpace);
            txtproduct = view.findViewById(R.id.txtProductName);
            qty = view.findViewById(R.id.txtQty);
            desc = view.findViewById(R.id.txtPLUDesc);
            disc_perc = view.findViewById(R.id.txtDiscPerc);
            disc_amt= view.findViewById(R.id.txtDiscAmt);
            imgDelete = view.findViewById(R.id.imgDelete);
            llCart = view.findViewById(R.id.llCart);
            txtInitial = view.findViewById(R.id.txtInitial);
            llDiscount = view.findViewById(R.id.layoutDiscount);
            txtNotes = view.findViewById(R.id.txtNotes);
            btnChangeStatus = view.findViewById(R.id.btnChangeStatus);
            txtBatchNumber = view.findViewById(R.id.txtBatchNumber);
        }
        public void click(final SalesOrderDetailModel cartModel, final OrderDetailWarehouseAdapter.OnItemClickListener listener){
            btnChangeStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(cartModel);
                }
            });

            txtBatchNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickDetail(cartModel);
                }
            });
        }
    }


    public OrderDetailWarehouseAdapter(Context context, List<SalesOrderDetailModel> journal, OrderDetailWarehouseAdapter.OnItemClickListener listener) {
        this.journalList = journal;
        this.mContext = context;
        this.listener = listener;
        realm = Realm.getDefaultInstance();
    }

    @Override
    public OrderDetailWarehouseAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order_detail_warehouse, parent, false);

        return new OrderDetailWarehouseAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OrderDetailWarehouseAdapter.MyViewHolder holder, int position) {
        SalesOrderDetailModel journal = journalList.get(position);
        holder.click(journal, listener);
        Locale current = mContext.getResources().getConfiguration().locale;
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(current);
        DecimalFormat formatter = new DecimalFormat("#,###,###", symbols);
        // Cart cart = cartsList.get(position);
        holder.imgDelete.setVisibility(View.GONE);
        if(journal.getItemName().contains("Discount"))
            holder.qty.setVisibility(View.GONE);
        holder.qty.setText(" x " + String.valueOf((int) journal.getItemQty()));
        String desc = "";
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
                holder.desc.getLayoutParams();
        holder.txtgivespace.setVisibility(View.GONE);
        holder.txtproduct.setVisibility(View.GONE);
        holder.desc.setText(Html.fromHtml("<b>" + journal.getItemName() + (journal.getItemBatch().equals("") ? "" : (" - " + journal.getItemBatch())) + "</b>"));
        params.weight = 0.5f;
        desc = journal.getItemName();
        holder.desc.setLayoutParams(params);
        ItemMaster productModels = realm.where(ItemMaster.class)
                .equalTo("itemNo", journal.getItemId())
                .findFirst();
        if(productModels != null)
        {
            holder.skuid.setText(productModels.getItemNo());
        }
//        holder.txtBatchNumber.setVisibility(View.GONE);
//        holder.txtNotes.setVisibility(View.GONE);
        if(position == 0)
        {
            holder.txtNotes.setText("Ukuran kisaran 80m-90m");
            holder.txtBatchNumber.setText("04072019 - 87m\n03072019 - 105m\n02072019 - 99m");
        }
        else
        {
            holder.txtNotes.setText("Ukuran lebih dari 100m");
            holder.txtBatchNumber.setText("04022019 - 84m");
        }
        //holder.desc.setText(journal.getVRDesc());

        if(journal.getStatus().equals("FulFill"))
        {
            holder.btnChangeStatus.setEnabled(false);
        }
        else
        {
            holder.btnChangeStatus.setEnabled(true);
        }

        boolean tabletSize = mContext.getResources().getBoolean(R.bool.isTablet);
        if(!tabletSize) {
            holder.txtInitial.setVisibility(View.GONE);
        }

        holder.txtNotes.setTypeface(holder.txtNotes.getTypeface(), Typeface.ITALIC);
        if(journal.getNotes() != null && !journal.getNotes().isEmpty()) {
            holder.txtNotes.setText(journal.getNotes());
            holder.txtNotes.setVisibility(View.VISIBLE);
        } else {
//            holder.txtNotes.setVisibility(View.GONE);
        }

        holder.txtInitial.setText(desc.length() > 2 ?
                desc.substring(0,2) : desc);

        holder.llDiscount.removeAllViews();
    }

    @Override
    public int getItemCount() {
        return journalList.size();
    }

    public interface OnItemClickListener{
        void onClick(SalesOrderDetailModel item);
        void onClickDetail(SalesOrderDetailModel item);
    }

}
