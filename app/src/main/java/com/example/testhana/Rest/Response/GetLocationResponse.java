package com.example.testhana.Rest.Response;

import com.example.testhana.Models.Realm.LocationCheckIn;

import java.util.List;

public class GetLocationResponse {
    private String result;
    private String message;
    private List<LocationCheckIn> locations;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<LocationCheckIn> getLocations() {
        return locations;
    }

    public void setLocations(List<LocationCheckIn> locations) {
        this.locations = locations;
    }
}
