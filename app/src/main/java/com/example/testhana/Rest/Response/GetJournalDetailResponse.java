package com.example.testhana.Rest.Response;


import com.example.testhana.Models.Realm.JournalDetail;

import java.util.List;

public class GetJournalDetailResponse {
    private String result;
    private String message;
    private List<JournalDetail> journalDetails;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<JournalDetail> getJournalDetails() {
        return journalDetails;
    }

    public void setJournalDetails(List<JournalDetail> journalDetails) {
        this.journalDetails = journalDetails;
    }
}
