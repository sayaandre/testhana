package com.example.testhana.Rest.Response;

import com.example.testhana.Models.Realm.BatchItemMaster;

import java.util.List;

public class GetBatchItemResponse {
    private String result;
    private String message;
    private List<BatchItemMaster> batchitemlist;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<BatchItemMaster> getBatchitemlist() {
        return batchitemlist;
    }

    public void setBatchitemlist(List<BatchItemMaster> batchitemlist) {
        this.batchitemlist = batchitemlist;
    }
}
