package com.example.testhana.Rest.ModelClass;

public class DocumentLinesClass {
    private String ItemCode;
    private String Quantity;
    private String TaxCode;
    private String UnitPrice;

    public DocumentLinesClass(String ic, String q, String tc, String up)
    {
        this.ItemCode = ic;
        this.Quantity = q;
        this.TaxCode = tc;
        this.UnitPrice = up;
    }

    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(String taxCode) {
        TaxCode = taxCode;
    }

    public String getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        UnitPrice = unitPrice;
    }
}
