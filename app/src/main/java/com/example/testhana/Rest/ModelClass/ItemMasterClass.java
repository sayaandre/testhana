package com.example.testhana.Rest.ModelClass;

import java.util.List;

public class ItemMasterClass {
    private String ItemCode;
    private String ItemName;
    private String ForeignName;
    private String ItemType;
    private int ItemsGroupCode;
    private List<ItemPriceClass> ItemPrices;

    public ItemMasterClass(String ic, String inm, String fn, String it, int igc, List<ItemPriceClass> ip)
    {
        this.ItemCode = ic;
        this.ItemName = inm;
        this.ForeignName = fn;
        this.ItemType = it;
        this.ItemsGroupCode = igc;
        this.ItemPrices = ip;
    }

    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getForeignName() {
        return ForeignName;
    }

    public void setForeignName(String foreignName) {
        ForeignName = foreignName;
    }

    public String getItemType() {
        return ItemType;
    }

    public void setItemType(String itemType) {
        ItemType = itemType;
    }

    public int getItemsGroupCode() {
        return ItemsGroupCode;
    }

    public void setItemsGroupCode(int itemsGroupCode) {
        ItemsGroupCode = itemsGroupCode;
    }

    public List<ItemPriceClass> getItemPrices() {
        return ItemPrices;
    }

    public void setItemPrices(List<ItemPriceClass> itemPrices) {
        ItemPrices = itemPrices;
    }
}
