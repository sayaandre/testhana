package com.example.testhana.Rest;


import com.example.testhana.Helper.Constant;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    //public static String url = "http://192.168.1.123:2345/oviax/";
    public static String OTPurl = Constant.otpUrl;
    public static String url = Constant.baseUrl;

    private static Retrofit retrofit = null;
    private static Retrofit OTPretrofit = null;

    // untuk terhubung dengan server biasa (di luar demo)
    public static Retrofit getClient() {
        if(retrofit == null) {
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getClientOTP() {
        if(OTPretrofit == null) {

            final String ApiKey = "1lLA4B8A87E47D742ED9BE95140BD0";

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request newRequest  = chain.request().newBuilder()
                                    .addHeader("Authorization", "bearer " + ApiKey)
                                    .build();
                            return chain.proceed(newRequest);
                        }})
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            OTPretrofit = new Retrofit.Builder()
                    .baseUrl(OTPurl)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return OTPretrofit;
    }

    // untuk terhubung dengan server demo kita
    /*public static Retrofit getClient() {
        if(retrofit == null) {
            ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.COMPATIBLE_TLS)
                    .tlsVersions(TlsVersion.TLS_1_0)
                    .cipherSuites(CipherSuite.TLS_RSA_WITH_3DES_EDE_CBC_SHA,
                            CipherSuite.TLS_RSA_WITH_RC4_128_SHA)
                    .build();

            OkHttpClient okHttpClient = new OkHttpClient();
            TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[] {};
                }

                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

            // Install the all-trusting trust manager
            try {
                SSLContext sc = SSLContext.getInstance("TLS");
                sc.init(null, trustAllCerts, new SecureRandom());
                okHttpClient = new OkHttpClient.Builder()
                        //.sslSocketFactory(sc.getSocketFactory())
                        .connectionSpecs(Collections.singletonList(spec))
                        .hostnameVerifier(new HostnameVerifier() {
                            @Override
                            public boolean verify(String hostname, SSLSession session) {
                                return true;
                            }
                        })
                        .build();
            } catch (Exception e) {
                e.printStackTrace();
            }

            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }*/
}
