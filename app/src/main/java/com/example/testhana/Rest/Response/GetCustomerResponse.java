package com.example.testhana.Rest.Response;


import com.example.testhana.Models.Realm.CustomerMaster;

import java.util.List;

public class GetCustomerResponse {
    private String result;
    private String message;
    private List<CustomerMaster> customers;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CustomerMaster> getCustomers() {
        return customers;
    }

    public void setCustomers(List<CustomerMaster> customers) {
        this.customers = customers;
    }
}
