package com.example.testhana.Rest.Response;

import com.example.testhana.Models.Realm.JournalDetail;
import com.example.testhana.Models.Realm.SalesOrderDetail;

import java.util.List;

public class GetSalesOrderDetailResponse {
    private String result;
    private String message;
    private List<SalesOrderDetail> journalDetails;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SalesOrderDetail> getJournalDetails() {
        return journalDetails;
    }

    public void setJournalDetails(List<SalesOrderDetail> journalDetails) {
        this.journalDetails = journalDetails;
    }
}
