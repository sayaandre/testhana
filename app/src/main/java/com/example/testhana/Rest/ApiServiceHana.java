package com.example.testhana.Rest;


import com.example.testhana.Rest.ModelClass.ItemMasterClass;
import com.example.testhana.Rest.ModelClass.LoginHanaClass;
import com.example.testhana.Rest.ModelClass.OrderClass;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiServiceHana {

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @POST("Login")
    Call<ResponseBody> login(@Body LoginHanaClass params);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @GET("BusinessPartners")
    Call<ResponseBody> getBusinessPartner(@Query("$select") String select, @Query("$skip") String page, @Header("Cookie") String cookie);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @GET("BusinessPartners")
    Call<ResponseBody> getFirstBusinessPartner(@Query("$select") String select, @Header("Cookie") String cookie);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @POST("BusinessPartners")
    Call<ResponseBody> createBusinessPartner(@Header("Cookie") String cookie, @Body Map<String, Object> params);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @GET("BusinessPartners('{bpid}')")
    Call<ResponseBody> getBusinessPartnerById(@Path("bpid") String bpid, @Query("$select") String select, @Header("Cookie") String cookie);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @DELETE("BusinessPartners('{bpid}')")
    Call<ResponseBody> deleteBusinessPartner(@Path("bpid") String bpid, @Header("Cookie") String cookie);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @PATCH("BusinessPartners('{bpid}')")
    Call<ResponseBody> updateBusinessPartner(@Path("bpid") String bpid, @Header("Cookie") String cookie, @Body Map<String, Object> params);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @GET("BusinessPartnerGroups")
    Call<ResponseBody> getBusinessPartnerGroup(@Header("Cookie") String cookie);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @GET("Currencies")
    Call<ResponseBody> getBusinessPartnerCurrency(@Header("Cookie") String cookie);

    //==============================================================================================================================
    //==============================================================================================================================
    //==============================================================================================================================


    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @GET("Items")
    Call<ResponseBody> getItems(@Query("$skip") String page, @Query("$select") String select, @Header("Cookie") String cookie);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @GET("Items")
    Call<ResponseBody> getFirstItems(@Query("$select") String select, @Header("Cookie") String cookie);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @POST("Items")
    Call<ResponseBody> createItem(@Header("Cookie") String cookie, @Body ItemMasterClass params);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @GET("Items('{iid}')")
    Call<ResponseBody> getItemId(@Path("iid") String iid, @Query("$select") String select, @Header("Cookie") String cookie);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @DELETE("Items('{iid}')")
    Call<ResponseBody> deleteItem(@Path("iid") String iid, @Header("Cookie") String cookie);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @PATCH("Items('{iid}')")
    Call<ResponseBody> updateItem(@Path("iid") String iid, @Header("Cookie") String cookie, @Body ItemMasterClass params);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @GET("ItemGroups")
    Call<ResponseBody> getItemGroup(@Header("Cookie") String cookie);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @GET("UnitOfMeasurementGroups")
    Call<ResponseBody> getUoMGroup(@Header("Cookie") String cookie);

    //==============================================================================================================================
    //==============================================================================================================================
    //==============================================================================================================================

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @GET("Orders")
    Call<ResponseBody> getOrders(@Header("Cookie") String cookie);

    @Headers({"Content-Type: application/json;odata=minimalmetadata;charset=utf-8"})
    @POST("Orders")
    Call<ResponseBody> createOrder(@Header("Cookie") String cookie, @Body OrderClass params);
}
