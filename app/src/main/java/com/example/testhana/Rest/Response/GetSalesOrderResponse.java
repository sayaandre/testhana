package com.example.testhana.Rest.Response;

import com.example.testhana.Models.Realm.Journal;
import com.example.testhana.Models.Realm.SalesOrder;

import java.util.List;

public class GetSalesOrderResponse {
    private String result;
    private String message;
    private List<SalesOrder> details;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SalesOrder> getDetails() {
        return details;
    }

    public void setDetails(List<SalesOrder> details) {
        this.details = details;
    }
}
