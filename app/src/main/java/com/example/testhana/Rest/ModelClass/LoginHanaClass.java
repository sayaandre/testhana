package com.example.testhana.Rest.ModelClass;

public class LoginHanaClass {
    private String CompanyDB;
    private String Password;
    private String UserName;

    public LoginHanaClass(String dcb, String pass, String uname){
        this.CompanyDB = dcb;
        this.Password = pass;
        this.UserName = uname;
    }

    public String getCompanyDB() {
        return CompanyDB;
    }

    public void setCompanyDB(String companyDB) {
        CompanyDB = companyDB;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }
}
