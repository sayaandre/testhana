package com.example.testhana.Rest.ModelClass;

import java.util.List;

public class OrderClass {
    private String CardCode;
    private String DocDueDate;
    private List<DocumentLinesClass> DocumentLines;

    public OrderClass(String cc, String ddd, List<DocumentLinesClass> dl)
    {
        this.CardCode = cc;
        this.DocDueDate = ddd;
        this.DocumentLines = dl;
    }

    public String getCardCode() {
        return CardCode;
    }

    public void setCardCode(String cardCode) {
        CardCode = cardCode;
    }

    public String getDocDueDate() {
        return DocDueDate;
    }

    public void setDocDueDate(String docDueDate) {
        DocDueDate = docDueDate;
    }

    public List<DocumentLinesClass> getDocumentLines() {
        return DocumentLines;
    }

    public void setDocumentLines(List<DocumentLinesClass> documentLines) {
        DocumentLines = documentLines;
    }
}
