package com.example.testhana.Rest.ModelClass;

import java.util.List;

public class ItemPriceClass {
    private int PriceList;
    private double Price;
    private String Currency;
    private String AdditionalPrice1;
    private String AdditionalCurrency1;
    private String AdditionalPrice2;
    private String AdditionalCurrency2;
    private int BasePriceList;
    private double Factor;
    private List UoMPrices;

    public ItemPriceClass(int pl, double p, String c, String ap1, String ac1, String ap2, String ac2, int bpl, double f, List uom)
    {
        this.PriceList = pl;
        this.Price = p;
        this.Currency = c;
        this.AdditionalPrice1 = ap1;
        this.AdditionalCurrency1 = ac1;
        this.AdditionalPrice2 = ap2;
        this.AdditionalCurrency2 = ac2;
        this.BasePriceList = bpl;
        this.Factor = f;
        this.UoMPrices = uom;
    }

    public int getPriceList() {
        return PriceList;
    }

    public void setPriceList(int priceList) {
        PriceList = priceList;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    public String getAdditionalPrice1() {
        return AdditionalPrice1;
    }

    public void setAdditionalPrice1(String additionalPrice1) {
        AdditionalPrice1 = additionalPrice1;
    }

    public String getAdditionalCurrency1() {
        return AdditionalCurrency1;
    }

    public void setAdditionalCurrency1(String additionalCurrency1) {
        AdditionalCurrency1 = additionalCurrency1;
    }

    public String getAdditionalPrice2() {
        return AdditionalPrice2;
    }

    public void setAdditionalPrice2(String additionalPrice2) {
        AdditionalPrice2 = additionalPrice2;
    }

    public String getAdditionalCurrency2() {
        return AdditionalCurrency2;
    }

    public void setAdditionalCurrency2(String additionalCurrency2) {
        AdditionalCurrency2 = additionalCurrency2;
    }

    public int getBasePriceList() {
        return BasePriceList;
    }

    public void setBasePriceList(int basePriceList) {
        BasePriceList = basePriceList;
    }

    public double getFactor() {
        return Factor;
    }

    public void setFactor(double factor) {
        Factor = factor;
    }

    public List getUoMPrices() {
        return UoMPrices;
    }

    public void setUoMPrices(List uoMPrices) {
        UoMPrices = uoMPrices;
    }
}
