package com.example.testhana.Rest;


import com.example.testhana.Rest.Response.ChangeStatusSalesOrderDetailResponse;
import com.example.testhana.Rest.Response.ChangeStatusSalesOrderResponse;
import com.example.testhana.Rest.Response.GetCustomerResponse;
import com.example.testhana.Rest.Response.GetItemResponse;
import com.example.testhana.Rest.Response.GetJournalDetailResponse;
import com.example.testhana.Rest.Response.GetJournalResponse;
import com.example.testhana.Rest.Response.GetLocationResponse;
import com.example.testhana.Rest.Response.GetSalesOrderDetailResponse;
import com.example.testhana.Rest.Response.GetSalesOrderResponse;
import com.example.testhana.Rest.Response.LoginResponse;
import com.example.testhana.Rest.Response.ManageCustomerResponse;
import com.example.testhana.Rest.Response.ManageItemResponse;
import com.example.testhana.Rest.Response.SubmitAbsenceResponse;
import com.example.testhana.Rest.Response.SubmitJournalResponse;
import com.example.testhana.Rest.Response.SubmitSalesOrderResponse;
import com.example.testhana.Rest.Response.UpdateTokenFBResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiService {

    @FormUrlEncoded
    @POST("WebMaster?doLogin")
    Call<LoginResponse> login(@Field("phone") String phone, @Field("password") String password, @Field("imei") String imei);

    @FormUrlEncoded
    @POST("WebMaster?updateTokenFB")
    Call<UpdateTokenFBResponse> updateTokenFB(@Field("userid") int userid, @Field("token") String token);

    @FormUrlEncoded
    @POST("WebMaster?getItem")
    Call<GetItemResponse> getItem(@Field("storeId") int storeId, @Field("merchantId") int merchantId);

    @FormUrlEncoded
    @POST("WebMaster?manageItem")
    Call<ManageItemResponse> manageItem(@Field("mode") int mode, @Field("id") int id,
                                        @Field("no") String no, @Field("desc") String desc,
                                        @Field("name") String name, @Field("typeid") int typeid,
                                        @Field("groupid") int groupid, @Field("uomid") int uomid,
                                        @Field("barcode") String barcode, @Field("price") double price,
                                        @Field("image") String image);

    @FormUrlEncoded
    @POST("WebMaster?getCustomer")
    Call<GetCustomerResponse> getCustomer(@Field("storeId") int storeId, @Field("merchantId") int merchantId);

    @FormUrlEncoded
    @POST("WebMaster?manageCustomer")
    Call<ManageCustomerResponse> manageCustomer(@Field("mode") int mode, @Field("id") int id,
                                                @Field("code") String code, @Field("role") int role,
                                                @Field("name") String name, @Field("group") int group,
                                                @Field("curr") int curr, @Field("phone") String phone,
                                                @Field("address") String address, @Field("email") String email,
                                                @Field("image") String image);

    @FormUrlEncoded
    @POST("Transaction?getJournal")
    Call<GetJournalResponse> getJournal(@Field("trxDate") String trxDate, @Field("cashierId") int cashierid);

    @FormUrlEncoded
    @POST("Transaction?getJournalDetail")
    Call<GetJournalDetailResponse> getJournalDetail(@Field("journalId") long journalId);

    @FormUrlEncoded
    @POST("Transaction?submitJournal")
    Call<SubmitJournalResponse> submitJournal(@Field("receiptNumber") String receiptno, @Field("total") double total,
                                              @Field("rowCounts") int rowcount, @Field("trxType") String trxtype,
                                              @Field("trxDate") String trxdate, @Field("trxTime") String trxtime,
                                              @Field("cashierId") int cashierid, @Field("customerId") String customerid,
                                              @Field("refNo") String refno, @Field("docEntry") int docentry,
                                              @Field("docNum") int docnum, @Field("journalDetail") String journaldetail);

    @FormUrlEncoded
    @POST("WebMaster?getLocation")
    Call<GetLocationResponse> getLocation(@Field("kodesalesman") int kodesalesman, @Field("date") String date);

    @FormUrlEncoded
    @POST("WebMaster?submitAbsence")
    Call<SubmitAbsenceResponse> submitAbsence(@Field("date") String date, @Field("locationid") int locationid);

    @FormUrlEncoded
    @POST("Transaction?getSalesOrder")
    Call<GetSalesOrderResponse> getSalesOrder(@Field("salesId") int cashierid, @Field("role") String role);

    @FormUrlEncoded
    @POST("Transaction?getSalesOrderDetail")
    Call<GetSalesOrderDetailResponse> getSalesOrderDetail(@Field("journalId") long journalId);

    @FormUrlEncoded
    @POST("Transaction?submitSalesOrder")
    Call<SubmitSalesOrderResponse> submitSalesOrder(@Field("salesOrderNum") String receiptno, @Field("total") double total,
                                                    @Field("rowCounts") int rowcount, @Field("trxDate") String trxdate,
                                                    @Field("trxTime") String trxtime,
                                                    @Field("userId") int cashierid, @Field("customerId") String customerid,
                                                    @Field("status") String refno, @Field("journalDetail") String journaldetail);

    @FormUrlEncoded
    @POST("Transaction?changeStatusSalesOrder")
    Call<ChangeStatusSalesOrderResponse> changeStatusSalesOrder(@Field("salesOrderId") long journalId,
                                                                @Field("status") String status);

    @FormUrlEncoded
    @POST("Transaction?changeStatusSalesOrderDetail")
    Call<ChangeStatusSalesOrderDetailResponse> changeStatusSalesOrderDetail(@Field("salesOrderDetailId") long journalId,
                                                                            @Field("status") String status);
}
