package com.example.testhana.Rest.Response;


import com.example.testhana.Models.Realm.Journal;

import java.util.List;

public class GetJournalResponse {
    private String result;
    private String message;
    private List<Journal> journals;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Journal> getJournals() {
        return journals;
    }

    public void setJournals(List<Journal> journals) {
        this.journals = journals;
    }
}
