package com.example.testhana.Rest.Response;


import com.example.testhana.Models.Realm.ItemMaster;

import java.util.List;

public class GetItemResponse {
    private String result;
    private String message;
    private List<ItemMaster> items;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ItemMaster> getItems() {
        return items;
    }

    public void setItems(List<ItemMaster> items) {
        this.items = items;
    }
}
