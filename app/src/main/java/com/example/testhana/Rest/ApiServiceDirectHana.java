package com.example.testhana.Rest;

import com.example.testhana.Rest.Response.GetBatchItemResponse;
import com.example.testhana.Rest.Response.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiServiceDirectHana {

    @FormUrlEncoded
    @POST("values?doTest")
    Call<GetBatchItemResponse> getBatchItem(@Field("kodelokasi") String kodelokasi);
}
